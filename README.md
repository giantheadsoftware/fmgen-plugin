# fmgen Maven Plugin 2.1.0
> _This documentation is definitely a work in progress.  The features evolved as they were needed,
often for pretty specific purposes.  In many cases, it's not obvious why the feature exists until
you need it.  I'll continue improving this document as time permits._

## Purpose
The fmgen plugin allows FreeMarker templates to be used during a Maven build (i.e. code-generation) to produce files from FreeMarker tempaltes (FTL).

The models supplied to the template engine can be produced in multiple ways.

## Plugin Management
```xml
<build>
  <pluginManagement>
    <plugins>
      <plugin>
        <groupId>com.giantheadsoftware</groupId>
        <artifactId>fmgen-maven-plugin</artifactId>
        <!-- use this version for JDK 11 or later -->
        <version>2.1.0-jdk11</version>
        <!-- use this version for JDK 8 -->
        <!-- <version>2.1.0-jdk8</version> -->
      </plugin>
    </plugins>
  </pluginManagement>
</build>
```

## Plugin Configuration Parameters

|Parameter|Type|Description
|---|---|---|
|aggregators|list&lt;aggregator>|List of aggregators to be executed.
|basePath|String|**default: `${project.basedir}`** _NOTE: this will not default to `${project.basedir}` if targetPath is explicitly set. To make sure the project basedir is used, set it explicitly in the configuration i.e.: `<baseDir>${project.basedir}</baseDir>`_
|buildDir|string|**default: `${project.build.directory}`**
|classNameMap|String (regex replace)|String replace expression that's used to produce the generated classname as derived from the source classname.  For example, a prefix or suffix could be added to the name. _TODO: examples_
|customProps|map|An arbitrary map of name/value pairs that can be used by any ModelProducer or TemplateLoader that can recognize them.  Some custom properties used by the provided modules are shown below. _TODO: examples_
|excludeArgTypePattern|string|Regex pattern for excluding methods by argument type (i.e. FQ classname).  If any argument matches this pattern, the method is excluded.  By default, none are excluded. **default: `^$`**
|excludeMemberAnnotationPattern|string|Regex pattern for excluding class members by annotation (i.e. FQ annotation classname).  If any member-level annotation matches this patttern, the member is excluded. By default, none are excluded. **default: `^$`**
|excludeMemberPattern|string|Regex pattern for excluding class members by name.  By default, none are excluded. **default: `^$`**
|excludeModelAnnotationPattern|string|Regex pattern for excluding model classes by annotation (i.e. FQ annotation classname).  If any class-level annotation matches this patttern, the class is excluded. By default, none are excluded. **default: `^$`**
|excludeModelPattern|string|Regex pattern for excluding model files by base file name, extension not included (i.e. the class name).    By default, none are excluded. **default: `^$`**
|excludeTypePattern|string|Regex pattern for excluding class members by type or return type (i.e. FQ classname).  If the type matches this pattern, the member is excluded.  By default, none are excluded. **default: `^$`**
|genDir|string|Generated files are written here.  **default: `${project.build.directory}/generated-sources/fmgen`**
|imports|list&lt;string&gt;|Add &lt;import&gt; elements that will be added to the imports list.  Add imports here that may be required by the template but would not be detected by scanning the model classes.  These will be subject to import exclusion rules such as excluding `java.lang.*` and current package.  Imports added here will be sorted with any imports identified by the model producer.  Note that imports can also be added from within the template using `addImports(string_sequence)`.
|includeArgTypePattern|string|Regex pattern for matching methods by argument type (i.e. FQ classname).  If any argument matches this pattern, the method is included (unless excluded).  By default, all are included. **default: `.*`**
|includeEnums|boolean|If true, `enum` types are included in the model scan.  Full support of enums (such as making all the values accessible to the template) is supported only by the `JavaFilteredMethodProducer`. By default, enums are not included. **default: `false`**
|includeMemberAnnotationPattern|string|Regex pattern for matching class members by annotation (i.e. FQ annotation classname).  If any member-level annotation matches this patttern, the member is included (unless excluded).   By default, all are included. **default: `.*`**
|includeMemberPattern|string|Regex pattern for matching class members by name.  By default, all are included. **default: `.*`**
|includeModelAnnotationPattern|string|Regex pattern for matching class members by type or return type (i.e. FQ classname).  If the type matches this pattern, the member is included (unless excluded).  By default, all are included. **default: `.*`**
|includeModelPattern|string|Regex pattern for matching model files by base file name, extension not included (i.e. the class name).  By default, all are allowed. **default: `.*`**
|includeTypePattern|string|Regex pattern for matching methods by argument type (i.e. FQ classname).  If any argument matches this pattern, the method is included (unless excluded).  By default, all are included. **default: `.*`**
|**modelBasePath**|string|**(required)** This is the path to the "model" files.  This is a relative path (starting with no '/') relative to the top of the src directory.  For models that are loaded from complied classes, this is the package name (with slashes instead of dots) where the model classes reside, i.e. `com/giantheadsoftware/model`
|**modelProducer**|string|**(required)** Fully qualified classname of the class that produces the models.  Built-in produces are listed in the section [Model Producers](#model-producers) below.  This class must implement the interface `com.giantheadsoftware.fmgen.mode.ModelProducer`.  If it's not posible or desired to implement that interface, then the methods declared in that interface must be provided.
|modelProps|map|An arbitrary map of name/value pairs that is passed to the model and is available to the FTL template under the name "modelProps". _TODO: examples_
|packageMap|string (regex replace)|Specifies how the source package name is altered to become the destination package name.  This property is ignored if the `targetPath` property is set. _TODO: examples_
|targetFileExt|string|When set, applies this file extension to generated files.  When `.ts` or `.js` are applied, the file path is converted from CamelCase to slug-case.  If multiple dots appear in the value, they are preserved in the final file name, but only the extension after the final dot will impact the formatting. For example, `.service.ts` will result in a slug-case file with a `.service.ts` extension.  **default: `.java`**
|targetPath|string|Specifies the target package (and file path) of generated files.  This is relative to the `genDir`.  If this is set, then all generated files will go to this location, ignoring `packageMap`.  Example: `com.giantheadsoftware.model`
|targetPathMap|string (regex replace)|If applied, causes each generated file to be placed in a directory specified by replacing the target path according to the regex replace pattern.  The input path is whatever is produced by applying **packageMap** or **targetPath**. Paths are relative to the **genDir**. _TODO: examples_|
|**template**|string|**(required)** The filename of the FTL template to apply.
|**templateLoader**|string|**(required)** One of `ResourceTemplateLoader` (for templates within the classpath) or `FileTemplateLoader` (for templates outside the classpath in local files).  The FQCN is not required for either of these default loaders.
|**templatePath**|string|**(required)** Path to the location where the FTL template(s) can be found.  This will be relative to the classpath root when the the `ResourceTemplateLoader` is used (i.e. `com/giantheadsoftware/templates`).  When using the `FileTemplateLoader`, the path is relative to `buildDir` unless it's absolute.


### Aggregator Parameters

|Parameter|Type|Description
|---|---|---|
|**name**|string|**(required)** The name applied to this aggregator.  It's not really used for anything, but is provided to the template engine.
|**template**|string|**(required)** The name of the FTL template file.
|**fileName**|string|**(required)** The name of the generated file.
|params|map|An arbitrary set of name/value pairs accessible to the FTL template under the name "params".  This is in addition to the modelProps, which are also available to the aggregator.


### Custom Parameters

|Parameter|Type|Description|
|---|---|---|
|ifc-package|string|Package where pre-existing (not generated) interfaces are located.  This is useful if the generated classes are expected to implement predefined interfaces.|
|ifc-class-name-map|string|String replace pattern that may be used for interfaces|
|baseClassName|string|If the model files are expected to extend a common base class, provide that name here.|
|abstracts|string|Comma separted list of class names (not fully qualified -- expected to be found in the modelPath package).  Classes listed here will be created as `abstract` |


## Model Producers
The model producer implementation is set by the `modelProducer` config param.

_Built-in model producers:_

### JavaGetterProducer

**Purpose**: Used for generating data model instances (i.e. POJOs).
**Model Sources**:  Compiled Java classes with conventional POJO getter methods
**FQCN**: com.giantheadsoftware.fmgen.model.java.JavaGetterProducer

**Notes**:
This producer is suited to generating POJO classes, such as an object model or message DTOs.  It scans compiled source classes from the classpath.  All public field accessors (getters) are scanned by default, and are converted to "properties" in the model.  Getters can be filtered with the include/exclude patterns for members, annotations, and/or types.

The source classes can be interfaces with the getter signatures defined.  The setters can be defined also, but won't be used by the plugin.  Conventional getter method patterns are recognized: `get[A-Z].*` and `is[A-Z].*` (for boolean properties).  Getter methods must have zero arguments.  Methods with arguments are ignored.

> The challenge with using precompiled classes for code generation is that code generation happens before compilation.  Therefore, it's impractical to generate soruces that are decvlared in the same project.  It's necessary to have the model defined in a different project that can be a dependency for this project or for the plugin itself.

### JavaDtoProducer

**Purpose**: Used for generating data model instances (i.e. POJOs).
**Model Sources**:  Compiled Java classes with public fields
**FQCN**: com.giantheadsoftware.fmgen.model.java.JavaDtoProducer

**Notes**:
This producer is suited to generating POJO classes, such as an object model or message DTOs.  It scans compiled source classes from the classpath.  All public fields are scanned by default, and are converted to "properties" in the model.  Getters can be filtered with the include/exclude patterns for members, annotations, and/or types.

> The challenge with using precompiled classes for code generation is that code generation happens before compilation.  Therefore, it's impractical to generate soruces that are decvlared in the same project.  It's necessary to have the model defined in a different project that can be a dependency for this project or for the plugin itself.

### JavaFilteredMethodProducer

**Purpose**: Used for generating arbitrary classes.
**Sources**:  Compiled Java classes with public methods.
**FQCN**: com.giantheadsoftware.fmgen.model.java.JavaFilteredMethodProducer

**Notes**:
This producer can be used for non-POJO applications, such as generating implementations of an API interface.  It scans compiled source classes from the classpath.  All public methods are added to the model by default, but methods can be filtered with the include/exclude patterns for members, annotations, types, and/or args.

> The challenge with using precompiled classes for code generation is that code generation happens before compilation.  Therefore, it's impractical to generate soruces that are decvlared in the same project.  It's necessary to have the model defined in a different project that can be a dependency for this project or for the plugin itself.

### JsonProducer

**Purpose**: Used for generating arbitrary files.
**Sources**:  JSON files with top-level Object or Array
**FQCN**: com.giantheadsoftware.fmgen.model.Json.JsonProducer

**Notes**:
This producer presents the structure of the JSON file(s) as the model for the template.   The `modelProps` are also available in the model.  The include/exclude patterns are not applied to JSON models.

### Custom ModelProducer
A custom model producer can be used by implemeting the `com.giantheadsoftware.fmgen.model.ModelProducer` interface.

## Template Loaders

### ResourceTemplateLoader
FTL templates are loaded as classpath resources.

### FileTemplateLoader
FTL templates are loaded as regular files.

### Custom Template Loaders
A custom template loader can be installed by creating a class that implements the `com.giantheadsoftware.fmgen.template.TemplateLoader` interface.  Set the FQCN of the custom loader in to the `templateLoader` config property.

Make sure the custom loader is available to the plugin by having it in the build classpath, or by adding the JAR as a dependency to the plugin.

## Aggregators
The `fmgen` plugin can produce multiple files per build, one output file per model source class located by the ModelProducer.  An Aggregator is a special model that is processed just once per build, and therefore produces just a single file. The Aggregator has access to all of the model classes.

The aggregator instance is passed to the FTL template as the model.

> An Aggregator is useful for creating a base class or a factory class, for example.

The aggregator receives its own config params, including the FTL template to be used.  Refer to *Aggregator Parameters* above.

## The Java Class Model Structure \{WIP\}
The Java Class producers provide the following model to the template engine:

> Running the build in debug mode (`mvn -X`) causes the entire model to be logged -- extremely verbose!

### JavaClassModel
This is the top-level model that gets processed by the FreeMarker template.

#### JavaClassModel Properties

|property|type|description|
|:--|---|---|
|allRefs|AllClassMap||
|baseClassName|String|The (optional) base class that all model classes extend at some level in depth|
|fqn|String|Fully qualified class name|
|imports|List&lt;String>|List of fully-qualified classes that would have to be imported (formatted for Java imports)|
|importSlugPaths|List&lt;String>|List of import paths with the class names converted from CamelCase to slug-case. (suitable for TypeScript)|
|interfaces|List&lt;JavaTypeRef>|List of interfaces implemented by the class|
|methods|List&lt;JavaMethodModel>|The public methods of this model class|
|modelInterface|JavaTypeRef||
|modelProps|Map&lt;String,Object>|A map of the `modeProps` that were declared in the POM file.|
|name|String|"Simple" (unqualified) class name|
|pluginParams|Map&lt;String,Object>|All of the plugin parameters specified (or derived from) the configuration in the POM file.|
|**srcClassRef**|JavaTypeRef|**Use this to access the JavaTypeRef properties for this model class**.  Most of the detailed reflection data is here.  Refer to [JavaTypeRef Properties](#javatyperef-properties) for details.|
|superClass|JavaTypeRef|The direct superclass|
|targetFileName|String|Relative path to the file that will be generated.  This path will be derived from the fully qualified class name.|
|targetPackage|String|The target package of the generated model class.  This will just be the target file path if not generating Java.|
|template|String|The FreeMarker template to be used to generate the output file, as specified in the POM config.|
|typeParams|List&lt;JavaTypeRef>|Generic type params defined at the class level.|

#### JavaClassModel Methods

|method|type|args|description|
|:--|---|---|---|
|addImports|void|List&lt;String>|_TODO: examples_|
|mapImportPath|String|String|_TODO: examples_|
|hasContainerMembers|boolean|n/a|true if any methods in this class return collections or maps, or has collection or map args|
|toCamelCase|Stirng|String|Converts the string argument to **CamelCase**|
|toSlugCase|String|String|Converts the string argument to **slug-case**|

### JavaTypeRef
This object contains most of the class-level data extraced form a model class.

#### JavaTypeRef Properties

|property|type|description|
|:--|---|---|
|actual|JavaTypeRef|if the referenced type is variable and has an actual class, return that, otherwise null.|
|allRefs|Set&lt;JavaTypeRef>|All types referenced by this type in any way.|
|classToInstantiate|Class|This is implemented for collection and map classes. Returns a concrete class that can be instantiated in place of an abstract one, or null if there is no such class.|
|defaultContainer|JavaTypeRef|if the referenced type is a container with an abstract/interface type, return the concrete container class that can be instantiated for it.|
|fqn|String|The fully qualified type name, which may have been mapped from its original name to a target name.|
|hasActual|boolean|true if the referenced type is variable and has an actual class.|
|hasLowerBounds|boolean|true if the referenced type is variable and has lower bounds.|
|hasUpperBounds|boolean|true if the referenced type is variable and has upper bounds.|
|isAbstract|boolean|true if the modifiers of this type indicate it's an abstract class.|
|isArray|boolena|true if the referenced type is an array.|
|isBoundedVar|boolean|true if the referenced type is variable and has upper or lower bounds.|
|isCollection|boolean|true if the referenced type extends `java.util.Collection`.|
|isContainer|boolean|true if either `isCollection` or `isMap` are true.|
|isEnum|boolena|true if the referenced type is `enum`.|
|isImported|boolean|true if this item is expected to be added to the imports list of a generated Java class file.|
|isInModel|boolean|true if the source class of this type is located in one of the packages where model classes are found.  This implies that the referenced entity is a top-level model itself.|
|isInterface|boolean|true if the modifiers of this type indicate it's an interface.|
|isMap|boolean|true if the referenced type extends `java.util.Map`.|
|isNullable|boolean|true if the type is not primitive.|
|isParameterized|boolean|true if the referenced type is `ParameterizedType`.|
|isVar|boolean|true if the referenced type is `TypeVariable` or `WildcardType`.|
|lowerBounds|List&gt;JavaTypeRef>|if the referenced type is variable, the list of lower bounds, otherwise empty list.|
|modifierLabels|List&lt;String>|String values of the class modifiers (i.e. "public", "abstract", etc.).|
|modifiers|int|If this type can be resolved to a Class via getRawClass(), return the modifiers of that class. Otherwise, return 0.|
|name|String|the potentially unqualified name for this ref.  If a type is determined to be imported, then the package name is removed from the front, and the name just becomes the simple name of the type.|
|nameOrVar|String|if `selfVar == null`, return `name`, else return `selfVar`.|
|origFqn|String|the fully qualified name, prior to mapping.|
|origPackage|String|The fully qualified type name, prior to any mapping.|
|package|String|The fully qualified package name, which may have been mapped from its original name to a target package.|
|pluginParams|Map&lt;String,Object>||
|pluginProperties|Properties|The String valued- elements from `pluginParams`|
|rawClass|Class (java.lang)|Not to be confused with `rawClassRef`.  This method returns the raw class, even if it's parameterized or variable with actual or single upper bound.  Returns null if the type is variable/wildcard without actual or single upper bound.|
|rawClassRef|JavaTypeDef|Not to be confused with `getRawClassRef`.  This method returns a value only when the ref is for a ParameterizedType.  Returns the rawClass if this is a parameterized type reference, otherwise null.|
|referencedEntity|JavaEntity|the JavaEntity instance referenced by this ref.|
|scopeEntity|JavaScopeEntity||
|scopeMember|JavaClassMemberModel||
|scopeModel|JavaClassModel||
|self|JavaTypeRef|A reference to `this`|
|selfVar|String|A generated string (based on the capital letters of the class name) that can be used as a self-referencing variable name in a generic parameter declaration.|
|type|Type (java.lang.reflect)|The Java type of this class.|
|typeParams|List&lt;JavaTypeRef>|For parameterized types, these are the parameters.  They can be variables or actual classes.|
|upperBounds|List&gt;JavaTypeRef>|if the referenced type is variable, the list of upper bounds, otherwise empty list.|


#### JavaTypeRef Methods

|method|type|args|description|
|:--|---|---|---|
|setDefaultContainer|void|JavaTypeRef|||

## Example POM File

### Import Map Example

* The first character in the map is the delimiter.  After the 1st delimiter is the regex pattern to match.  The next occurrence of the delimiter starts the "replace" string.
* The string may contain variables defined in the maven configuration.  Variables use the ${} notation.
```xml
<importMaps>
  <importMap>/^com\.giantheadsoftware\.fmgen\.model\.${thePackage}\.dto\.(\w+)/${fullPkgPath}.model.dto.(1)</importMap>
</importMaps>
```

### Sample Plugin Configuration

```xml
  <build>
    <plugins>
      <plugin>
        <groupId>com.giantheadsoftware</groupId>
        <artifactId>fmgen-maven-plugin</artifactId>
        <version>2.0.5</version>
        <dependencies>
          <!-- this dependency contains the model source classes that will be scanned -->
          <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>my-model</artifactId>
            <version>${project.version}</version>
          </dependency>
        </dependencies>
        <!-- config shared for all executiouns -->
        <configuration>
          <!-- The modelPath is where the source classes are found within the classpath. -->
          <!-- It's a Java package in path form -->
          <modelBasePath>com/giantheadsoftware/my/model</modelBasePath>
          <modelProducer>com.giantheadsoftware.fmgen.model.java.JavaGetterProducer</modelProducer>
          <!-- find & load templates as classpath resources -->
          <templateLoader>ResourceTemplateLoader</templateLoader>
          <!-- where FTL templates are located relative to classpath root or project root,
               depending on templateLoader -->
          <templatePath>/fm-templates</templatePath>
          <customProps>
            <keyType>Long</keyType>
          </customProps>
        </configuration>
        <executions>
          <execution>
            <id>pojos</id>
            <phase>generate-sources</phase>
            <goals>
              <goal>fmgen</goal>
            </goals>
            <!-- config overrides for just this execution -->
            <configuration>
              <!-- template file name without the .ftl extension -->
              <template>mapPojo</template>
              <!-- the path/package where files will be generated -->
              <targetPath>com/giantheadsoftware/model/pojo</targetPath>
              <!-- properties that are passed into the ModelProducer and TemplateLoader -->
              <customProps>
                <!-- substitution pattern to convert source names to destination names-->
                <!-- this pattern appends "Pojo" to the end of the class name -->
                <classNameMap>/^({modelPackage}\.[^\.]+)$/(1)Pojo/</classNameMap>
                <!-- All the generated classes can extend/implement a common base class/interface -->
                <baseClassName>BasePojo</baseClassName>
                <!-- these classes will be generated as abstract -->
                <abstracts>Owned,Entity</abstracts>
                <!-- identifies the package where pre-existing interfaces for the generated classes are located -->
                <ifc-package>com.giantheadsoftware.ifc</ifc-package>
              </customProps>
              <!-- arbitrary properties passed to the model -->
              <modelParams>
                <copyright>Copyright (c) 2016-2021 Gianthead Software</copyright>
              </modelParams>
              <aggregators>
                <!-- use an aggregator to create a base class -->
                <aggregator>
                  <name>Base POJO</name>
                  <fileName>BasePojo.java</fileName>
                  <template>mapPojoBase</template>
                </aggregator>
              </aggregators>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <!-- adds the generated sources to the build -->
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>build-helper-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>addtoSourceFolder</id>
            <goals>
              <goal>add-source</goal>
            </goals>
            <phase>process-sources</phase>
            <configuration>
              <sources>
                <source>${project.build.directory}/generated-sources/fmgen</source>
              </sources>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
```
