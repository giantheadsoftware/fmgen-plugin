package com.giantheadsoftware.fmgen.model;

import com.giantheadsoftware.fmgen.Conventions;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A model is not required to implement the Model interface, but there are some values
 * that the model should be able to produce.  This wrapper attempts to normalize the various ways 
 * that these values could be accessed
 * @author clininger $Id: $
 */
public class ModelWrapper implements Model {

  private final Object modelish;

  public ModelWrapper(Object modelish, Map<String, Object> pluginParams) {
    if (!(modelish instanceof Model)) {
      if (modelish instanceof Map) {
        modelish = new LinkedMapModel(pluginParams, (Map)modelish);
      }
    }
    this.modelish = modelish;
  }
  
  @Override
  public String getTargetFileName() {
    if (modelish instanceof Model) {
      return ((Model)modelish).getTargetFileName();
    }
    try {
      Method method = modelish.getClass().getMethod(Conventions.ModelMethods.GET_TARGET_FILE_NAME_METHOD);
      if (method == null) {
        throw new IllegalArgumentException("Model class "+modelish.getClass().getName()+" has no method named "
            + Conventions.ModelMethods.GET_TARGET_FILE_NAME_METHOD);
      }
      return (String)method.invoke(modelish);
    } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException | SecurityException x) {
      throw new IllegalArgumentException("Couldn't invoke method "+Conventions.ModelMethods.GET_TARGET_FILE_NAME_METHOD+" on model class "
          + modelish.getClass().getName(), x);
    } 
  }

//  public void setTargetFileName(String fileName) {
//    if (modelish instanceof Model) {
//      ((Model)modelish).setTargetFileName(fileName);
//      return;
//    }
//    try {
//      Method method = modelish.getClass().getMethod(Conventions.ModelMethods.SET_TARGET_FILE_NAME_METHOD, String.class);
//      if (method == null) {
//        throw new IllegalArgumentException("Model class "+modelish.getClass().getName()+" has no method named "
//            + Conventions.ModelMethods.SET_TARGET_FILE_NAME_METHOD);
//      }
//      method.invoke(modelish, fileName);
//    } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException | SecurityException x) {
//      throw new IllegalArgumentException("Couldn't invoke method "+Conventions.ModelMethods.GET_TEMPLATE_METHOD+" on model class "
//          + modelish.getClass().getName(), x);
//    } 
//  }

  @Override
  public Map<String, Object> getPluginParams() {
    if (modelish instanceof Model) {
      return ((Model)modelish).getPluginParams();
    }
    try {
      Method method = modelish.getClass().getMethod(Conventions.ModelMethods.GET_PLUGIN_PARAMS_METHOD);
      if (method == null) {
        throw new IllegalArgumentException("Model class "+modelish.getClass().getName()+" has no method named "
            + Conventions.ModelMethods.GET_PLUGIN_PARAMS_METHOD);
      }
      return (Map)method.invoke(modelish);
    } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException | SecurityException x) {
      throw new IllegalArgumentException("Couldn't invoke method "+Conventions.ModelMethods.GET_PLUGIN_PARAMS_METHOD+" on model class "
          + modelish.getClass().getName(), x);
    } 
  }

  @Override
  public String getTemplate() {
    if (modelish instanceof Model) {
      return ((Model)modelish).getTemplate();
    }
    try {
      Method method = modelish.getClass().getMethod(Conventions.ModelMethods.GET_TEMPLATE_METHOD);
      if (method == null) {
        throw new IllegalArgumentException("Model class "+modelish.getClass().getName()+" has no method named "
            + Conventions.ModelMethods.GET_TEMPLATE_METHOD);
      }
      return (String)method.invoke(modelish);
    } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException | SecurityException x) {
      throw new IllegalArgumentException("Couldn't invoke method "+Conventions.ModelMethods.GET_TEMPLATE_METHOD+" on model class "
          + modelish.getClass().getName(), x);
    } 
  }

  @Override
  public void setTemplate(String template) {
    if (modelish instanceof Model) {
      ((Model)modelish).setTemplate(template);
      return;
    }
    try {
      Method method = modelish.getClass().getMethod(Conventions.ModelMethods.SET_TEMPLATE_METHOD, String.class);
      if (method == null) {
        throw new IllegalArgumentException("Model class "+modelish.getClass().getName()+" has no method named "
            + Conventions.ModelMethods.SET_TEMPLATE_METHOD);
      }
      method.invoke(modelish, template);
    } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException | SecurityException x) {
      throw new IllegalArgumentException("Couldn't invoke method "+Conventions.ModelMethods.GET_TEMPLATE_METHOD+" on model class "
          + modelish.getClass().getName(), x);
    } 
  }

  @Override
  public Map<String, Object> getModelProps() {
    if (modelish instanceof Model) {
      return ((Model)modelish).getModelProps();
    }
    try {
      Method method = modelish.getClass().getMethod(Conventions.ModelMethods.GET_PROPERTIES_METHOD);
      if (method == null) {
        throw new IllegalArgumentException("Model class "+modelish.getClass().getName()+" has no method named "
            + Conventions.ModelMethods.GET_PROPERTIES_METHOD);
      }
      return (Map)method.invoke(modelish);
    } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException | SecurityException x) {
      throw new IllegalArgumentException("Couldn't invoke method "+Conventions.ModelMethods.GET_PROPERTIES_METHOD+" on model class "
          + modelish.getClass().getName(), x);
    } 
  }

}
