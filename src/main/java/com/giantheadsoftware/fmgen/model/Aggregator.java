package com.giantheadsoftware.fmgen.model;

import com.giantheadsoftware.fmgen.ParameterNames;
import com.giantheadsoftware.fmgen.util.RegexReplace;
import com.giantheadsoftware.fmgen.util.Util;
import org.apache.maven.plugin.MojoExecutionException;

import java.io.File;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author clininger $Id: $
 */
public class Aggregator implements Model {

  private String name;
  private String template;
  private String fileName;
  private File genDir;
  private Map<String, Object> pluginParams;
  private final Set<String> imports = new TreeSet<>();
  private Pattern importIncludePattern = Pattern.compile(".*"); // by default include everything
  private Pattern importExcludePattern = Pattern.compile("^$"); // by default exclude nothing
  private List<RegexReplace> importMaps = new LinkedList<>();
  /**
   * Parameters specific to this aggregator, defined in the config
   */
  private Map<String, Object> aggParams;
  private Collection<Model> models;
  private boolean runWithoutModels = false;

  @Override
  public Map<String, Object> getPluginParams() {
    return pluginParams;
  }

  /**
   * @return all plugin parameters
   */
  public Map<String, Object> getParams() {
    return pluginParams;
  }

  public void setParams(Map<String, Object> params) {
    this.pluginParams = params;
  }

  public Map<String, Object> getAggParams() {
    return aggParams;
  }

  public void setAggParams(Map<String, Object> aggParams) {
    this.aggParams = aggParams;
  }

  /**
   * @return the name of the aggregator, as set in the config
   */
  public String getName() {
    return name;
  }

  /**
   * @param name Name as set in the config
   */
  public void setName(String name) {
    this.name = name;
  }

  public Collection<Model> getModels() {
    return models;
  }

  public void setModels(Collection<Model> models) {
    this.models = models;
  }

  /**
   * @return the base name of the template (without path or extension)
   */
  @Override
  public String getTemplate() {
    return template;
  }

  @Override
  public void setTemplate(String template) {
    this.template = template;
  }

  /**
   * @return target file name, relative to genDir
   */
  @Override
  public String getTargetFileName() {
    return fileName;
  }

  public void setTargetFileName(String fileName) {
    this.fileName = fileName;
  }

  /**
   * If provided, overrides the plugin genDir, just for this aggregator instance
   * @return aggregator's genDir
   */
  public File getGenDir() {
    return genDir;
  }

  public void setGenDir(File genDir) {
    this.genDir = genDir;
  }

  public void setImportMaps(List<String> importMaps) {
    this.importMaps.addAll(importMaps.stream()
          .map(importMap -> {
            try {
              return new RegexReplace(importMap, new Properties());
            } catch (MojoExecutionException x) {
              Util.log.error("Error with import map: "+importMap, x);
              return null;
            }
          })
          .filter(Objects::nonNull)
          .collect(Collectors.toList()));
  }

  @Override
  public Map<String, Object> getModelProps() {
    return (Map<String, Object>) pluginParams.get(ParameterNames.MODEL_PROPS);
  }

  public void setImportIncludePattern(String importIncludePattern) {
    this.importIncludePattern = Pattern.compile(importIncludePattern);
  }

  public void setImportExcludePattern(String importExcludePattern) {
    this.importExcludePattern = Pattern.compile(importExcludePattern);
  }

  public void addImports(List<String> imports) {
    this.imports.addAll(imports);
  }

  public List<String> getImports() {
    return imports.stream()
        .filter(imp -> Util.checkStringPattern(imp, importIncludePattern, importExcludePattern))
          .map(this::mapImport)
        .collect(Collectors.toList());
  }

  public String mapImport(String path) {
    for (RegexReplace rr : importMaps) {
      String replaced = rr.replace(path);
      if (!replaced.equals(path)) {
        return replaced;
      }
    }
    return path;
  }

  /**
   * If true, then this aggregator will run even if there were no models produced by the model producer.
   * Default = false;
   * @return runWithoutModels value
   */
  public boolean isRunWithoutModels() {
    return runWithoutModels;
  }

  public void setRunWithoutModels(boolean runWithoutModels) {
    this.runWithoutModels = runWithoutModels;
  }
}
