package com.giantheadsoftware.fmgen.model;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.giantheadsoftware.fmgen.ParameterNames;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author clininger $Id: $
 */
public class LinkedListModel extends LinkedList<Object> implements Model {

  private final Map<String, Object> pluginParams;
  private String targetFileName;
  private String template;

  public LinkedListModel(String targetFileName, Map<String, Object> pluginParams) {
    this.targetFileName = targetFileName;
    this.pluginParams = pluginParams;
  }

  public LinkedListModel(String targetFileName, Map<String, Object> pluginParams, List<Object> src) {
    this(targetFileName, pluginParams);
    addAll(src);
  }
    
  @Override
  public String getTargetFileName() {
   return targetFileName;
  }

  public void setTargetFileName(String targetFileName) {
    this.targetFileName = targetFileName;
  }

  @Override
  public Map<String, Object> getPluginParams() {
    return pluginParams;
  }

  @Override
  public String getTemplate() {
    return template;
  }

  @Override
  public void setTemplate(String template) {
    this.template = template;
  }

  @Override
  public Map<String, Object> getModelProps() {
    return (Map<String, Object>) pluginParams.get(ParameterNames.MODEL_PROPS);
  }

}
