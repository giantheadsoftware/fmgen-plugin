package com.giantheadsoftware.fmgen.model;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.giantheadsoftware.fmgen.ParameterNames;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author clininger $Id: $
 */
public class LinkedMapModel extends LinkedHashMap<String, Object> implements Model {

  public LinkedMapModel(Map<String, Object> pluginParams) {
    put("pluginParams", pluginParams);
    put(ParameterNames.MODEL_PROPS, pluginParams.get(ParameterNames.MODEL_PROPS));
  }

  public LinkedMapModel(Map<String, Object> pluginParams, Map<String, Object> source) {
    this(pluginParams);
    putAll(source);
  }

  @Override
  public String getTargetFileName() {
   return (String) get("targetFileName");
  }

  public void setTargetFileName(String targetFileName) {
    put("targetFileName", targetFileName);
  }

  @Override
  public Map<String, Object> getPluginParams() {
    return (Map<String, Object>) get("pluginParams");
  }

  @Override
  public String getTemplate() {
    return (String) get("template");
  }

  @Override
  public void setTemplate(String template) {
    put("template", template);
  }

  @Override
  public Map<String, Object> getModelProps() {
    return (Map<String, Object>) get(ParameterNames.MODEL_PROPS);
  }

}
