package com.giantheadsoftware.fmgen.model;

import com.giantheadsoftware.fmgen.Conventions;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A model is not required to implement the Model interface, but there are some values
 * that the model should be able to produce.  This wrapper attempts to normalize the various ways
 * that these values could be accessed
 * @author clininger $Id: $
 */
public class ProducerWrapper implements ModelProducer {

  private final Object producerish;

  public ProducerWrapper(Object modelish) {
    this.producerish = modelish;
  }

  @Override
  public Map<String, ? extends Model> getModels() throws MojoExecutionException, MojoFailureException {
    if (producerish instanceof ModelProducer) {
      return ((ModelProducer)producerish).getModels();
    }
    try {
      Method method = producerish.getClass().getMethod(Conventions.ProducerMethods.GET_MODELS_METHOD);
      if (method == null) {
        throw new IllegalArgumentException("Model class "+producerish.getClass().getName()+" has no method named "
            + Conventions.ProducerMethods.GET_MODELS_METHOD);
      }
      return (Map)method.invoke(producerish);
    } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException | SecurityException x) {
      throw new IllegalArgumentException("Couldn't invoke method "+Conventions.ProducerMethods.GET_MODELS_METHOD+" on model class "
          + producerish.getClass().getName(), x);
    }
  }
}
