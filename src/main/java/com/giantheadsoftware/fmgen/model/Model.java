package com.giantheadsoftware.fmgen.model;

import com.giantheadsoftware.fmgen.FilenameFormat;
import com.giantheadsoftware.fmgen.util.Util;

import java.util.Map;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Interface that a top-level model should implement
 * @author clininger $Id: $
 */
public interface Model {

  /**
   * @return the filename to be generated, relative to the genDir
   */
  String getTargetFileName();

  /**
   * Produce the file name in the specified format
   * @param filenameFormat "SLUG" or "CAMEL"
   * @return formatted file name
   */
  default String getTargetFileNameInFormat(String filenameFormat) {
    try {
      FilenameFormat format = FilenameFormat.valueOf(filenameFormat);
      String defaultFileName = getTargetFileName();
      switch (format) {
        case SLUG:
          return Util.fileToSlug(defaultFileName);
        case CAMEL:
        default:
          return Util.fileToCamel(defaultFileName);
      }
    } catch (IllegalArgumentException x) {
      return getTargetFileName();
    }
  }

  /**
   * @return the plugin parameters
   */
  Map<String, Object> getPluginParams();

  /**
   * @return The modelProps set in the config
   */
  Map<String, Object> getModelProps();

  /**
   * @return the base name of the template, no path or extension
   */
  String getTemplate();

  void setTemplate(String template);
}
