package com.giantheadsoftware.fmgen.model.json;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static com.giantheadsoftware.fmgen.ParameterNames.*;
import com.giantheadsoftware.fmgen.model.LinkedListModel;
import com.giantheadsoftware.fmgen.model.LinkedMapModel;
import com.giantheadsoftware.fmgen.model.Model;
import com.giantheadsoftware.fmgen.model.ModelProducer;
import com.giantheadsoftware.fmgen.util.ModelPackageReader.JsonFileFilter;
import com.giantheadsoftware.fmgen.util.ModelSourceFinder;
import com.giantheadsoftware.fmgen.util.ModelSource;
import com.github.cliftonlabs.json_simple.JsonArray;
import com.github.cliftonlabs.json_simple.JsonException;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsonable;
import com.github.cliftonlabs.json_simple.Jsoner;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;

/**
 *
 * @author clininger $Id: $
 */
public class JsonProducer implements ModelProducer {
  private final Map<String, Object> pluginParams;
  private final Log log;
  private final String modelBasePath;
  private final String modelPathIncludePatternStr;
  private final String modelPathExcludePatternStr;
  private final String template;
  private final Pattern includeModelPattern;
  private final Pattern excludeModelPattern;

  public JsonProducer(Map<String, Object> pluginParams) {
    this.pluginParams = pluginParams;
    this.log = (Log) pluginParams.get(LOG);
    modelBasePath = (String) pluginParams.get(MODEL_BASE_PATH);
    modelPathIncludePatternStr = (String) pluginParams.get(MODEL_PATH_INCLUDE_PATTERN);
    modelPathExcludePatternStr = (String) pluginParams.get(MODEL_PATH_EXCLUDE_PATTERN);
    template = (String) pluginParams.get(TEMPLATE);
    includeModelPattern = (Pattern) pluginParams.get(INCLUDE_MODEL_PATTERN);
    excludeModelPattern = (Pattern) pluginParams.get(EXCLUDE_MODEL_PATTERN);
  }
    
  @Override
  public Map<String, Model> getModels() throws MojoExecutionException, MojoFailureException {
    Map<String, Model> models = new HashMap<>();
    
    try {
      List<ModelSource> jsonSources = new ModelSourceFinder(pluginParams).getModelSources(new JsonFileFilter(modelBasePath, modelPathIncludePatternStr, modelPathExcludePatternStr, includeModelPattern, excludeModelPattern));
      
      jsonSources.stream()
          .map(this::readJson)
          .filter(jsonEntry -> jsonEntry != null)
          .forEach(jsonEntry -> {
            if (jsonEntry.getValue() instanceof JsonObject) {
              // if the top-level element in the JSON is an object with a "models" entry which is a JsonArray, use that array to define multiple models from the same file
              Object multiModel = ((JsonObject)jsonEntry).get("models");
              if (multiModel != null && multiModel instanceof JsonObject) {
                ((JsonObject)multiModel).entrySet().stream()
                    .forEach(jsonMapEntry -> putModel(models, jsonMapEntry.getKey(), (Jsonable) jsonMapEntry.getValue()));
              } else {
                // there's no multimodel list, so just take the top-level object as the model
                putModel(models, jsonEntry.getKey(), (Jsonable) jsonEntry.getValue());
              }
            } else if (jsonEntry.getValue() instanceof Jsonable) {
              putModel(models, jsonEntry.getKey(), (Jsonable) jsonEntry.getValue());
            }
          });
          
    } catch (IOException ex) {
      throw new MojoExecutionException("Failed to load JSON files", ex);
    }
    return models;
  }

  private Map.Entry<String, Object> readJson(ModelSource ms) {
    try {
      return new SimpleEntry(ms.name, Jsoner.deserialize(new InputStreamReader(ms.getInputStream())));
    } catch (JsonException x) {
      log.error("Unable to deserialize JSON from "+ms.toString(), x);
      return null;
    }
  }
  
  /**
   * Put the model onto the map.  Create the appropriate model type, Map or List, based on the JSON model.
   * @param map
   * @param targetFileName
   * @param jsonModel 
   */
  private void putModel(Map<String, Model> map, String targetFileName, Jsonable jsonModel) {
    if (jsonModel instanceof JsonObject) {
      LinkedMapModel model = new LinkedMapModel(pluginParams, (Map) jsonModel);
      model.setTargetFileName(targetFileName);
      model.setTemplate(template);
      map.put(targetFileName, model);
    } else if (jsonModel instanceof JsonArray) {
      LinkedListModel model = new LinkedListModel(targetFileName, pluginParams, (List) jsonModel);
      model.setTargetFileName(targetFileName);
      model.setTemplate(template);
      map.put(targetFileName, model);
    }
  }
}
