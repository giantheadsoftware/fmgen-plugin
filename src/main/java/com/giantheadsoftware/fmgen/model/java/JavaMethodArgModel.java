package com.giantheadsoftware.fmgen.model.java;

import com.giantheadsoftware.fmgen.model.java.util.AllClassMap;
import com.giantheadsoftware.fmgen.util.ToString;
import com.giantheadsoftware.fmgen.util.Util;
import java.util.stream.Collectors;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author clininger $Id: $
 */
public class JavaMethodArgModel extends JavaClassMemberModel {
  /**
   * the model on which this type is being analyzed.
   */
  private final JavaMethodModel method;

  public JavaMethodArgModel(String name, JavaTypeRef ref) {
    super(ref);
    this.method = (JavaMethodModel)ref.getScopeMember();
    this.name = name;
  }

//  @Override
//  public String getSignature() {
//    String signature = "";
//    signature += getAnnotations().isEmpty() ? "" : getAnnotations().stream().map(anno -> anno.getSignature()).collect(Collectors.joining(" "));
//    signature += getSelfRef().getName();
//    signature += " " + name;
//    return signature;
//  }

  @Override
  public AllClassMap getAllRefs() {
    return super.getAllRefs();
  }

  @Override
  public JavaClassMemberModel getScopeMember() {
    return method;
  }

  @Override
  public String toString() {
    return new ToString().toString(this, this::toString); 
  }

  public String toString(ToString ts) {
    String value = ts.newLine()+"name = " + ts.toString(name)
        + ts.newLine()+"type = " + ts.toString(getTypeRef(), (ts2) -> getTypeRef().toString(ts2))
        + ts.newLine()+"typeParams = " + ts.toString(getTypeParams(), (ts2, ref) -> ts2.toString(ref, ref::toString))
        + ts.newLine()+"annotations = " + ts.toString(getAnnotations(), (ts2, anno) -> ts2.toString(anno, anno::toString))
        + ts.newLine()+"declModel = " + getScopeModel().getFqn()
        + ts.newLine()+"declMethod = " + getScopeMember().getName()
        + ts.newLine()+"imports = " + ts.toString(getImportRefs().stream().map(ref -> ref.getFqn()).collect(Collectors.toSet()))
    ;
    return value;
  }

}
