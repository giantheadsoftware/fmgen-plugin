package com.giantheadsoftware.fmgen.model.java;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author clininger $Id: $
 */
public abstract class JavaClassMemberModel extends JavaBaseEntity implements JavaScopeEntity {

  /**
   * The declared name of the member
   */
  protected String name;

  /**
   * Reference to the class that declared this member (could be super class or interface)
   */
  private JavaTypeRef<?> declRef;

  public JavaClassMemberModel(JavaTypeRef<?> ref) {
    super(ref);
  }

  @Override
  public String getFqn() {
    return name;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public JavaClassMemberModel getScopeMember() {
    return null;
  }

  public JavaTypeRef<?> getDeclRef() {
    return declRef;
  }

  public void setDeclRef(JavaTypeRef<?> declRef) {
    this.declRef = declRef;
  }

}
