package com.giantheadsoftware.fmgen.model.java;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.giantheadsoftware.fmgen.ParameterNames;
import com.giantheadsoftware.fmgen.model.Model;
import com.giantheadsoftware.fmgen.model.java.util.AllClassMap;
import com.giantheadsoftware.fmgen.util.RegexReplace;
import com.giantheadsoftware.fmgen.util.ToString;
import com.giantheadsoftware.fmgen.util.Util;
import org.apache.maven.plugin.MojoExecutionException;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class JavaClassModel extends JavaBaseEntity implements JavaScopeEntity, Model {
  /**
   * Reference to the source class this model was derived from
   */
  private JavaTypeRef<?> srcClassRef;

  /**
   * The target package of the generated model class
   */
  private String targetPackage;
  /**
   * The fm template to be used to generate the output file
   */
  private String template;

  /**
   * The direct superclass
   */
  protected JavaTypeRef<?> superClass;
  /**
   * List of interfaces implemented by the class
   */
  protected List<JavaTypeRef<?>> interfaces = new LinkedList<>();
  /**
   * The optional base class that all model classes extend at some level in depth
   */
  protected String baseClassName;
  /**
   * The public properties of this model class
   */
  protected List<JavaMethodModel> methods = new LinkedList<>();
  /**
   * Classes inherited by this class, including superclass + interfaces
   */
  protected List<JavaTypeRef<?>> inheritClasses = new LinkedList<>();
  /**
   * The interface that defines the public API of this model class
   */
  protected JavaTypeRef<?> modelInterface;
  /**
   * Pattern that defines the "whitelist" of classes to import
   */
  private Pattern importIncludePattern = Pattern.compile(".*");  // by default, include all
  /**
   * Pattern that defines the "blacklist" of classes to import.  This overrides the whitelist.
   */
  private Pattern importExcludePattern = Pattern.compile("^$");  // by default, include none
  /**
   * List of match-replace patterns to be applied to imports
   */
  private final List<RegexReplace> importMapPatterns = new LinkedList<>();
  /**
   * Imports added by POM configuration or by the template during rendering
   */
  private final List<String> addedImports = new LinkedList<>();

  /**
   * Constructor
   * @param ref They type ref that's the basis for this model
   */
  public JavaClassModel(JavaTypeRef<?> ref) {
    super(ref);
  }

  public JavaTypeRef<?> getSrcClassRef() {
    return srcClassRef;
  }

  public void setSrcClassRef(JavaTypeRef<?> srcClassRef) {
    this.srcClassRef = srcClassRef;
  }

  public void addImports(List<String> imports) {
    addedImports.addAll(imports);
  }

  /**
   * The FQN of the class is tied to the name of the type
   * @return the FQN from the selfRef
   */
  @Override
  public String getFqn() {
    return getSelfRef().getFqn();
  }

  /**
   * The name of the class is tied to the name of the type
   * @return the name from the selfRef
   */
  @Override
  public String getName() {
    return getSelfRef().getName();
  }

	public String getBaseName() {
		return getSelfRef().getBaseName();
	}

	public String getTypeParamStr() {
		return getSelfRef().getTypeParamStr();
	}

  @Override
  public List<JavaTypeRef<?>> getTypeParams() {
    return typeVars.stream().map(JavaBaseEntity::getSelfRef).collect(Collectors.toList());
  }

  public JavaTypeRef<?> getModelInterface() {
    return modelInterface;
  }

  public String getTargetPackage() {
    return targetPackage;
  }

  @Override
  public AllClassMap getAllRefs() {
    AllClassMap refs = super.getAllRefs();
    if (srcClassRef != null)
      refs.addClassRef(srcClassRef);
    refs.addClassRefs(inheritClasses);
    refs.addClassRefs(interfaces);
    if (superClass != null)
      refs.addClassRef(superClass);
    if (modelInterface != null)
      refs.addClassRef(modelInterface);
    methods.forEach(method -> refs.addClassRefs(method.getAllRefs()));
    return refs;
  }

  public void setModelInterface(JavaTypeRef<?> modelInterface) {
    this.modelInterface = modelInterface;
  }

  public void setImportIncludePattern(String importIncludePattern) {
    this.importIncludePattern = Pattern.compile(importIncludePattern);
  }

  public void setImportExcludePattern(String importExcludePattern) {
    this.importExcludePattern = Pattern.compile(importExcludePattern);
  }

  public void setImportMapPatterns(List<String> importMapPatterns, Properties importMapProps) throws MojoExecutionException {
    this.importMapPatterns.clear();
    if (importMapPatterns != null) {
      List<MojoExecutionException> exceptions = new ArrayList<>();
      importMapPatterns.stream()
            .map(pattern -> {
              try {
                return new RegexReplace(pattern, importMapProps);
              } catch (MojoExecutionException x) {
                exceptions.add(x);
                return null;
              }
            })
            .filter(Objects::nonNull)
            .forEach(this.importMapPatterns::add);
      if (!exceptions.isEmpty()) {
        throw exceptions.get(0);  // throw the 1st exception encountered
      }
    }
  }

  public List<String> getImports() {
    if (getPluginParams() == null) {
      // call occurred before params were provided
      return Collections.emptyList();
    }
    // convert the importRefs to strings
    Set<String> importSet = getImportRefs().stream()
        .map(JavaTypeRef::getFqn)
        .collect(Collectors.toSet());
    // include the refs added by config or template
    importSet.addAll(addedImports);
    // apply pattern filters & sort the final result
    return importSet.stream()
        .map(imp -> imp.endsWith("[]") ? imp.substring(0, imp.lastIndexOf('[')) : imp)  // don't inlclude [] on array imports
        .filter(imp -> Util.checkStringPattern(imp, importIncludePattern, importExcludePattern))
          .map(this::mapImportPath)
        .sorted()
        .collect(Collectors.toList());
  }

  /**
   * @return the imported classes, class names only, in slug format
   */
  public List<String> getImportSlugPaths() {
    return getImports().stream()
          .map(Util::package2FilePath)
          .map(Util::fileToSlug)
          .collect(Collectors.toList());
  }

  /**
   * Match the path against all of the import map patterns.  If one of them alters the path, return that alteration
   * and stop processing.  If none of the patterns produce an alteration, return the original path.
   * @param path The path to map
   * @return Potentially mapped path
   */
  public String mapImportPath(String path) {
    for (RegexReplace rr : importMapPatterns) {
      String replacement = rr.replace(path);
      if (!replacement.equals(path)) {
        return replacement;
      }
    }
    return path;
  }

  public JavaTypeRef<?> getSuperClass() {
    return superClass;
  }

  public String getBaseClassName() {
    return baseClassName;
  }

  public List<JavaTypeRef<?>> getInterfaces() {
    return interfaces;
  }

  public List<JavaMethodModel> getMethods() {
    return methods;
  }

  /**
   * Change the package where the model will be written.  Also changes the typeRef.
   * @param targetPackage the package where the model will be written.
   */
  public void setTargetPackage(String targetPackage) {
    this.targetPackage = targetPackage;
    this.typeRef.setTargetPackage(targetPackage);
  }

  @Override
  public void setTemplate(String template) {
    this.template = template;
  }

  /**
   * @return true if any methods in this class return collections or maps, or has collection or map args
   */
  public boolean hasContainerMembers() {
    for (JavaMethodModel method : getMethods()) {
      if (method.getTypeRef().isContainer() || method.hasContainerArgs()) {
        return true;
      }
    }
    return false;
  }

  @Override
  public String getTargetFileName() {
    return (targetPackage != null ? targetPackage.replace(".", "/")+'/'+typeRef.getName()+getPluginParams().get(ParameterNames.TARGET_FILE_EXT) :null);
  }

  @Override
  public String getTemplate() {
    return template;
  }

  @Override
  public Map<String, Object> getModelProps() {
    return (Map<String, Object>) getPluginParams().get(ParameterNames.MODEL_PROPS);
  }

  public String toSlugCase(String name) {
    return Util.toSlug(name);
  }

  public String toCamelCase(String name) {
    return Util.toCamel(name);
  }

  private boolean inToString;

  @Override
  public String toString() {
    if (inToString) {
      return getClass().getSimpleName()+ "{ (circular reference)"
          + "\nname=" + getName()
          + "\nfullName=" + getFqn()
          + "\n}";
    }
    inToString = true;
    String value = new ToString().toString(this, this::toString);
    inToString = false;
    return value;
  }

  public String toString(ToString ts) {
      return ts.newLine()+"name = " + ts.toString(getName())
          +ts.newLine()+"srcClassRef = " + ts.toString(getSrcClassRef(), ts2 -> (getSrcClassRef() != null ? getSrcClassRef().toString(ts2) : "null"))
          +ts.newLine()+"selfRef = " + ts.toString(getSelfRef(), getSelfRef()::toString)
          +ts.newLine()+"superClass = " + ts.toString(getSuperClass(), ts2 -> (getSuperClass() != null ? getSuperClass().toString(ts2) : "null"))
          +ts.newLine()+"baseClassName = " + ts.toString(getBaseClassName())
          +ts.newLine()+"interfaces = " + ts.toString(getInterfaces(), (ts2, intf) -> ts2.toString(intf, intf::toString))
          +ts.newLine()+"typeParams = " + ts.toString(getTypeParams(), (ts2, param) -> ts2.toString(param, param::toString))
          +ts.newLine()+"modelInterface = " + ts.toString(getModelInterface(), ts2 -> (getModelInterface() != null ? getModelInterface().toString(ts2) : "null"))
          +ts.newLine()+"methods = " + ts.toString(getMethods(), (ts2, method) -> ts2.toString(method, method::toString))
          +ts.newLine()+"hasContainerMembers() = " + ts.toString(hasContainerMembers())
          +ts.newLine()+"targetFileName = " + ts.toString(getTargetFileName())
          +ts.newLine()+"targetPacakge = " + ts.toString(getTargetPackage())
          +ts.newLine()+"template = " + ts.toString(getTemplate())
          +ts.newLine()+"imports = " + ts.toString(getImports())
          +ts.newLine()+"allRefs = " + ts.toString(getAllRefs());
  }

  @Override
  public Map<String, Object> getPluginParams() {
    return super.getPluginParams();
  }

}
