package com.giantheadsoftware.fmgen.model.java;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.giantheadsoftware.fmgen.model.java.util.AllClassMap;
import com.giantheadsoftware.fmgen.util.ToString;
import com.giantheadsoftware.fmgen.util.Util;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
/**
 *
 * @author clininger
 * $Id: $
 */
public class JavaAnnotationModel extends JavaClassModel {
  private final Annotation annotation;
  private final Class<? extends Annotation> annotationClass;
  private final Map<String, Object> values = new LinkedHashMap<>();

  public JavaAnnotationModel(JavaTypeRef ref, Annotation annotation) {
    super(ref);
    this.annotation = annotation;
    annotationClass = annotation.annotationType();
    scanAnnotationValues();
  }

  public Map<String, Object> getValues() {
    return values;
  }

  private void scanAnnotationValues() {

    for (Method method : annotation.annotationType().getDeclaredMethods()) {
      try {
        if (method.getParameterCount() > 0 || Void.class == method.getReturnType()) {
          continue;
        }
        values.put(method.getName(), method.invoke(annotation));
      } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException x) {
        throw new IllegalArgumentException(MessageFormat.format("Could not get values for Annotation type {0}, method {1}"
              , annotationClass.getName(), method.getName(), x));
      }
    }
  }

//  @Override
//  public String getSignature() {
//    String signature = '@'+name;
//    if (!values.isEmpty()) {
//      signature += '(';
//      signature += values.entrySet().stream().map(e -> e.getKey() + " = " + e.getValue()).collect(Collectors.joining(", "));
//      signature += ')';
//    }
//    return signature;
//  }


  @Override
  public AllClassMap getAllRefs() {
    return super.getAllRefs();
    // TODO: handle type refs in values
  }

  @Override
  public String toString() {
    String value = "DtoAnnotation{"
        + "\ntypeRef=" + super.toString()
          + ",\nfqn=" + getFqn()
        + ",\nvalues=" + Util.collectionToString(values.entrySet().stream().map(
              e -> e.getKey() + " = " + String.valueOf(e.getValue()))
            .collect(Collectors.toList()))
        + ",\ntypeParams=" + Util.collectionToString(getTypeParams().stream().map(
            t -> t.getFqn()).collect(Collectors.toList()))
        + "\n}";
    return value;
  }
  @Override
  public String toString(ToString ts) {
    return super.toString(ts)
        + ts.newLine()+"values = "+ts.toString(values);
  }
}
