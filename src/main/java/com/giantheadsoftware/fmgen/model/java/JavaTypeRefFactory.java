package com.giantheadsoftware.fmgen.model.java;

import com.giantheadsoftware.fmgen.ParameterNames;
import com.giantheadsoftware.fmgen.util.RegexReplace;
import com.giantheadsoftware.fmgen.util.Util;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author clininger $Id: $
 */
public class JavaTypeRefFactory implements ParameterNames {

  private final Map<String, Object> params;
  private final Properties properties;
//  private final String targetPackage;
  /* pattern used to identify class names suitable for mapping */
  protected final RegexReplace classNameMapper;
  protected final RegexReplace interfaceNameMapper;
  private final Set<String> modelClassNames = new HashSet<>();
  private final Log log;

  public JavaTypeRefFactory(Map<String, Object> params, Properties properties, Log log) throws MojoExecutionException {
    this.params = params;
    this.properties = properties;
    this.log = log;
//    targetPackage = properties.getProperty(JavaPojoProducer.TARGET_PACKAGE);
    classNameMapper = new RegexReplace(properties.getProperty(CLASSNAME_MAP), properties);
    interfaceNameMapper = new RegexReplace(properties.getProperty(IFC_CLASSNAME_MAP), properties);
  }

//  /**
//   * If a classNameMap is provided, use it to create a regex matching pattern and a string replace template.
//   *
//   * @param map Pattern used to map source -> dest class names
//   * @return a regex pattern used to match source class names.
//   * @throws MojoExecutionException
//   */
//  private Pattern initClassNamePattern(String map) throws MojoExecutionException {
//    if (map == null || map.isEmpty()) {
//      return null;
//    }
//    return pattern;
//  }
//
//  private Pattern initInterfaceNamePattern(String map) {
//    if (map == null || map.isEmpty()) {
//      return null;
//    }
//    log.debug("Interface replace-pattern: " + map);
//    String delim = map.substring(0, 1);
//    String[] args = map.substring(1).split(delim);
//    log.debug("ARGS:  " + Arrays.toString(args));
//    Pattern pattern = Pattern.compile(args[0]);
//    interfaceNameReplace = args[1];
//    return pattern;
//  }

  public void addModelClasses(Collection<Class<?>> modelClasses) {
    modelClasses.stream().map(mc -> mc.getName()).forEach(modelClassNames::add);
  }

  /**
   * Initialize common parameters
   * @param ctx contains
   */
  private void setJtrProps(CreateContext<?> ctx) {
    if (ctx.ref.getOrigFqn() == null)
      ctx.ref.setOrigFqn(ctx.type.getTypeName());
    ctx.ref.setFqn(mapClassName(ctx.ref.getOrigFqn()));
    ctx.ref.setBaseName(ctx.ref.getFqn());
    ctx.ref.setInModel(modelClassNames.contains((ctx.ref.getOrigFqn())));
//    if (ctx.model != null)
//      ctx.model.addClassRef(ctx.ref);
    computeModelInterface(ctx.ref);
  }

  /**
   * Create a reference to the type that will be the model.
   * The main difference between this and another type reference is that there's no scope entity for a model ref
   * @param t the model's type
   * @return A reference to the model type
   */
  public JavaTypeRef<?> createModelRef(Type t) {
    if (t == null)
      return null;
    CreateContext<?> ctx = new CreateContext(t, null);
    return getOrCreateContext(t, null).ref;
  }

  /**
   * Create a reference to the type provided.  If a reference to this type already exists (determined by the specific generic signature)
   * then that pre-existing ref will be returned.
   * @param t the type to reference
   * @param scopeEntity the class or member entity that is making the reference
   * @return the type reference
   */
  public JavaTypeRef createTypeRef(Type t, JavaScopeEntity scopeEntity) {
    if (t == null)
      return null;
    if (scopeEntity == null) {
      throw new IllegalArgumentException("Scope entity required");
    }
    return getOrCreateContext(t, scopeEntity).ref;
  }

  private CreateContext<?> getOrCreateContext(Type t, JavaScopeEntity scopeEntity) {
    CreateContext<Type> ctx = new CreateContext(t, scopeEntity);
    return buildTypeRef(ctx);
  }

  private CreateContext<?> buildTypeRef(CreateContext<Type> ctx) {
    log.debug("Creating new instance of " + ctx.signature);
    if (ctx.type instanceof TypeVariable)
      createVariableRef(ctx);
    else if (ctx.type instanceof WildcardType)
      createWildcardRef(ctx);
    else if (ctx.type instanceof ParameterizedType)
      createParameterizedTypeRef(ctx);
    else if (ctx.type instanceof Class)
      createClassRef(ctx);
    else
      throw new UnsupportedOperationException("Type not supported: " + (ctx.type != null ? ctx.type.getClass().getName() : "null"));
    log.debug("Created ref: " + Util.indentStruct(ctx.ref.toString()));
    return ctx;
  }

  /**
   * Create a descriptor for a simple, non-parameterized class type
   * @param ctx current context
   * @return JavaTypeRef for the class
   */
  private CreateContext<?> createClassRef(CreateContext<?> ctx) {
    Class<?> c = (Class<?>)ctx.type;
    setJtrProps(ctx);
//    ctx.ref.setOrigFqn(c.getName());
    log.debug("   BaseName: " + ctx.ref.getFqn() + "; typename: " + c.getTypeName());
    return ctx;
  }

  /**
   * Create a descriptor for a parameterized type.  Type refs are created for all generic params.
   * @param ctx current context
   * @return JavaTypeRef for the parameterized type
   */
  private CreateContext<?> createParameterizedTypeRef(CreateContext<Type> ctx) {
    ParameterizedType pt = (ParameterizedType)ctx.type;
    ctx.ref.setOrigFqn(pt.getRawType().getTypeName());
    setJtrProps(ctx);
    log.debug("   BaseName: " + ctx.ref.getFqn() + "; typename: " + pt.getTypeName());

    // recursively create a ref for the raw type
    ctx.ref.setRawTypeRef(buildTypeRef(ctx.copyWithType(pt.getRawType())).ref);

    // creat refs for all of the params
    for (Type typeParam : pt.getActualTypeArguments()) {
      log.debug("   Actual Type Param: " + typeParam.getTypeName());
      ctx.ref.getTypeParams().add(buildTypeRef(ctx.copyWithType(typeParam)).ref);
    }
    return ctx;
  }

  /**
   * Create a descriptor for a type variable.  Type refs are created for all bounds.
   * @param ctx current context
   * @return JavaTypeRef for the type variable
   */
  private CreateContext<?> createVariableRef(CreateContext<Type> ctx) {
    TypeVariable<?> tv = (TypeVariable<?>)ctx.type;
    JavaTypeVar jtv = new JavaTypeVar(ctx.ref);
    jtv.name = tv.getName();
    ((JavaTypeRef<JavaTypeVar>)ctx.ref).setReferencedEntity(jtv);
    ctx.ref.setBaseName(jtv.name);
    ctx.ref.setFqn(jtv.name);
    if (ctx.ref.getScopeModel() != null) {
      // determine if this variable has been previously declared at the class level
      log.debug("   ...looking for type " + jtv.name + " in model " + ctx.ref.getScopeModel().getFqn());
      for (JavaTypeVar typeVar : ctx.ref.getScopeModel().typeVars) {
        if (typeVar.getName().equals(jtv.name)) {
          jtv.setTypeVarRef(typeVar);
          log.debug("   class-level type parameter");
          break;
        }
      }
      // if not defined at class level, determine whether the var was previously defined at the property level
      if (!jtv.isTypeVarRef() && ctx.ref.getScopeMember() != null) {
        log.debug("   ...looking for type " + jtv.name + " in member " + ctx.ref.getScopeModel().getOrigFqn() + '.' + ctx.ref.getScopeMember().name);
        for (JavaTypeVar typeVar : ctx.ref.getScopeMember().getTypeVars()) {
          if (typeVar.name.equals(jtv.name)) {
            jtv.setTypeVarRef(typeVar);
            log.debug("   property-level type parameter");
            break;
          }
        }
      }
//      jtv.member = ctx.member;
      if (!jtv.isTypeVarRef()) {
        if (ctx.ref.getScopeMember() != null)
          ctx.ref.getScopeMember().addTypeVar(jtv);
        else
          ctx.ref.getScopeModel().addTypeVar(jtv);
        for (Type bound : tv.getBounds()) {
          log.debug("   bound: " + bound.getTypeName());
          jtv.getBounds().add(buildTypeRef(ctx.copyWithType(bound)).ref);
        }
      }
//      else {  I think this will be set automatically by setting the typeVarRef
//        // try to determine if there is an actual type arg
//        if (jtv.getTypeVarRef().hasActual()) {
//          jtv.setActual(jtv.getTypeVarRef().getActual());
//        }
//      }
    }
    return ctx;
  }

  /**
   * Create a descriptor for a wildcard type.  Type refs are created for upper & lower bounds.
   * @param ctx current context
   * @return JavaTypeRef for the type variable
   */
  private CreateContext<?> createWildcardRef(CreateContext<Type> ctx) {
    WildcardType wc = (WildcardType)ctx.type;
    ctx.ref.setFqn("?");
    ctx.ref.setBaseName("?");
    JavaTypeVar jtv = new JavaTypeVar(ctx.ref);
    jtv.name = "?";
    for (Type bound : wc.getUpperBounds()) {
      log.debug("   bound: " + bound.getTypeName());
      jtv.getBounds().add(buildTypeRef(ctx.copyWithType(bound)).ref);
    }
    for (Type bound : wc.getLowerBounds()) {
      log.debug("   bound: " + bound.getTypeName());
      jtv.getLowerBounds().add(buildTypeRef(ctx.copyWithType(bound)).ref);
    }
    return ctx;
  }

  /**
   * If the classname matches the mapping pattern, then map it. 
	 * The package is not changed, only the classname
   *
   * @return The potentially mapped classname.
   */
  private String mapClassName(final String origName) {
    if (modelClassNames.contains(origName)) {
			String pkg = origName.substring(0, origName.lastIndexOf('.')+1);
      String mappedName =  pkg + classNameMapper.replace(origName.substring(pkg.length()));
      
      if (!mappedName.equals(origName))
        log.debug("Mapped " + origName + " -> " + mappedName);
      return mappedName;
    }
    return origName;
  }

  /**
   * If an "interfacePackage" was defined in the POM, determine the FQ class name of the interface that defines this type.
   * If typeRef does not represent a class in the model, return null.
   *
   * @param typeRef Ref to JavaClassModel to find the model interface for
   * @return A TypeRef referencing the model's interface (or null if input is not a model class)
   */
  protected JavaTypeRef<?> computeModelInterface(JavaTypeRef<?> typeRef) {
    if (!(typeRef.getReferencedEntity() instanceof JavaClassModel && (typeRef).isInModel()))
      return null;
    String interfacePackage = properties.getProperty(IFC_PACKAGE);
    if (interfacePackage == null) {
      return null;
    }
    // the ifcPackage was specified in the POM.  Use that to locte the model interface
    // get the model from the ref
    JavaClassModel model = (JavaClassModel)typeRef.getReferencedEntity();
    //take the source class of that model
    Class srcClass = model.getSrcClassRef().getRawClass();
    // map the src class name based on the mapping pattern in the POM
    String mappedSrcName = interfacePackage + '.' + interfaceNameMapper.replace(srcClass.getSimpleName());
    //see if a class exists by that mapped name
    Class ifaceClass = null;
    try {
      ifaceClass = Class.forName(mappedSrcName);
    }
    catch (ClassNotFoundException x) {
      log.debug("no class found for model interface: " + ifaceClass);
    }
    JavaTypeRef<?> modelInterfaceRef = new JavaTypeRef(ifaceClass, params, properties);
    modelInterfaceRef.setFqn(mappedSrcName);
    modelInterfaceRef.setBaseName(modelInterfaceRef.getFqn());
    modelInterfaceRef.setInModel(false);
//        .modelInterface(modelInterfaceBuilder);
//    model.addClassRef(modelInterfaceBuilder);
    if (!modelInterfaceRef.isInModel())
      modelInterfaceRef.setImported(true);
    log.debug("Computed model interface: \n" + Util.indentStruct(modelInterfaceRef.toString()));
    return modelInterfaceRef;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //
  public class CreateContext<T extends Type> {

    public final T type;
//    public final JavaClassModel model;
//    public final JavaClassMemberModel member;
    public final JavaTypeRef<?> ref;
    public final String signature;

    public CreateContext(T type, JavaScopeEntity scopeEntity) {
      this(type, scopeEntity, null);
    }

    private CreateContext(T type, JavaScopeEntity scopeEntity, JavaTypeRef<?> ref) {
      this.type = type;
      // if a member scope is provided, use that to get the scope model
      this.ref = ref == null ? new JavaTypeRef<>(type, params, properties) : ref;
      this.ref.setScopeMember(scopeEntity instanceof JavaClassMemberModel ? (JavaClassMemberModel)scopeEntity : null);
      this.ref.setScopeModel(this.ref.getScopeMember() != null
            ? this.ref.getScopeMember().getScopeModel()
            : (scopeEntity instanceof JavaClassModel) ? (JavaClassModel)scopeEntity : null);
      signature = signature(type);
    }

    /**
     * Copy this context with a new type
     * @param type new type
     * @return new CreateContext instance
     */
    public CreateContext<T> copyWithType(T type) {
      return new CreateContext(type, ref.getScopeEntity());
    }

    /**
     * Copy this context with a new ref
     * @param ref new ref
     * @return new CreateContext instance
     */
    public CreateContext<T> copyWithTypeRef(JavaTypeRef<?> ref) {
      return new CreateContext(type, ref.getScopeEntity(), ref);
    }

    public final String signature(Type t) {
      if (t == null)
        throw new IllegalArgumentException("Cannot generate signature for null");
      if (t instanceof TypeVariable)
        return signature((TypeVariable<?>)t);
      if (t instanceof WildcardType)
        return signature((WildcardType)t);
      if (t instanceof ParameterizedType)
        return signature((ParameterizedType)t);
      if (t instanceof Class)
        return signature((Class<?>)t);
      throw new IllegalArgumentException("Cannot generate signature for " + t.getTypeName() + " of type " + t.getClass().getName());
    }

    public final String signature(Class<?> c) {
      return mapClassName(c.getName());
    }

    public String signature(ParameterizedType pt) {
      StringBuilder sb = new StringBuilder(mapClassName(pt.getRawType().getTypeName()));
      if (pt.getActualTypeArguments().length > 0) {
        sb.append('<');
        boolean first = true;
        for (Type actualArg : pt.getActualTypeArguments()) {
          sb.append(first ? "" : ", ").append(signature(actualArg));
          first = false;
        }
        sb.append('>');
      }
      return sb.toString();
    }

    public final String signature(TypeVariable<?> tv) {
      String sig = prefix() + tv.getTypeName();
      if (tv.getBounds().length > 0) {
        sig += " extends ";
        sig += Arrays.asList(tv.getBounds()).stream().map(t -> t.getTypeName()).collect(Collectors.joining(" | "));
      }
      return sig;
    }

    public final String signature(WildcardType wct) {
      String sig = prefix() + wct.getTypeName();
      if (wct.getUpperBounds().length > 0) {
        sig += " extends ";
        sig += Arrays.asList(wct.getUpperBounds()).stream().map(t -> t.getTypeName()).collect(Collectors.joining(" | "));
      }
      if (wct.getLowerBounds().length > 0) {
        sig += " super ";
        sig += Arrays.asList(wct.getLowerBounds()).stream().map(t -> t.getTypeName()).collect(Collectors.joining(" | "));
      }
      return sig;
    }

    private String prefix() {
      return (ref.getScopeModel() != null ? ref.getScopeModel().getOrigFqn() : "<>") + '.'
            + (ref.getScopeMember() != null ? ref.getScopeMember().name : "<>") + '.';
    }
  }
}
