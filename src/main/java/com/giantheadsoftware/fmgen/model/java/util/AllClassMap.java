package com.giantheadsoftware.fmgen.model.java.util;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.giantheadsoftware.fmgen.model.java.JavaTypeRef;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.TreeMap;
import java.util.stream.Collectors;
/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author clininger
 * $Id: $
 */
public class AllClassMap extends TreeMap<String, TypeRefList> {

  /**
   * Map the ref by its FQN
   * @param ref ref to add to the map
   */
  public void addClassRef(JavaTypeRef<?> ref) {
    if (ref == null || ref.getFqn() == null) {
      throw new IllegalArgumentException("can't add null ref");
    }
    if (containsKey(ref.getFqn())) {
      get(ref.getFqn()).add(ref);
    } else {
      TypeRefList list = new TypeRefList(ref);
      list.add(ref);
      put(ref.getFqn(), list);
    }
  }

  /**
   * map all of these refs by their FQNs
   * @param refs refs to map
   */
  public void addClassRefs(Collection<JavaTypeRef<?>> refs) {
    refs.forEach(this::addClassRef);
  }

  /**
   * map all of these refs by their FQNs
   * @param refs  refs to map
   */
  public void addClassRefs(AllClassMap refs) {
    refs.values().forEach(refList -> addClassRefs(refList.getList()));
  }

  /**
   * @return all of the Type Refs in this map
   */
  public Collection<JavaTypeRef<?>> getAllRefs() {
    return values().stream()
          .map(TypeRefList::getList)
          .flatMap(Collection::stream)
          .collect(Collectors.toSet());
  }

  public Type getType(String fqn) {
    TypeRefList list = get(fqn);
    return list != null ? list.getType() : null;
  }

  public boolean isImported(String fqn) {
    TypeRefList list = get(fqn);
    return list != null && list.isImported();
  }

  @Override
  public String toString() {
    return "[\n" + entrySet().stream()
        .map(entry -> entry.getKey() + "("+entry.getValue().getList().size()+")<"+(isImported(entry.getKey()) ? "I" : "")+">")
        .collect(Collectors.joining("\n")) + "\n]";
  }
}
