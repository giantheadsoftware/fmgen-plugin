package com.giantheadsoftware.fmgen.model.java;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.giantheadsoftware.fmgen.util.Util;
import org.apache.maven.plugin.MojoExecutionException;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author clininger $Id: $
 */
public class JavaGetterProducer extends JavaPojoProducer {
  private static final Pattern GETTER_PATTERN = Pattern.compile("^(get|is)([A-Z].*)$");
  public JavaGetterProducer(Map<String, Object> params) throws MojoExecutionException {
    super(params);
  }

  @Override
  protected JavaPojoModel scanModel(Class<?> dtoClass) throws MojoExecutionException {
    log.debug("!@!");
    log.debug("SCANNING CLASS " + dtoClass.getName());
    JavaPojoModel model = scanClass(dtoClass);

    // examine the public methods
    Method[] methods = dtoClass.getMethods();
//		dtoClass.getMethods();
    // have to sort them because order is not guaranteed
    Arrays.sort(methods, new Comparator<Method>() {
      @Override
      public int compare(Method o1, Method o2) {
        return o1.getName().compareTo(o2.getName());
      }
    });
    log.debug("Adding properties: ");
    log.debug("\tinclude, exclude member patterns: "+includeMemberPattern + ", " +excludeMemberPattern);
    log.debug("\tinclude, exclude type patterns: "+includeTypePattern + ", " +excludeTypePattern);
    log.debug("\tinclude, exclude member anno patterns: "+includeMemberAnnotationPattern + ", " +excludeMemberAnnotationPattern);
//    log.info("\tinclude, exclude arg type patterns: "+includeArgTypePattern + ", " +excludeArgTypePattern);
    for (Method method : methods) {
      String methodName = method.getName();
      if (Modifier.isPublic(method.getModifiers())
            && Util.checkStringPattern(methodName, includeMemberPattern, excludeMemberPattern)
            && Util.checkStringPattern(method.getReturnType().getName(), includeTypePattern, excludeTypePattern)
            && Util.checkAnnotations(method.getAnnotations(), includeMemberAnnotationPattern, excludeMemberAnnotationPattern)
            && Util.checkParamTypes(method.getParameters(), includeArgTypePattern, excludeArgTypePattern)) {
        Matcher m = GETTER_PATTERN.matcher(methodName);
        if (m.find()) {
          String propertyName = m.group(2);
          propertyName = propertyName.substring(0, 1).toLowerCase() + propertyName.substring(1);
          addProperty(model, propertyName, method.getReturnType(), method.getGenericReturnType(), method.getAnnotations(), method.getTypeParameters(),
              method.getDeclaringClass());
        }
      } else {
        log.debug("Excluding property "+methodName);
      }
    }
    return model;
  }
}
