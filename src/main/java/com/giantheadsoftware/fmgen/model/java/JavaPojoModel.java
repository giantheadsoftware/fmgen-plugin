package com.giantheadsoftware.fmgen.model.java;

import com.giantheadsoftware.fmgen.model.java.util.AllClassMap;
import com.giantheadsoftware.fmgen.util.ToString;
import java.util.LinkedList;
import java.util.List;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Model for Java classes
 *
 * @author clininger
 */
public class JavaPojoModel extends JavaClassModel {

  /**
   * The public properties of this model class
   */
  public List<JavaPropertyModel> properties = new LinkedList<>();

  public JavaPojoModel(JavaTypeRef ref) {
    super(ref);
  }

  public List<JavaPropertyModel> getProperties() {
    return properties;
  }


  /**
   * @return true if any properties in this class have collection or map types
   */
  @Override
  public boolean hasContainerMembers() {
//    if (super.hasContainerMembers()) {
//      return true;
//    }
    for (JavaPropertyModel property : getProperties()) {
      if (property.getTypeRef().isContainer()) {
        return true;
      }
    }
    return false;
  }

  @Override
  public AllClassMap getAllRefs() {
    final AllClassMap refs = super.getAllRefs();
    properties.stream().forEach(property -> refs.addClassRefs(property.getAllRefs()));
    return refs;
  }

  @Override
  public String toString() {
    return new ToString().toString(this, this::toString); 
  }
  
  @Override
  public String toString(ToString ts) {
    String value = super.toString(ts)
        + ts.newLine()+"properties = " + ts.toString(getProperties(), (ts2, prop) -> ts2.toString(prop, prop::toString));
    return value;
  }

}
