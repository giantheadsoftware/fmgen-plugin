package com.giantheadsoftware.fmgen.model.java;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Map;
import org.apache.maven.plugin.MojoExecutionException;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author clininger $Id: $
 */
abstract class JavaPojoProducer extends JavaClassProducer {

  /**
   * Constructor.
   *
   * @param params Receives the parameter values defined by the mojo and provided in the POM
   * @throws MojoExecutionException
   */
  public JavaPojoProducer(Map<String, Object> params) throws MojoExecutionException {
    super(params);
    if (params == null) {
      throw new MojoExecutionException("Plugin params must be provided to the producer");
    }
  }

  @Override
  protected JavaPojoModel createModel(JavaTypeRef ref) {
    return new JavaPojoModel(ref);
  }

  protected void addProperty(JavaPojoModel model,
      String name,
      Class type,
      Type genericType,
      Annotation[] annotations,
      TypeVariable[] typeVars,
      Class declaringClass) {
    log.debug("\nAdding property " + name);
    JavaPropertyModel property = new JavaPropertyModel(typeFactory.createTypeRef(genericType, model));
    property.name = name;
    if (property.typeRef.isInModel()) {
      property.typeRef.setTargetPackage(computeTargetPackage(property.typeRef));
    }
    if (property.typeRef.isParameterized()) {
      property.typeRef.getTypeParams()
          .forEach(paramRef -> paramRef.setTargetPackage(computeTargetPackage(paramRef)));
    }
    property.setDeclRef(model.getSelfRef());
    // locate the class ref for the declaring class
    // TODO:  is the following stll needed?
//    if (model.getOrigFqn().equals(declaringClass.getName())) {
//      property.declaringRef = model;
//    } else {
//      property.declaringRef = model.inheritClasses.stream()
//          .filter(t -> ((JavaPojoModel) t).origTypeName.equals(declaringClass.getName()))
//          .findFirst()
//          .get();
//    }
    // get the type vars declared on this property
    property.addTypeVars(scanTypeVars(typeVars, property));

    // do if the type is a collection/map...
    setDefaultContainer(property.getSelfRef());

    // property-level annotations
    property.addAnnotations(scanAnnotations(annotations, property));

    property.getAllRefs().values().stream()
        .flatMap(trl -> trl.getList().stream())
//        .filter(ref -> !ref.isInModel())
        .forEach(ref -> ref.setImported(true));
    model.properties.add(property);
  }

}
