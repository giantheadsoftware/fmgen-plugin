package com.giantheadsoftware.fmgen.model.java;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.giantheadsoftware.fmgen.model.java.util.AllClassMap;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
/**
 *
 * @author clininger $Id: $
 */
public interface JavaEntity {

  /**
   * @return reference representing this entity
   */
  JavaTypeRef getSelfRef();
  /**
   * @return reference representing this entity
   */
  default JavaTypeRef getTypeRef() {
    return getSelfRef();
  }

  default Map<String, Object> getPluginParams() {
    return getSelfRef().getPluginParams();
  }

  default Type getType() {
    return getSelfRef().getType();
  }

  default Class getRawClass() {
    return getSelfRef().getRawClass();
  }

  /**
   * @return The fully qualified name of the entity
   */
  default String getFqn() {
    return getSelfRef().getFqn();
  }

  /**
   * Then name which is likely to be used in the FTL template.
   * @return the "processed" name of the entity.  This may have been altered from the original, fully qualified name.
   */
  default String getName() {
    return getSelfRef().getName();
  };

  /**
   * @return The original, fully qualified name
   */
  default String getOrigFqn() {
    return getSelfRef().getOrigFqn();
  };

  /**
   * @return the declared modifiers (public/private, abstract, etc.)
   */
  default int getModifiers() {
    return getSelfRef().getModifiers();
  }

  /**
   * @return true if the abstract modifier is present
   */
  default boolean isAbstract() {
    return getSelfRef().isAbstract();
  }

  /**
   * @return the strings associated with the modifiers
   */
  default List<String> getModifierLabels() {
    return getSelfRef().getModifierLabels();
  }

//  /**
//   * @return the signature of the entity, often used for declarations
//   */
//  String getSignature();

  /**
   * @return List of annotation refs associated with the entity.
   */
  List<JavaAnnotationModel> getAnnotations();

  /**
   * Get a specific annotation by type.  TODO:  handle repeated annotations
   * @param fqn type of annotation to get
   * @return annotation or null
   */
  JavaAnnotationModel getAnnotation(String fqn);

  /**
   * If the type of this entity is parameterized, get those params
   * @return list of type params, or empty list
   */
  default List<? extends JavaTypeRef> getTypeParams() {
    return getSelfRef().getTypeParams();
  }

  /**
   * If this class is parameterized, return the type vars.  This is less generic than the getTypeParams() call.
   * @return type variables declared for this class
   */
   List<JavaTypeVar> getTypeVars();

  /**
   * A concrete declaration is one that uses the generated implementation class rather than its defining interface.
   * @return the concrete declaration
   */
//  String getConcreteDeclaration();

  /**
   * An abstract declaration is one that uses a class's interface rather than its generated implementation.
   * @return the abstract declaration
   */
//  String getAbstractDeclaration();

  /**
   * @return the JjavaClassModel where this entity was declared.  May be null if this is a top-level model itself
   */
  default JavaClassModel getScopeModel() {
    return getSelfRef().getScopeModel();
  }

  /**
   * @return If this entity was declared within a member of a model class, this is that member
   */
  default JavaClassMemberModel getScopeMember() {
    return getSelfRef().getScopeMember();
  }

  /**
   * @return All the refs declared by this entity that may be needed to import in a generated class
   */
  AllClassMap getAllRefs();

  /**
   * @return All the refs declared by this entity that may be needed to import in a generated class
   */
  default Set<JavaTypeRef> getImportRefs() {
    return getAllRefs().values().stream()
        .filter(trList -> trList.isImported())
        .flatMap(rl -> rl.getList().stream())
        .collect(Collectors.toSet());
  };
}
