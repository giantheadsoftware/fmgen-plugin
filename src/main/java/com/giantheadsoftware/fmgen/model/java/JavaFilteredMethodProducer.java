package com.giantheadsoftware.fmgen.model.java;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.giantheadsoftware.fmgen.ParameterNames;
import com.giantheadsoftware.fmgen.util.Util;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.maven.plugin.MojoExecutionException;

/**
 *
 * @author clininger $Id: $
 */
public class JavaFilteredMethodProducer extends JavaClassProducer {


  public JavaFilteredMethodProducer(Map<String, Object> params) throws MojoExecutionException {
    super(params);
    if (params == null) {
      throw new MojoExecutionException("Plugin params must be provided to the producer");
    }
  }

  @Override
  protected JavaClassModel scanModel(Class sourceClass) throws MojoExecutionException {
    log.debug("!@!");
    log.debug("SCANNING CLASS " + sourceClass.getName());
    JavaClassModel model = scanClass(sourceClass);

    // examine the public methods
    Method[] methods = sourceClass.getMethods();
//		dtoClass.getMethods();
    // have to sort them because order is not guaranteed
    Arrays.sort(methods, new Comparator<Method>() {
      @Override
      public int compare(Method o1, Method o2) {
        return o1.getName().compareTo(o2.getName());
      }
    });
    log.info("Adding methods: ");
    log.debug("\tinclude, exclude member patterns: "+includeMemberPattern + ", " +excludeMemberPattern);
    log.debug("\tinclude, exclude type patterns: "+includeTypePattern + ", " +excludeTypePattern);
    log.debug("\tinclude, exclude member anno patterns: "+includeMemberAnnotationPattern + ", " +excludeMemberAnnotationPattern);
    log.debug("\tinclude, exclude arg type patterns: "+includeArgTypePattern + ", " +excludeArgTypePattern);
    for (Method method : methods) {
      String methodName = method.getName();
      if (Object.class != method.getDeclaringClass()
          && Util.checkStringPattern(methodName, includeMemberPattern, excludeMemberPattern)
          && Util.checkStringPattern(method.getReturnType().getName(), includeTypePattern, excludeTypePattern)
          && Util.checkAnnotations(method.getAnnotations(), includeMemberAnnotationPattern, excludeMemberAnnotationPattern)
          && Util.checkParamTypes(method.getParameters(), includeArgTypePattern, excludeArgTypePattern)) {
        JavaMethodModel methodModel = scanMethod(model, method, methodName, method.getReturnType(), method.getGenericReturnType(),
              method.getAnnotations(), method.getModifiers(), method.getTypeParameters(),
              method.getParameters(), method.getGenericParameterTypes(), method.getDeclaringClass());
        if (accept(methodModel)) {
          log.debug("\nAdding method " + methodModel.getName());
          model.getMethods().add(methodModel);
        } else {
          log.debug("\nExcluding method " + methodModel.getName());
        }
      } else {
        log.debug("\nMethod excluded" + methodName);
      }
    }
    return model;
  }

  protected boolean accept(JavaMethodModel methodModel) {
    return true;
  }
}
