package com.giantheadsoftware.fmgen.model.java;

import com.giantheadsoftware.fmgen.model.java.util.AllClassMap;
import com.giantheadsoftware.fmgen.util.Util;
import java.lang.reflect.WildcardType;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author clininger $Id: $
 */
public class JavaTypeVar extends JavaClassMemberModel {

//  public final String signature;
  private final  boolean wildcard;
  /**
   * not null if this type var is referencing a previously defined type var
   */
  private JavaTypeVar typeVarRef;
  /**
   * upper bounds of wildcard or type variable
   */
  private final List<JavaTypeRef<?>> bounds = new LinkedList<>();
  /**
   * lower bounds of wildcard or type variable
   */
  private final List<JavaTypeRef<?>> lowerBounds = new LinkedList<>();

  /**
   * If the type var has an actual class value declared, put that ref here
   */
  private JavaTypeRef<?> actual;

  public JavaTypeVar(JavaTypeRef<?> typeRef) {
    super(typeRef);
    wildcard = typeRef.getType() instanceof WildcardType;
  }

  public JavaTypeVar getTypeVarRef() {
    return typeVarRef;
  }

//  @Override
//  public String getSignature() {
//    if (hasActual())
//      return actual.getSignature();
//    JavaTypeDescriptor bound = bounds.stream().findFirst().orElse(null);
//    if (bound != null) {
//      if (isWildcard())
//        return  "? extends " + bound.getSignature();
//      else
//        return bound.getSignature();
//    } else {
//      return name;
//    }
//  }

  public String getClassName() {
    JavaTypeVar var = typeVarRef != null ? typeVarRef : this;
    if (var.hasActual())
      return var.getActual().getName();
    if (!var.bounds.isEmpty())
      return var.bounds.get(0).getFqn();
    return "unbounded";
  }

  public boolean isWildcard() {
    return wildcard;
  }

  public boolean isTypeVarRef() {
    return typeVarRef != null;
  }

  public void setTypeVarRef(JavaTypeVar typeVarRef) {
    this.typeVarRef = typeVarRef;
  }

  public List<JavaTypeRef<?>> getBounds() {
    return bounds;
  }

  public List<JavaTypeRef<?>> getLowerBounds() {
    return lowerBounds;
  }

  public boolean isInModel() {
    return isTypeVarRef() ? typeVarRef.isInModel() :
          (bounds.stream().anyMatch(bound -> bound.isInModel())
          || lowerBounds.stream().anyMatch(bound -> bound.isInModel()));
  }

  /**
   * @return true if this type var has an actual class declared
   */
  public boolean hasActual() {
    return isTypeVarRef() ? typeVarRef.hasActual() : actual != null;
  }

  /**
   * @return The actual class assigned to this variable, if any
   */
  public JavaTypeRef getActual() {
    return isTypeVarRef() ? typeVarRef.getActual() : actual;
  }

  /**
   * Default implementation returns empty list
   * @return empty list
   */
  @Override
  public List<JavaAnnotationModel> getAnnotations() {
    return Collections.EMPTY_LIST;
  }

  /**
   * Default implementation returns null;
   * @param fqn ignored
   * @return null
   */
  @Override
  public JavaAnnotationModel getAnnotation(String fqn) {
    return null;
  }

//  public String getConcreteDeclaration() {
//    return getDeclaration(true);
//  }
//
//  public String getAbstractDeclaration() {
//    return getDeclaration(false);
//  }


//  /**
//   * If this TypeVar has an actual, return a signature based on that.  Otherwise, reconstruct the type var declaration, providing the bounds.
//   * Currently, only vars with a single bound are supported.
//   * @param concrete true for a concrete declaration, false for abstract.
//   * @return The declaration of this TypeVar
//   */
//  public String getDeclaration(boolean concrete) {
//    if (hasActual())
//      return concrete ? actual.getConcreteDeclaration() : actual.getAbstractDeclaration();
//    JavaTypeDescriptor bound = bounds.stream().findFirst().orElse(null);
//    if (bound != null) {
//      if (isWildcard())
//        return concrete ? bound.getConcreteDeclaration() : "? extends " + bound.getAbstractDeclaration();
//      else
//        return concrete ? bound.getConcreteDeclaration() : bound.getAbstractDeclaration();
//    } else {
//      return name;
//    }
////		StringBuilder sb = new StringBuilder(name);
////		if (!isTypeVarRef() && !bounds.isEmpty())
////		{
////			sb.append(" extends ");
////			boolean first = true;
////			for (JavaTypeDescriptor jt : bounds)
////			{
////				if (!first)
////					sb.append(" & ");
////				first = false;
////				sb.append(concrete ? jt.getConcreteDeclaration() : jt.getAbstractDeclaration());
////			}
////		}
////		return sb.toString();
//  }

  @Override
  public AllClassMap getAllRefs() {
    AllClassMap refs = super.getAllRefs();
    bounds.stream().forEach(bound -> refs.addClassRefs(bound.getAllRefs()));
    if (getActual() != null)
      refs.addClassRefs(getActual().getAllRefs());
    return refs;
  }

  @Override
  public String toString() {
    return "JavaTypeVar{"
        + "\nname=" + getName()
        + ",\nclassName=" + getClassName()
        + ",\nwildcard=" + wildcard
        + ",\ntypeVarRef=" + typeVarRef
        + ",\nscopeMember=" + (getScopeMember() != null ? getScopeMember().getName() : "")
        + ",\nbounds=" + Util.collectionToString(bounds.stream().map(jtd -> jtd.getName()).collect(Collectors.toList()))
        + ",\nactual=" + (getActual() != null ? getActual().getFqn() : "null")
//        + ",\ndeclaration.concrete=" + getConcreteDeclaration()
//        + ",\ndeclaration.abstract=" + getAbstractDeclaration()
        + "\n}";
  }

}
