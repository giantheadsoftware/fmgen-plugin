package com.giantheadsoftware.fmgen.model.java;

import com.giantheadsoftware.fmgen.model.java.util.AllClassMap;
import com.giantheadsoftware.fmgen.util.ToString;
import com.giantheadsoftware.fmgen.util.Util;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author clininger $Id: $
 */
public class JavaMethodModel extends JavaClassMemberModel {
  /**
   * The annotations on the method, if any.
   */  /**
   * The args of this method
   */
  public final List<JavaMethodArgModel> args = new LinkedList<>();

  public JavaMethodModel(JavaTypeRef<?> ref) {
    super(ref);
  }

  public List<JavaMethodArgModel> getArgs() {
    return args;
  }
	
	/**
	 * @return the method name + the argument types, in parens, no spaces: i.e. methodName(int,String)
	 */
	public String getSignature() {
		return name + "(" 
					+ args.stream().map(arg -> arg.typeRef.getName()).collect(Collectors.joining(","))
					+")";
	}
	
	/**
	 * @return the method signature with the return type at the end, after a colon, no spaces:
	 * i.e. methodName(int,String):List&lt;String&gt;
	 */
	public String getSignatureWithType() {
		return getSignature() + ":" + typeRef.getName();
	}

	/**
	 * @return true if any args are container (collection or map) types
	 */
  public boolean hasContainerArgs() {
		return args.stream().anyMatch(arg -> arg.getTypeRef().isContainer());
  }

//  @Override
//  public String getSignature() {
//    String signature = "";
//    signature += getTypeParams().isEmpty() ? "" : "<" + getTypeParams().stream().map(tv -> tv.getSignature()).collect(Collectors.joining(", ")) + "> ";
//    signature += getSelfRef().getSignature();
//    signature += "(" + args.stream().map(arg -> arg.getSignature()).collect(Collectors.joining(", ")) + ")";
//    return signature;
//  }

  @Override
  public final AllClassMap getAllRefs() {
    AllClassMap refs = super.getAllRefs();
    args.stream().forEach(arg -> refs.addClassRefs(arg.getAllRefs()));

    return refs;
  }

  @Override
  public String toString() {
    return new ToString().toString(this, this::toString); 
  }

  public String toString(ToString ts) {
    String value = ts.newLine()+"name = " + ts.toString(name)
        + ts.newLine()+"type = " + ts.toString(getTypeRef(), (ts2) -> getTypeRef().toString(ts2))
        + ts.newLine()+"typeVars = " + ts.toString(getTypeVars(), (ts2, ref) -> ts.toString(ref.name))
        + ts.newLine()+"typeParams = " + ts.toString(getTypeParams(), (ts2, ref) -> ts2.toString(ref, ref::toString))
        + ts.newLine()+"annotations = " + ts.toString(getAnnotations(), (ts2, anno) -> ts2.toString(anno, anno::toString))
        + ts.newLine()+"args = " + ts.toString(getArgs(), (ts2, arg) -> ts2.toString(arg, arg::toString))
        + ts.newLine()+"declClass = " + getDeclRef().getFqn()
        + ts.newLine()+"imports = " + ts.toString(getImportRefs().stream().map(ref -> ref.getFqn()).collect(Collectors.toSet()))
    ;
    return value;
  }

}
