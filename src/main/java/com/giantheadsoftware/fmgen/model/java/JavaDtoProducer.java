package com.giantheadsoftware.fmgen.model.java;

import java.lang.reflect.Field;
import java.lang.reflect.TypeVariable;
import java.util.Map;
import org.apache.maven.plugin.MojoExecutionException;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author clininger $Id: $
 */
public class JavaDtoProducer extends JavaPojoProducer {

  public JavaDtoProducer(Map<String, Object> params) throws MojoExecutionException {
    super(params);
  }

  @Override
  protected JavaPojoModel scanModel(Class<?> dtoClass) throws MojoExecutionException {
    log.debug("!@!");
    log.debug("SCANNING CLASS " + dtoClass.getName());
    JavaPojoModel model = scanClass(dtoClass);

    // examine the public fields
    for (Field field : dtoClass.getFields()) {
      addProperty(model, field.getName(), field.getType(), field.getGenericType(), field.getAnnotations(), new TypeVariable[0], field.getDeclaringClass());
    }

    log.debug("Generated (raw) model: \n" + model.toString());
    return model;
  }
}
