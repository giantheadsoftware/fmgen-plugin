package com.giantheadsoftware.fmgen.model.java.util;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import com.giantheadsoftware.fmgen.model.java.JavaTypeRef;
import com.giantheadsoftware.fmgen.util.ToString;
import com.giantheadsoftware.fmgen.util.Util;
import java.lang.reflect.Type;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
/**
 * This is a container for a list of refs that all have the same type name
 * @author clininger
 * $Id: $
 */
public class TypeRefList {
  private final List<JavaTypeRef<?>> list = new LinkedList<>();
  private final JavaTypeRef<?> first;
//  private boolean mapped;
  private boolean notImported;  // variable that can override the impreted status
  private final String simpleName;  //TODO:  shouldn't this be mappable?

  public TypeRefList(JavaTypeRef<?> firstRef) {
    first = firstRef;
    add(firstRef);
    simpleName = computeSimpleName();
//    mapped = first.isMapped();
  }

  public List<JavaTypeRef<?>> getList() {
    return list;
  }

  public boolean addAll(Collection<? extends JavaTypeRef<?>> c) {
    final  List<String> errors = new ArrayList<>();
    c.forEach(e -> {
      try {
        add(e);
      } catch (IllegalArgumentException x) {
        errors.add(x.getMessage());
      }
    });
    if (!errors.isEmpty()) {
      throw new IllegalArgumentException(Util.collectionToString(errors));
    }
    return true;
  }

  public final boolean add(JavaTypeRef<?> e) {
    if (!first.getFqn().equals(e.getFqn())) {
      throw new IllegalArgumentException(MessageFormat.format("Wrong FQN in TypeRefList: {0} != {1}", e.getFqn(), first.getFqn()));
    }
    return list.add(e);
  }

//  public void map(String to) {
//    list.stream().peek(e -> e.setFqn(to)).forEach(e -> e.setMapped(true));
//    simpleName = computeSimpleName();
//    this.mapped = true;
//  }

  public Type getType() {
    return first.getType();
  }

  public TypeRefList setName(String name) {
    list.forEach(e -> e.setBaseName(name));
    return this;
  }

  /**
   * @return the simple, not qualified, name of the class (which may have been mapped)
   */
  public String getSimpleName() {
    return simpleName;
  }

//  /**
//   * @return true if this ref has been name-mapped
//   */
//  public boolean isMapped() {
//    return mapped;
//  }

//  /**
//   * Set this entry as "mapped", meaning that the classname has been checked and/or processed by the name mapping regex
//   * @param mapped
//   * @return
//   */
//  public TypeRefList setMapped(boolean mapped) {
//    this.mapped = mapped;
//    list.stream().forEach(e -> e.setMapped(mapped));
//    return this;
//  }

  /**
   * @return true if this ref is a candidate to be imported
   */
  public boolean isImported() {
    return !notImported && list.stream().anyMatch(JavaTypeRef::isImported);
  }

  public boolean isVar() {
    return first.isVar();
  }

  public TypeRefList setNotImported() {
    this.notImported = true;
    list.forEach(ref -> ref.setImported(false));
    return this;
  }

  private String computeSimpleName() {
    int lastDot = first.getFqn().lastIndexOf('.');
    return lastDot > 0 ? first.getFqn().substring(lastDot+1) : first.getFqn();
  }

  @Override
  public String toString() {
    return new ToString().toString(this, this::toString); 
  }

  public String toString(ToString ts) {
      return "["+list.size()+"] simpleName = " + simpleName + "; notImported = " + notImported;
  }
}
