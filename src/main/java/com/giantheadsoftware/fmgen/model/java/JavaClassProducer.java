package com.giantheadsoftware.fmgen.model.java;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.giantheadsoftware.fmgen.ParameterNames;
import static com.giantheadsoftware.fmgen.ParameterNames.*;
import com.giantheadsoftware.fmgen.model.ModelProducer;
import com.giantheadsoftware.fmgen.model.java.util.TypeRefList;
import com.giantheadsoftware.fmgen.util.ModelClassFinder;
import com.giantheadsoftware.fmgen.util.RegexReplace;
import com.giantheadsoftware.fmgen.util.Util;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

/**
 *
 * @author clininger $Id: $
 */
abstract class JavaClassProducer implements ModelProducer {
  // property names

  protected static final String TARGET_PACKAGE = "targetPackage";

  // class fields
  protected final Log log;
  protected final Map<String, Object> pluginParams;
  protected final Properties pluginProperties = new Properties();
  protected final String baseClassName;
  protected final Set<String> abstractClasses = new HashSet<>();
  protected final JavaTypeRefFactory typeFactory;
  protected final RegexReplace packageNameMapper;

  protected final Pattern includeMemberPattern;
  protected final Pattern excludeMemberPattern;
  protected final Pattern includeTypePattern;
  protected final Pattern excludeTypePattern;
  protected final Pattern includeMemberAnnotationPattern;
  protected final Pattern excludeMemberAnnotationPattern;
  protected final Pattern includeArgTypePattern;
  protected final Pattern excludeArgTypePattern;  /**
   * Constructor.
   *
   * @param params Receives the parameter values defined by the mojo and provided in the POM
   * @throws MojoExecutionException
   */
  public JavaClassProducer(Map<String, Object> params) throws MojoExecutionException {
    if (params == null) {
      throw new MojoExecutionException("Plugin params must be provided to the producer");
    }
    pluginParams = params;
    log = (Log)params.get(LOG);
    initProperties(params);
    packageNameMapper = new RegexReplace(pluginProperties.getProperty(PACKAGE_MAP), pluginProperties);
    baseClassName = pluginProperties.getProperty(BASE_CLASS_NAME);
    if (pluginProperties.containsKey(ABSTRACTS))
      abstractClasses.addAll(Arrays.asList(pluginProperties.getProperty(ABSTRACTS).split(", *")));
    typeFactory = new JavaTypeRefFactory(params, pluginProperties, log);

    includeMemberPattern = (Pattern) params.get(ParameterNames.INCLUDE_MEMBER_PATTERN);
    excludeMemberPattern = (Pattern) params.get(ParameterNames.EXCLUDE_MEMBER_PATTERN);
    includeTypePattern = (Pattern) params.get(ParameterNames.INCLUDE_TYPE_PATTERN);
    excludeTypePattern = (Pattern) params.get(ParameterNames.EXCLUDE_TYPE_PATTERN);
    includeMemberAnnotationPattern = (Pattern) params.get(ParameterNames.INCLUDE_MEMBER_ANNOTATION_PATTERN);
    excludeMemberAnnotationPattern = (Pattern) params.get(ParameterNames.EXCLUDE_MEMBER_ANNOTATION_PATTERN);
    includeArgTypePattern = (Pattern) params.get(ParameterNames.INCLUDE_ARG_TYPE_PATTERN);
    excludeArgTypePattern = (Pattern) params.get(ParameterNames.EXCLUDE_ARG_TYPE_PATTERN);
  }

  private void initProperties(Map<String, Object> params) {
    for (Map.Entry<String, Object> param : params.entrySet())
      if (param.getValue() instanceof String)
        pluginProperties.setProperty(param.getKey(), (String)param.getValue());
  }

  /**
   * Locate the model definitions using the modelClassFinder. Scan the models and return them in a map keyed by fullName.
   *
   * @return Map of scanned model classes
   * @throws MojoExecutionException on error
   */
  @Override
  public Map<String, JavaClassModel> getModels() throws MojoExecutionException {
    try {
      Map<String, JavaClassModel> models = new LinkedHashMap<>();

      // locate the model classes on the classpath
      List<Class<?>> sourceClasses = new ModelClassFinder(pluginParams).getSourceClasses();
      typeFactory.addModelClasses(sourceClasses);

      // construct the template models
      for (Class<?> sourceClass : sourceClasses) {
        JavaClassModel model = scanModel(sourceClass);
        reduceClassNames(model);
        log.debug("Generated model: \n" + Util.indentStruct(model.toString()));
        models.put(model.getFqn(), model);
      }

      return models;
    }
    catch (Exception ex) {
      throw new MojoExecutionException(ex.getMessage(), ex);
    }
  }

  /**
   * Scan the model definition. Fully populate the properties.
   *
   * @param <M> ref to model class
   * @param c Definition of a model class.
   * @return A populated JavaClassModel.
   */
  protected abstract JavaClassModel scanModel(Class<?> c) throws MojoExecutionException;

  /**
   * Populate the model based on the class
   *
   * @param <M> ref to model class
   * @param c     the source class
   * @return populated class model
   */
  protected <M extends JavaClassModel> M scanClass(Class<?> c) throws MojoExecutionException {
    if (c == null)
      throw new IllegalArgumentException("Source class must be provided");
    // create self
    M model = createModel(typeFactory.createModelRef(c));
    model.setImportMapPatterns((List<String>) pluginParams.get(IMPORT_MAPS), pluginProperties);
    // create & set the src class ref
    setSrcClassRef(model, c);

    model.baseClassName = baseClassName;
    if (baseClassName != null)
      log.debug("base class " + (c.isInterface() ? "interface" : "") + ": " + baseClassName);

    // we're scanning the model source class; get the type vars
    for (TypeVariable<?> tVar : c.getTypeParameters()) {
      log.debug("   Root Class Type Param: " + tVar.getName());
      model.getTypeParams().add(typeFactory.createTypeRef(tVar, model));
    }
    setTargetPackage(model);

    // addd any imports specified in the POM
    model.addImports((List<String>)pluginParams.get(ParameterNames.IMPORTS));

    // class annotations
    for (Annotation anno : c.getAnnotations()) {
      model.addAnnotation(new JavaAnnotationModel(typeFactory.createTypeRef(anno.annotationType(), model), anno));
    }

    for (Type iface : c.getGenericInterfaces()) {
      log.debug("Creating type for super interface " + iface.getTypeName());
      JavaTypeRef<?> ifaceRef = typeFactory.createTypeRef(iface, model);
      model.inheritClasses.add(ifaceRef);
      if (pluginProperties.getProperty(IFC_PACKAGE) == null)
        model.interfaces.add(ifaceRef);
    }

    setSuperClass(model);
    return model;
  }

  /**
   * Determine the target path for models based on mojo config params
   * @param typeRef the typeRef of the class
   * @return the target package
   */
  protected String computeTargetPackage(JavaTypeRef<?> typeRef) {
		String fixedPath = pluginProperties.getProperty(TARGET_PATH);
    if (fixedPath != null && typeRef.isInModel()) {
        return Util.filePath2Package(fixedPath);
    }
		return packageNameMapper.replace(typeRef.getPackage());
  }

  /**
   * Set the targetPackage according to the TARGET_PATH, if provided, otherwise try the PACKAGE_MAP.
   * If neither are provided, just retain the same package as the source class.
   * @param model The model to set the package on
   */
  protected void setTargetPackage(JavaClassModel model)  {
    model.setTargetPackage(computeTargetPackage(model.getTypeRef()));
  }

  protected <M extends JavaClassModel> M createModel(JavaTypeRef<?> ref) {
    return ref.isEnum() ? (M)new JavaEnumModel(ref) : (M)new JavaClassModel(ref);
  }

  /**
   * Create a type ref for the source class.  We can't use the type factory for this
   * because it will recognize it as a model interface and try to map it.
   * @param model The model this class is the source of
   * @param srcClass the source class of the model
   */
  protected void setSrcClassRef(JavaClassModel model, Class<?> srcClass) {
    // create & set the src class ref
    JavaTypeRef<?> srcClassRef = new JavaTypeRef(srcClass, pluginParams, pluginProperties);
    srcClassRef.setFqn(srcClass.getName());
    srcClassRef.setOrigFqn(srcClass.getName());
    srcClassRef.setBaseName(srcClass.getSimpleName());
    srcClassRef.setScopeModel(model);
    srcClassRef.setImported(false); //TODO: make this a config property?
    model.setSrcClassRef(srcClassRef);
  }

  protected void setSuperClass(JavaClassModel model) {
    if (model.getSrcClassRef().isInterface()) {
      //localte the first inherited class/interface that is in the model
      model.superClass = model.inheritClasses.stream().filter(JavaTypeRef::isInModel).findFirst().orElse(null);
      // remove it from the interface list so it's not double-represented
      model.interfaces.remove(model.superClass);
    }
    else {
      log.debug("Creating superclass type");
      model.superClass = typeFactory.createTypeRef(((Class<?>)model.getType()).getGenericSuperclass(), model);
      model.inheritClasses.add(model.superClass);
    }
    if (model.superClass == null && baseClassName != null) {
      // no superclass identified, but there is a baseClass in the config.  Synthesize a type for it and make it superClass
      String fullName = model.getTargetPackage() + '.' + baseClassName;
      Class<?> superC = null;
      try {
        superC = Class.forName(fullName);
      }
      catch (ClassNotFoundException x) {
        log.debug("no class found for super class: " + fullName);
      }
      model.superClass = (JavaTypeRef<?>) typeFactory.createTypeRef(superC, model);
    }
  }

  protected JavaMethodModel scanMethod(JavaClassModel model,
      Method method,
      String name,
      Class<?> returnType,
      Type genericType,
      Annotation[] annotations,
      int modifiers,
      TypeVariable<?>[] typeParameters,
      Parameter[] methodArgs,
      Type[] methodGenericArgTypes,
      Class<?> declaringClass) {
    log.debug("\nScanning method " + name);
    JavaMethodModel methodModel = new JavaMethodModel(typeFactory.createTypeRef(genericType, model));
    methodModel.name = name;
    // locate the class ref for the declaring class
    if (model.getOrigFqn().equals(declaringClass.getName())) {
      methodModel.setDeclRef(model.getSelfRef());
    }
    else {
      methodModel.setDeclRef(model.inheritClasses.stream()
          .filter(t -> t.getOrigFqn().equals(declaringClass.getName()))
          .findFirst()
          .get());
    }
    // get the type vars declared on this method
    methodModel.addTypeVars(scanTypeVars(typeParameters, methodModel));

    // do if the return type is a collection/map...
    setDefaultContainer(methodModel.getSelfRef());

    // method-level annotations
    if ((Boolean)pluginParams.get(INHERIT_METHOD_ANNOTATIONS)) {
      doForAllSupers(model.getRawClass(), clas -> {
        try {
          Method javaMethod = clas.getMethod(name, method.getParameterTypes());
          methodModel.addAnnotations(scanAnnotations(javaMethod.getAnnotations(), methodModel));
        } catch (NoSuchMethodException ignored) {
          // do nothing
        }
      });
    } else {
      methodModel.addAnnotations(scanAnnotations(annotations, methodModel));
    }


    // args
    for (int i = 0; i < methodArgs.length; i++) {
      Parameter p = methodArgs[i];
      Type argType = methodGenericArgTypes[i];
      JavaMethodArgModel arg = new JavaMethodArgModel(p.getName(), typeFactory.createTypeRef(argType, methodModel));
      arg.getSelfRef().setImported(true);
      setDefaultContainer(arg.getSelfRef());
      arg.getAnnotations().addAll(scanAnnotations(p.getAnnotations(), arg));
      methodModel.args.add(arg);
    }
    methodModel.getAllRefs().values().stream().flatMap(trl -> trl.getList().stream()).forEach(importRef -> importRef.setImported(true));
    return methodModel;
  }

  protected void doForAllSupers(Class<?> clas, Consumer<Class<?>> fn) {
    if (clas == null) {
      return;
    }
    fn.accept(clas);
    doForAllSupers(clas.getSuperclass(), fn);
    Arrays.stream(clas.getInterfaces()).forEach(intf -> doForAllSupers(intf, fn));
  }

  protected List<JavaTypeVar> scanTypeVars(TypeVariable<?>[] typeParameters, JavaScopeEntity scopeEntity) {
    return Arrays.asList(typeParameters).stream()
        .map(tv -> typeFactory.createTypeRef(tv, scopeEntity))
        .map(tvr -> (JavaTypeVar)tvr.getReferencedEntity())
        .collect(Collectors.toList());
  }

  protected List<JavaAnnotationModel> scanAnnotations(Annotation[] annotations, JavaScopeEntity scopeEntity) {
    return Arrays.stream(annotations)
        .map(anno -> new JavaAnnotationModel(typeFactory.createTypeRef(anno.annotationType(), scopeEntity), anno))
        .peek(annoRef -> annoRef.getSelfRef().setImported(true))
        .collect(Collectors.toList());
  }

  /**
   * For types that are abstract collections or maps, choose a concrete container type based on defaults or configuration.
   * TODO:  move this to the TypeRefFactory?
   * @param memberRef Potentially abstract member
   */
  protected void setDefaultContainer(JavaTypeRef<?> memberRef) {
    if (memberRef.isContainer() && (memberRef.isInterface() || memberRef.isAbstract())) {
      // If the return type is abstract, try to determine which concrete collection to instantiate
      Class<?> defaultCollection = Util.defaultCollectionClass(memberRef.getRawClass(), (Map<String, Class>)pluginParams.get(ParameterNames.DEFAULT_COLLECTIONS));
      memberRef.setDefaultContainer(typeFactory.createTypeRef(defaultCollection, memberRef.getScopeEntity()));
      if ((Boolean)pluginParams.get(ParameterNames.IMPORT_DEFAULT_COLLECTIONS)) {
        memberRef.getDefaultContainer().setImported(true);
      }
    }
  }

  /**
   * Reduce class names where possible from fully qualified names, such as classes in the same package and java.lang.* classes.
   * These classes are also removed from the imports list.
   * <p>
   * NOTE that we don't set the import flag to TRUE here, only false. If the typeRef is a candidate for importing
   * then it will be set to true when it's created. What we're doing here is culling the import list after we know
   * what all of the classes will be.
   *
   * @param model Model which contains the class references
   */
  private void reduceClassNames(JavaClassModel model) {
    Map<String, String> reduced = new HashMap<>(); // tracks names already reduced to avoid duplication; key= reduced, value = fqn
    log.debug("REDUCE CLASSNAMES");
    model.getAllRefs().entrySet().stream()
        .peek(entry -> {
          if (!entry.getValue().isImported()) {
            log.debug(entry.getKey() + " is not marked for import");
          }
        })
        .filter(entry -> {
          if (entry.getValue().isVar()) {
            entry.getValue().setNotImported();
            return false;
          }
          return true;
        })  // eliminate type vars & wildcards
        .filter(entry -> {
          // eliminate all primitives:  neither import nor reduce
          TypeRefList list = entry.getValue();
          Type t = list.getType();
          if (t instanceof Class
              && (((Class<?>)t).isPrimitive()
              || ((Class<?>)t).isArray() && ((Class<?>)t).getComponentType().isPrimitive())) {
            list.setNotImported();
            return false;
          }
          return true;
        })
        .filter(entry -> {
          // if class is in "ignored" package, just reduce, don't import
          String fqn = entry.getKey();
          if (isIgnoredPackage(model, fqn)) {
            log.debug("REDUCTION: ignoring import from package: " + fqn);
            String simpleName = entry.getValue().getSimpleName();
            entry.getValue().setNotImported().setName(simpleName)/*.setMapped(true)*/;
            reduced.put(simpleName, entry.getKey());
            return false;
          }
          return true;
        })
        .filter(entry -> {
          // If a matching simple classname already exists, don't import or reduce, just use fqn
          String fqn = entry.getKey();
          if (reduced.containsKey(entry.getValue().getSimpleName())) {
            if (!reduced.get(entry.getValue().getSimpleName()).equals(fqn)) {
              // Some other class already reduces to this name.
              // Don't import: use FQN
              log.debug(MessageFormat.format("REDUCTION: can''t reduce {0}: {1} already exists in the class",
                  fqn, entry.getValue().getSimpleName()));
              entry.getValue().setNotImported().setName(fqn);
              return false;
            } else {
              log.debug("FQN already reduced: "+fqn);
              return false;
            }
          }
          return true;
        })
        .forEach(entry -> {
          // for everything else, import & reduce
          log.debug(MessageFormat.format("REDUCTION: importing & reducing {0}", entry.getKey()));
          entry.getValue().setName(entry.getValue().getSimpleName());
        });
  }

  protected boolean isIgnoredPackage(JavaClassModel model, String fqClassName) {
    if (fqClassName == null || fqClassName.startsWith("java.lang.")) {
       return true;  // ignored
    }
    int lastDot = fqClassName.lastIndexOf(".");
    if (lastDot <= 0)
      return true;  // should be a dot in the fullName
    String packageName = fqClassName.substring(0, lastDot);
    return packageName.equals(model.getTargetPackage());
  }
}
