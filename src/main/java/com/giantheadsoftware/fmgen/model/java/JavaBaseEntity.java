package com.giantheadsoftware.fmgen.model.java;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.giantheadsoftware.fmgen.model.java.util.AllClassMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 *
 * @author clininger
 * $Id: $
 */
public abstract class JavaBaseEntity implements JavaEntity {
  protected final JavaTypeRef<? extends JavaBaseEntity> typeRef;
  /**
   * List of the type variables declared by the entity
   */
  protected final List<JavaTypeVar> typeVars = new LinkedList<>();
  /**
   * The annotations on the method, if any, mapped by fqn.
   */
  protected  final Map<String, JavaAnnotationModel> annotations = new LinkedHashMap<>();

  public JavaBaseEntity(JavaTypeRef<?> typeRef) {
    this.typeRef = (JavaTypeRef<? extends JavaBaseEntity>) typeRef;
  }

  @Override
  public JavaTypeRef<?> getSelfRef() {
    return typeRef;
  }

  /**
   * If this class is parameterized, return the type vars.  This is less generic than the getTypeParams() call.
   * @return type variables declared for this class
   */
  @Override
  public List<JavaTypeVar> getTypeVars() {
    return typeVars;
  }

  public void addTypeVar(JavaTypeVar jtv) {
    if (jtv == null) {
      throw new IllegalArgumentException("Can't add a null TypeVar");
    }
    typeVars.add(jtv);
  }

  public void addTypeVars(Collection<JavaTypeVar> jtvs) {
    jtvs.forEach(this::addTypeVar);
  }

  @Override
  public List<JavaAnnotationModel> getAnnotations() {
    return new ArrayList<>(annotations.values());
  }

  @Override
  public JavaAnnotationModel getAnnotation(String fqn) {
    return annotations.get(fqn);
  }

  public void addAnnotation(JavaAnnotationModel anno) {
    annotations.put(anno.getFqn(), anno);
  }

  public void addAnnotations(Collection<JavaAnnotationModel> annos) {
    annos.forEach(anno -> annotations.computeIfAbsent(anno.getFqn(), fqn -> anno));
  }

  @Override
  public AllClassMap getAllRefs() {
    final AllClassMap refs = new AllClassMap();
    refs.addClassRefs(getSelfRef().getAllRefs());
    getAnnotations().forEach(anno -> refs.addClassRefs(anno.getAllRefs()));
    getTypeVars().forEach(typeVar -> refs.addClassRefs(typeVar.getAllRefs()));
    return refs;
  }


}
