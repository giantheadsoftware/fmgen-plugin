/*
 * Copyright (c) 2016-2021 SET PROJECT ORGANIZATION IN POM.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.giantheadsoftware.fmgen.model.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author clininger $Id: $
 */
public class JavaEnumModel extends JavaClassModel {
	
	private final List<Enum> values = new ArrayList<>();

	public JavaEnumModel(JavaTypeRef ref) {
		super(ref);
		values.addAll(Arrays.stream(ref.getRawClass().getEnumConstants()).map(value -> (Enum)value).collect(Collectors.toList()));
	}

	public List<Enum> getValues() {
		return values;
	}
	
}
