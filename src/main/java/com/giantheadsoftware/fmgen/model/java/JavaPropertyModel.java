package com.giantheadsoftware.fmgen.model.java;

import com.giantheadsoftware.fmgen.util.ToString;
import java.util.stream.Collectors;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Model for a property in a POJO.
 *
 * @author clininger $Id: $
 */
public class JavaPropertyModel extends JavaClassMemberModel {

  public JavaPropertyModel(JavaTypeRef ref) {
    super(ref);
  }

  @Override
  public String getName() {
    return name;
  }
	
  public String getNameCapFirst() {
    return Character.toUpperCase(name.charAt(0)) + name.substring(1);
  }
	
	public String getGetterName() {
		String prefix = (typeRef.getType() == boolean.class) ? "is" : "get";
		return prefix + getNameCapFirst();
	}
	
	public String getSetterName() {
		return "set" + getNameCapFirst();
	}

  @Override
  public String toString() {
    return new ToString().toString(this, this::toString); 
  }

  public String toString(ToString ts) {
    String value = ts.newLine()+"name = " + ts.toString(name)
        + ts.newLine()+"type = " + ts.toString(getTypeRef(), (ts2) -> getTypeRef().toString(ts2))
        + ts.newLine()+"typeVars = " + ts.toString(getTypeVars(), (ts2, ref) -> ts.toString(ref.name))
        + ts.newLine()+"typeParams = " + ts.toString(getTypeParams(), (ts2, ref) -> ts2.toString(ref, ref::toString))
        + ts.newLine()+"annotations = " + ts.toString(getAnnotations(), (ts2, anno) -> ts2.toString(anno, anno::toString))
        + ts.newLine()+"declClass = " + getDeclRef().getFqn()
        + ts.newLine()+"imports = " + ts.toString(getImportRefs().stream().map(ref -> ref.getFqn()).collect(Collectors.toSet()))
    ;
    return value;
  }
}
