package com.giantheadsoftware.fmgen.model.java;

import com.giantheadsoftware.fmgen.util.ToString;
import com.giantheadsoftware.fmgen.util.Util;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * A reference to a type
 *
 * @author clininger $Id: $
 * @param <E> The Java entity type this ref is referencing
 */
public class JavaTypeRef<E extends JavaEntity> {
  private final Map<String, Object> pluginParams;

  private final Properties pluginProperties;
  /**
   * the raw type.
   */
  private final Type type;

  private E referencedEntity;


  /**
   * Labels of the modifiers
   */
  protected final List<String> modifierLabels = new LinkedList<>();

  /**
   * If this is a parameterized type, here is a ref tot he raw type
   */
  private JavaTypeRef<?> rawTypeRef;
  /**
   * original full type name, as provided by Class.getName() or Type.getTypeName().
   * This name does not change with mapping or reducing
   */
  private String origFqn;
  /**
   * the (possibly reduced) name of the type. It could the final FQN or simple name.
   * This is typically what should appear in the finally rendered class as it
   * takes into account all of the processing.
   */
  private String baseName;
  /**
   * the (possibly mapped) name of the type.
   */
  private String fqn;
  /**
   * if parameterized type, these are the parameters.
   */
  private final List<JavaTypeRef<?>> typeParams = new LinkedList<>();
  /**
   * if a type variable of this type is needed, use this.
   */
  private String selfVar;
  /**
   * the model within which this type was referenced
   */
  private JavaClassModel scopeModel;
  /**
   * if referenced from within a property declaration, this is the property
   */
  private JavaClassMemberModel scopeMember;
  /**
   * true if this type is a class in the model package.
   */
  private boolean inModel;

  /**
   * true if class name mapping has already occurred on this type
   */
  private boolean mapped;
  /**
   * True if this ref needs to be in the import list
   */
  private boolean imported;

  /**
   * If this ref is for an abstract container type, set the default concrete container that should be used
   */
  private JavaTypeRef<?> defaultContainer;



  /**
   * Constructor
   * @param type The type this ref is referencing
   * @param pluginParams from the maven plugin
   * @param pluginProperties from the maven plugin
   */
  public JavaTypeRef(Type type, Map<String, Object> pluginParams, Properties pluginProperties) {
    this.type = type;
    this.pluginParams = pluginParams;
    this.pluginProperties = pluginProperties;
  }

  public Map<String, Object> getPluginParams() {
    return pluginParams;
  }

  public Properties getPluginProperties() {
    return pluginProperties;
  }

  public JavaTypeRef<?> getSelf() {
    return this;
  }

  public Type getType() {
    return type;
  }

  /**
   * Not to be confused with getRawTypeRef().  This method returns
   * @return the raw class, even if it's parameterized or variable with actual or single upper bound.
   * Return null if the type is variable/wildcard without actual or single upper bound
   */
  public Class<?> getRawClass() {
    JavaTypeRef<?> rawClassRef = getRawClassRef();
    return rawClassRef != null ? (Class<?>)rawClassRef.type : null;
  }

  /**
   * Not to be confused with getRawClassRef().  This method returns a value only when it can be resolved
   * to an actual Class, and can work for types other than ParameterizedType.
   * @return the raw class, even if it's parameterized or variable with actual or single upper bound.
   * Return null if the type is variable/wildcard without actual or single upper bound
   */
  public JavaTypeRef<?> getRawClassRef() {
    if (isParameterized()) {
      return getRawTypeRef().getRawClassRef();
    }
    if (hasActual()) {
      return getActual().getRawClassRef();
    }
    if (hasUpperBounds()) {
      return getUpperBounds().get(0).getRawClassRef();
    }
    return type instanceof Class ? this : null;
  }

  /**
   * Not to be confused with getRawClassRef().  This method returns a value only when the ref is for a
   * ParameterizedType.
   * @return The rawClass if this is a parameterized type reference, otherwise null.
   */
  public JavaTypeRef<?> getRawTypeRef() {
    return isParameterized() ? rawTypeRef.getRawTypeRef() : this;
  }

  /**
   * @return the JavaEntity instance referenced by this ref.
   */
  public E getReferencedEntity() {
    return referencedEntity;
  }

  /**
   * @return the potentially unqualified baseName for this ref, without type params.  If a type is determined to be imported,
   * then the package name is removed from the front, and the name just becomes the simple name of the type.
   */
  public String getBaseName() {
    return baseName;
  }
	
	/**
	 * see https://gitlab.com/giantheadsoftware/fmgen-plugin/-/issues/1
	 * @return the type name including any type parameters.
	 */
	public String getName() {
		return baseName + getTypeParamStr();
	}
	
	/**
	 * @return the formatted type parameters of a parameterized type.  Returns empty string if not parameterized.
	 */
	public String getTypeParamStr() {
		String result = "";
		if (isParameterized()) {
			result += "<";
			boolean first = true;
			for (JavaTypeRef<?> param : typeParams) {
				if (!first) {
					result += ", ";
				}
				result += param.getName();
				first = false;
			}
			result += ">";
		}
		return result;
	}

//  @Override
//  public RefAs getRefAs() {
//    return refAs;
//  }

  /**
   * @return The fully qualified type name, which may have been mapped from its original name to a target name
   */
  public String getFqn() {
    return fqn;
  }

  /**
   * @return The fully qualified type name, which may have been mapped from its original name to a target name
   */
  public String getPackage() {
    return (fqn.lastIndexOf(".") > 0 ? fqn.substring(0, fqn.lastIndexOf(".")) : null);
  }

  /**
   * @return The fully qualified type name, prior to any mapping
   */
  public String getOrigPackage() {
    return (origFqn.lastIndexOf(".") > 0 ? origFqn.substring(0, origFqn.lastIndexOf(".")) : null);
  }

  /**
   * For parameterized types, these are the parameters.  They can be variables or actual classes.
   * @return Type params
   */
  public List<JavaTypeRef<?>> getTypeParams() {
    return typeParams;
  }

  /**
   * @return if this type can be resolved to a Class via getRawClass(), return the modifiers of that class.
   * Otherwise, return 0.
   */
  public int getModifiers() {
    return getRawClass() != null ? getRawClass().getModifiers() : 0;
  }

  /**
   * @return true if the modifiers of this type indicate it's an interface
   */
  public boolean isInterface() {
    return Modifier.isInterface(getModifiers());
  }

  /**
   * @return true if the modifiers of this type indicate it's an abstract class
   */
  public boolean isAbstract() {
    return Modifier.isAbstract(getModifiers());
  }

  /**
   * This is implemented for collection and map classes.
   * @return A concrete class that can be instantiated in place of an abstract one, or null if there is no such class.
   */
  public Class<?> getClassToInstantiate() {
    JavaTypeRef<?> rawClassRef  = getRawClassRef();
    if (rawClassRef == null) {
      return null;
    }
    if (rawClassRef.isAbstract() || rawClassRef.isInterface()) {
      if (isContainer()) {
        return getDefaultContainer() != null ? getDefaultContainer().getClassToInstantiate() : null;
      }
      return null;
    }
    return (Class<?>) rawClassRef.getType();
  }

  public List<String> getModifierLabels() {
    if (modifierLabels.isEmpty()) {
      setModifierLabels(getModifiers());
    }
    return modifierLabels;
  }

  /**
   * @return true if the referenced type is ParameterizedType
   */
  public boolean isParameterized() {
    return type instanceof ParameterizedType;
  }

  /**
   * @return true if the referenced type is TypeVariable or WildcardType
   */
  public boolean isVar() {
    return getReferencedEntity() instanceof JavaTypeVar;
  }

  /**
   * @return true if the referenced type is variable and has an actual class
   */
  public boolean hasActual() {
    return isVar() && ((JavaTypeVar) getReferencedEntity()).hasActual();
  }

  /**
   * @return if the referenced type is variable and has an actual class, return that, otherwise null.
   */  public JavaTypeRef<?> getActual() {
    return isVar() ? ((JavaTypeVar)getReferencedEntity()).getActual() : null;
  }

  /**
   * @return true if the referenced type is variable and has upper or lower bounds
   */
  public boolean isBoundedVar() {
    return hasUpperBounds() || hasLowerBounds();
  }

  /**
   * @return true if the referenced type is variable and has upper bounds
   */
  public boolean hasUpperBounds() {
    return isVar() && !((JavaTypeVar)getReferencedEntity()).getBounds().isEmpty();
  }

  /**
   * @return true if the referenced type is variable and has lower bounds
   */
  public boolean hasLowerBounds() {
    return isVar() && !((JavaTypeVar)getReferencedEntity()).getLowerBounds().isEmpty();
  }

  /**
   * @return if the referenced type is variable, the list of upper bounds, otherwise empty list
   */
  public List<JavaTypeRef<?>> getUpperBounds() {
    return isVar() ? ((JavaTypeVar)getReferencedEntity()).getBounds() : Collections.EMPTY_LIST;
  }

  /**
   * @return if the referenced type is variable, the list of lower bounds, otherwise empty list
   */
  public List<JavaTypeRef<?>> getLowerBounds() {
    return isVar() ? ((JavaTypeVar)getReferencedEntity()).getLowerBounds() : Collections.emptyList();
  }

  public boolean isNullable() {
      return !(getType() instanceof Class && ((Class<?>)getType()).isPrimitive());
  }

  public boolean isEnum() {
    Class<?> raw = getRawClass();
    return raw != null && raw.isEnum();
  }

  public boolean isArray() {
    Class<?> raw = getRawClass();
    return raw != null && raw.isArray();
  }

  /**
   * @return true if the referenced type is a collection or map
   */
  public boolean isContainer() {
    return isCollection() || isMap();
  }

  /**
   * @return true if the referenced type is a collection
   */
  public boolean isCollection() {
    return Util.instanceOf(getType(), Collection.class);
  }

  /**
   * @return true if the referenced type is a map
   */
  public boolean isMap() {
    return Util.instanceOf(getType(), Map.class);
  }

  /**
   * @return if the referenced type is a container with an abstract/interface type, return the concrete
   * container class that can be instantiated for it.
   */
  public JavaTypeRef<?> getDefaultContainer() {
    return defaultContainer;
  }

  /**
   * @param defaultContainer if the referenced type is a container with an abstract/interface type, set the concrete
   * container class that can be instantiated for it.
   */
  public void setDefaultContainer(JavaTypeRef<?> defaultContainer) {
    this.defaultContainer = defaultContainer;
  }

  /**
   * @return true if the source class of this type is located in one of the packages where model classes are found.
   * This implies that the referenced entity is a top-level model itself.
   */
  public boolean isInModel() {
    return inModel;
  }

  /**
   * @return true if this item is expected to be added to the imports list of a generated Java class file
   */
  public boolean isImported() {
    return imported;
  }

//  /**
//   * @deprecated
//   * @return true if this
//   */
//  public boolean isMapped() {
//    return mapped;
//  }

  // setters

  public void setReferencedEntity(E referencedEntity) {
    this.referencedEntity = referencedEntity;
  }

  public void setRawTypeRef(JavaTypeRef<?> rawTypeRef) {
    this.rawTypeRef = rawTypeRef;
  }

  public JavaClassModel getScopeModel() {
    return scopeModel;
  }

  public JavaClassMemberModel getScopeMember() {
    return scopeMember;
  }

  public JavaScopeEntity getScopeEntity() {
    return scopeMember != null ? scopeMember : scopeModel;
  }

  public String getSelfVar() {
    if (selfVar == null) {
      setSelfVar();
    }
    return selfVar;
  }

  public String getNameOrVar() {
    return selfVar == null ? baseName : selfVar;
  }

  public String getOrigFqn() {
    return origFqn;
  }

  public void setOrigFqn(String origFqn) {
    this.origFqn = origFqn;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public void setFqn(String fqn) {
    this.fqn = fqn;
  }

  /**
   * Changes the package on the FQN.  If the name is also the FQN, changes that too.
   * @param targetPackage name of the new package
   */
  public void setTargetPackage(String targetPackage) {
    if (baseName.equals(fqn)) {
      setBaseName(Util.changePackage(baseName, targetPackage));
    }
    setFqn(Util.changePackage(fqn, targetPackage));
  }

  public void setInModel(boolean inModel) {
    this.inModel = inModel;
  }

  public void setScopeModel(JavaClassModel scopeModel) {
    this.scopeModel = scopeModel;
  }

  public void setScopeMember(JavaClassMemberModel scopeMember) {
    this.scopeMember = scopeMember;
  }

//  public void setMapped(boolean mapped) {
//    this.mapped = mapped;
//  }

  public void setImported(boolean imported) {
    this.imported = imported;
  }

  /**
   * Examine the class modifiers of the source class and transfer them to the class model. Set the 'abstract' modifier if the class is listed in the 'abstracts' property.
   *
   */
  private void setModifierLabels(int mods) {
    modifierLabels.clear();
    if (Modifier.isPublic(mods))
      modifierLabels.add("public");
    if (Modifier.isProtected(mods))
      modifierLabels.add("protected");
    if (Modifier.isPrivate(mods))
      modifierLabels.add("private");
    if (Modifier.isFinal(mods))
      modifierLabels.add("final");
    if ((Modifier.isAbstract(mods)))
      modifierLabels.add("abstract");
    if (Modifier.isInterface(mods))
      modifierLabels.add("interface");
  }

  /**
   * Set the selfVar property of the typeRef if the typeRef is a model class. The selfVar is the type variable used within class definitions to represent this
   * class. It is
   * constructed by making an acronym from the class name, e.g. Person => P, TaskActivity => TA, etc. If multiple classes have the same acronym, a number is
   * appended to the end.
   *
   */
  private void setSelfVar() {
    if (!isInModel())
      return;
    String simpleName = getBaseName().contains(".") ? getBaseName().substring(getBaseName().lastIndexOf('.') + 1) : getBaseName();
    final String baseName = simpleName.substring(0, 1).toUpperCase() + simpleName.substring(1).replaceAll("[^A-Z0-9]", "");
    // check for uniqueness in the model
    boolean unique;
    String varName = baseName;
    int suffix = 0;
    do {
      unique = true;  // assume for now that the var is unique...
      // check for uniqueness among other classes in this model
      for (JavaTypeRef<?> ref : getAllRefs()) {
        if (ref == this) {
          continue;  // avoid infinie loop
        }
        if (varName.equals(ref.getSelfVar())) {
          unique = false;
          break;
        }
      }
      if (unique && getScopeModel() != null) {
        // check for uniqueness among type variables declared in the model
        for (JavaTypeVar tVar : getScopeModel().getTypeVars()) {
          if (varName.equals(tVar.name)) {
            unique = false;
            break;
          }
        }
      }
      // if this is declared at the property level, check uniqueness at that level
      if (unique && getScopeMember() != null) {
        // check for uniqueness within this property
        for (JavaTypeVar tVar : getScopeMember().getTypeVars()) {
          if (varName.equals(tVar.name)) {
            unique = false;
            break;
          }
        }
      }
      if (!unique) {
        varName = baseName + (suffix++);
      }
    } while (!unique);
    selfVar = varName;
  }

  public Set<JavaTypeRef<?>> getAllRefs() {
    Set<JavaTypeRef<?>> refs = new HashSet<>();
    refs.add(this);
    if (rawTypeRef != null) {
      refs.addAll(rawTypeRef.getAllRefs());
    }
    typeParams.stream().forEach(parameter -> refs.addAll(parameter.getAllRefs()));
    return refs;
  }

  @Override
  public String toString() {
    return new ToString().toString(this, this::toString);
  }

  public String toString(ToString ts) {
    String sb = ts.newLine()+"type = " + ts.toString(getType())
          + ts.newLine()+"baseName = " + ts.toString(getBaseName())
          + ts.newLine()+"selfVar = " + ts.toString(getSelfVar())
          + ts.newLine()+"nameOrVar = " + ts.toString(getNameOrVar())
          + ts.newLine()+"fqn = " + ts.toString(getFqn())
          + ts.newLine()+"origFqn = " + ts.toString(getOrigFqn())
          + ts.newLine()+"rawClass = " + ts.toString(getRawClass())
          + ts.newLine()+"rawClassRef = " + ts.toString(getRawClassRef(), ts2 -> (getRawClassRef() != null ? getRawClassRef().fqn : "null"))
          + ts.newLine()+"rawTypeRef = " + ts.toString(getRawTypeRef(), ts2 -> (getRawTypeRef() != null ? getRawTypeRef().fqn : "null"))
          + ts.newLine()+"package = " + ts.toString(getPackage())
          + ts.newLine()+"modifiers = " + ts.toString(getModifiers())
          + ts.newLine()+"modifierLabels = " + ts.toString(getModifierLabels(), (ts2, label) -> ts2.toString(label))
          + ts.newLine()+"interface = " + ts.toString(isInterface())
          + ts.newLine()+"abstract = " + ts.toString(isAbstract())
          + ts.newLine()+"container = " + ts.toString(isContainer())
          + ts.newLine()+"collection = " + ts.toString(isCollection())
          + ts.newLine()+"map = " + ts.toString(isMap())
          + ts.newLine()+"defaultContainer = " + ts.toString(getDefaultContainer(), ts2 -> (getDefaultContainer() != null ? getDefaultContainer().fqn : "null"))
          + ts.newLine()+"parameterized = " + ts.toString(isParameterized())
          + ts.newLine()+"var = " + ts.toString(isVar())
          + ts.newLine()+"boundedVar = " + ts.toString(isBoundedVar())
          + ts.newLine()+"hasActual() = " + ts.toString(hasActual())
          + ts.newLine()+"hasUpperBounds() = " + ts.toString(hasUpperBounds())
          + ts.newLine()+"hasLowerBounds() = " + ts.toString(hasLowerBounds())
          + ts.newLine()+"imported = " + ts.toString(isImported())
          + ts.newLine()+"classToInstantiate = " + ts.toString(getClassToInstantiate())
          + ts.newLine()+"typeParams = " + ts.toString(getTypeParams(), (ts2, ref) -> ts2.toString(ref, ref::toString))
          + ts.newLine()+"actual = " + ts.toString(getActual(), hasActual() ? getActual()::toString : null)
          + ts.newLine()+"upperBounds = " + ts.toString(getUpperBounds(), (ts2, ref) -> ts2.toString(ref, ref::toString))
          + ts.newLine()+"lowerBounds = " + ts.toString(getLowerBounds(), (ts2, ref) -> ts2.toString(ref, ref::toString))
          + ts.newLine()+"inModel = " + ts.toString(isInModel())
          + ts.newLine()+"scopeEntity = " + ts.toString(getScopeEntity(), ts2 -> (getScopeEntity()!= null ? ((JavaEntity)getScopeEntity()).getName() : "null"))
          + ts.newLine()+"scopeModel = " + ts.toString(getScopeModel(), ts2 -> (getScopeModel()!= null ? ((JavaEntity)getScopeModel()).getName() : "null"))
          + ts.newLine()+"scopeMember = " + ts.toString(getScopeMember(), ts2 -> (getScopeMember()!= null ? ((JavaEntity)getScopeMember()).getName() : "null"))
          + ts.newLine()+"referencedEntity = " + ts.toString(getReferencedEntity(), ts2 -> "");
    return sb;
  }
}
