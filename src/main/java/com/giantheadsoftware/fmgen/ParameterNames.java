package com.giantheadsoftware.fmgen;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author clininger $Id: $
 */
public interface ParameterNames {

  /**
   * Maven logger.
   */
  static final String LOG = "log";
  /**
   * If false, this execution is skipped
   */
  static final String ENABLE = "enable";
  /**
   * Project base directory.
   */
  static final String BASE_DIR = "baseDir";
  /**
   * Build target directory.
   */
  static final String TARGET_DIR = "targetDir";
  /**
   * Target directory of generated files.
   */
  static final String GEN_DIR = "genDir";
  /**
   * Target dir of the generated files, relative to GEN_DIR.
   */
  static final String BUILD_DIR = "buildDir";
  /**
   * Optional regex replace expression that will be applied to transform class names.
   */
  static final String CLASSNAME_MAP = "classNameMap";
  /**
   * Target dir of the generated files, relative to GEN_DIR.
   */
  static final String TARGET_PATH = "targetPath";
  /**
   * Optional regex replace pattern that can transform package names from source to destination.
   * This is ignored if targetPath is specified.
   */
  static final String PACKAGE_MAP = "packageMap";
  /**
   * Optional regex replacement map applied to each file.  Can be used to assign a different target path to each file.
   */
  static final String TARGET_PATH_MAP = "targetPathMap";

  /**
   * Optional list of regex match/replace patterns that will be applied to import paths
   */
  static final String IMPORT_MAPS = "importMaps";
  /**
   * Extension for generated files, default = ".java"
   */
  static final String TARGET_FILE_EXT = "targetFileExt";
  /**
   * Maven compilation classpath.
   */
  static final String COMPILE_CLASSPATH = "compileClasspath";
  /**
   * Model properties section: Map&lt;String,Object&gt;.
   */
  static final String MODEL_PROPS = "modelProps";
  /**
   * Custom properties section: Map&lt;String,Object&gt;.
   */
  static final String CUSTOM_PROPS = "customProps";
  /**
   * Pattern used to match model sources to be included. Default: ".*"
   */
  static final String INCLUDE_MODEL_PATTERN = "includeModelPattern";
  /**
   * Pattern used to match model sources to be excluded. Default: "^$"
   */
  static final String EXCLUDE_MODEL_PATTERN = "excludeModelPattern";
  /**
   * Pattern used to match model sources to be included. Default: ".*"
   */
  static final String INCLUDE_MEMBER_PATTERN = "includeMemberPattern";
  /**
   * Pattern used to match model sources to be excluded. Default: "^$"
   */
  static final String EXCLUDE_MEMBER_PATTERN = "excludeMemberPattern";
  /**
   * Pattern used to match model sources to be included. Default: ".*"
   */
  static final String INCLUDE_MODEL_ANNOTATION_PATTERN = "includeModelAnnotationPattern";
  /**
   * Pattern used to match model sources to be excluded. Default: "^$"
   */
  static final String EXCLUDE_MODEL_ANNOTATION_PATTERN = "excludeModelAnnotationPattern";
  /**
   * Pattern used to match model sources to be included. Default: ".*"
   */
  static final String INCLUDE_MEMBER_ANNOTATION_PATTERN = "includeMemberAnnotationPattern";
  /**
   * Pattern used to match model sources to be excluded. Default: "^$"
   */
  static final String EXCLUDE_MEMBER_ANNOTATION_PATTERN = "excludeMemberAnnotationPattern";
  /**
   * Pattern used to match model sources to be included. Default: ".*"
   */
  static final String INCLUDE_TYPE_PATTERN = "includeTypePattern";
  /**
   * Pattern used to match model sources to be excluded. Default: "^$"
   */
  static final String EXCLUDE_TYPE_PATTERN = "excludeTypePattern";
  /**
   * Pattern used to match model sources to be included. Default: ".*"
   */
  static final String INCLUDE_ARG_TYPE_PATTERN = "includeArgTypePattern";
  /**
   * Pattern used to match model sources to be excluded. Default: "^$"
   */
  static final String EXCLUDE_ARG_TYPE_PATTERN = "excludeArgTypePattern";
  /**
   * Boolean indicating whether enums should be scanned. Default: "false"
   */
  static final String INCLUDE_ENUMS = "includeEnums";
  /**
   * List of fully qualifies class names to be added to the imports of every generated class
   */
  static final String IMPORTS = "imports";
  /**
   * Pattern used to match model sources to be excluded. Default: "^$"
   */
  static final String IMPORT_DEFAULT_COLLECTIONS = "importDefaultCollections";
  /**
   * If true, scan methods in super classes for annotations
   */
  static final String INHERIT_METHOD_ANNOTATIONS = "inheritMethodAnnotations";
  /**
   * Pattern used to match model sources to be excluded. Default: "^$"
   */
  static final String DEFAULT_COLLECTIONS = "defaultCollections";
  /**
   * List of Maps containing aggregator properties
   */
  static final String AGGREGATORS = "aggregators";
  /**
   * Class that will produce the model.
   */
  static final String MODEL_PRODUCER = "modelProducer";
  /**
   * If provided, look for model files in this location, relative to build dir or absolute
   */
  static final String SRC_DIRS = "srcDirs";
  /**
   * if true, then the project classpath is not scanned for model files
   */
  static final String EXCLUDE_CLASSPATH = "excludeClasspath";
  /**
   * Package where model classes can be found.
   */
  static final String MODEL_BASE_PATH = "modelBasePath";
  /**
   * A regex pattern applied to any potential package name (after filtering by modelBasePath)
   */
  static final String MODEL_PATH_INCLUDE_PATTERN = "modelPathIncludePattern";
  /**
   * A regex pattern applied to any potential package name (after filtering by modelBasePath)
   * @deprecated use MODEL_PATH_INCLUDE_PATTERN instead
   */
  static final String MODEL_PATH_PATTERN = "modelPathPattern";
  /**
   * A regex pattern applied to any potential package name (after filtering by modelBasePath)
   */
  static final String MODEL_PATH_EXCLUDE_PATTERN = "modelPathExcludePattern";
  /**
   * Class that will locate and load FreeMarker templates.
   */
  static final String TEMPLATE_LOADER = "templateLoader";
  /**
   * Path to the FreeMarker template files.
   */
  static final String TEMPLATE_PATH = "templatePath";
  /**
   * Path (relative to TEMPLATE_PACKAGE) of the template to apply.
   */
  static final String TEMPLATE = "template";

  //Properties in the baseClass map
  /**
   * name of the aggregator.
   */
  static final String AGG__NAME = "name";
  /**
   * Template to be used for an aggregator.
   */
  static final String AGG__TEMPLATE = "template";
  /**
   * Output file name for the base class.
   */
  static final String AGG__FILE_NAME = "fileName";
  /**
   * Package name -- derived from targetPath.
   */
  static final String AGG__PACKAGE_NAME = "packageName";
  /**
   * Property that holds the params passed into the mojo
   */
  static final String AGG__PARAMS = "params";

  // kmown custom parameter names
  static final String IFC_PACKAGE = "ifc-package";
  static final String IFC_CLASSNAME_MAP = "ifc-class-name-map";
  static final String BASE_CLASS_NAME = "baseClassName";
  static final String ABSTRACTS = "abstracts";

}
