package com.giantheadsoftware.fmgen;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import com.giantheadsoftware.fmgen.model.Aggregator;
import com.giantheadsoftware.fmgen.model.Model;
import com.giantheadsoftware.fmgen.model.ModelWrapper;
import com.giantheadsoftware.fmgen.model.ProducerWrapper;
import com.giantheadsoftware.fmgen.model.java.JavaDtoProducer;
import com.giantheadsoftware.fmgen.template.ResourceTemplateLoader;
import com.giantheadsoftware.fmgen.template.TemplateLoader;
import com.giantheadsoftware.fmgen.util.RegexReplace;
import com.giantheadsoftware.fmgen.util.Util;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.MessageFormat;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.descriptor.MojoDescriptor;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import static com.giantheadsoftware.fmgen.ParameterNames.*;

/**
 * Mojo that uses FreeMarker to generate source files.
 *
 * @author clininger
 */
@Mojo(name = "fmgen",
    defaultPhase = LifecyclePhase.GENERATE_SOURCES,
    requiresDependencyResolution = ResolutionScope.COMPILE)
public class FmGenMojo extends AbstractMojo {

  private static final String INCLUDE_ALL = ".*";
  private static final String EXCLUDE_NONE = "^$";

  @Parameter(name = ENABLE, defaultValue = "true")
  private boolean enable;

  /**
   * This is supposed to be the base directory of the project, i.e. where the POM file resides. For some reason, the Mojo can't access the variable ${project.basedir}. It can
   * access ${project.build.directory}, so this is a hack to get the baseDir from there. The directory-maven-plugin would probably work too.
   */
  @Parameter(name = BASE_DIR, defaultValue = "${project.basedir}")
  private String baseDir;

  @Parameter(name = TARGET_DIR, defaultValue = "${project.build.directory}")
  private String targetDir;

  /**
   * Classpath where the model sources are located.
   */
  @Parameter(property = "project.compileClasspathElements", required = true, readonly = true)
  private List<String> classpath;

  /**
   * If true, don't search for model sources in the classpath.
   */
  @Parameter(name = EXCLUDE_CLASSPATH)
  private Boolean excludeClasspath;

  /**
   * Root path where generated files will be written.
   */
  @Parameter(defaultValue = "${project.build.directory}/generated-sources/fmgen", property = GEN_DIR, required = true)
  private File genDir;

  @Parameter(defaultValue = "${project.build.directory}", required = true)
  private File buildDir;

  @Parameter(name = SRC_DIRS)
  private List<String> srcDirs;

  /**
   * Base path to scan for model sources, relative to the classpath root. Use file separators, start without a slash. Only paths (i.e. classpath entries) that begin with this
   * string are searched for models.
   */
  @Parameter(name = MODEL_BASE_PATH, required = true, defaultValue = "")
  private String modelBasePath;

  /**
   * Pattern applied to potential model paths after being filtered by modelBasePath. Only paths matching this pattern are searched for model sources. This pattern is only applied
   * to the path, not the filename. Use the includeModelPattern/excludeModelPattern for filtering file names.
   */
  @Parameter(name = MODEL_PATH_INCLUDE_PATTERN, required = true, defaultValue = ".*")
  private String modelPathIncludePattern;

  /**
   * Pattern applied to potential model paths after being filtered by modelBasePath. Only paths NOT matching this pattern are searched for model sources. This pattern is only applied
   * to the path, not the filename. Use the includeModelPattern/excludeModelPattern for filtering file names.
   */
  @Parameter(name = MODEL_PATH_EXCLUDE_PATTERN, required = false, defaultValue = "^$")
  private String modelPathExcludePattern;

  /**
   * Pattern applied to potential model paths after being filtered by modelBasePath. Only paths matching this pattern are searched for model sources. This pattern is only applied
   * to the path, not the filename. Use the includeModelPattern/excludeModelPattern for filtering file names.
   * @deprecated Use MODEL_PATH_INCLUDE_PATTERN
   */
  @Deprecated()
  @Parameter(name = MODEL_PATH_PATTERN, required = false)
  private String modelPathPattern;

  /**
   * Properties passed to producers.  These will be added as a "hash" to the top level model under the property "modelProps"
   */
  @Parameter(name = MODEL_PROPS)
  private Map<String, Object> modelProps;

  /**
   * Properties passed to producers.
   */
  @Parameter(name = CUSTOM_PROPS)
  private Map<String, Object> customProps;

  /**
   * Optional regex pattern that will match model files to include. Applied only to the base file name.  Default is .* (all)
   */
  @Parameter(name = INCLUDE_MODEL_PATTERN, defaultValue = INCLUDE_ALL)
  private String includeModelPattern;

  /**
   * Optional regex pattern that will match model files to exclude. Applied only to the base file name. Default is ^$ (nothing)
   */
  @Parameter(name = EXCLUDE_MODEL_PATTERN, defaultValue = EXCLUDE_NONE)
  private String excludeModelPattern;

  /**
   * Optional regex pattern that will match model members to include. Default is .* (all)
   */
  @Parameter(name = INCLUDE_MEMBER_PATTERN, defaultValue = INCLUDE_ALL)
  private String includeMemberPattern;

  /**
   * Optional regex pattern that will match model members to exclude. Default is ^$ (nothing)
   */
  @Parameter(name = EXCLUDE_MEMBER_PATTERN, defaultValue = EXCLUDE_NONE)
  private String excludeMemberPattern;

  /**
   * Optional regex pattern that will match model types to include. Default is .* (all)
   */
  @Parameter(name = INCLUDE_TYPE_PATTERN, defaultValue = INCLUDE_ALL)
  private String includeTypePattern;

  /**
   * Optional regex pattern that will match model types to exclude. Default is ^$ (nothing)
   */
  @Parameter(name = EXCLUDE_TYPE_PATTERN, defaultValue = EXCLUDE_NONE)
  private String excludeTypePattern;

  /**
   * Optional regex pattern that will match class-level annotations to include. Default is .* (all)
   */
  @Parameter(name = INCLUDE_MODEL_ANNOTATION_PATTERN, defaultValue = INCLUDE_ALL)
  private String includeModelAnnotationPattern;

  /**
   * Optional regex pattern that will match class-level annotations to exclude. Default is ^$ (nothing)
   */
  @Parameter(name = EXCLUDE_MODEL_ANNOTATION_PATTERN, defaultValue = EXCLUDE_NONE)
  private String excludeModelAnnotationPattern;

  /**
   * Optional regex pattern that will match member-level annotations to include. Default is .* (all)
   */
  @Parameter(name = INCLUDE_MEMBER_ANNOTATION_PATTERN, defaultValue = INCLUDE_ALL)
  private String includeMemberAnnotationPattern;

  /**
   * Optional regex pattern that will match member-level annotations to exclude. Default is ^$ (nothing)
   */
  @Parameter(name = EXCLUDE_MEMBER_ANNOTATION_PATTERN, defaultValue = EXCLUDE_NONE)
  private String excludeMemberAnnotationPattern;

  /**
   * Optional regex pattern that will match arg types to include. Default is .* (all)
   */
  @Parameter(name = INCLUDE_ARG_TYPE_PATTERN, defaultValue = INCLUDE_ALL)
  private String includeArgTypePattern;

  /**
   * Optional regex pattern that will match arg types to exclude. Default is ^$ (nothing)
   */
  @Parameter(name = EXCLUDE_ARG_TYPE_PATTERN, defaultValue = EXCLUDE_NONE)
  private String excludeArgTypePattern;

  /**
   * If true, enums will be included in the scanned models.  See README for moe info.  Default = false.
   */
  @Parameter(name = INCLUDE_ENUMS, defaultValue = "false")
  private boolean includeEnums;

  /**
   * A list of FQ class names to be included as imports in the generated files.
   */
  @Parameter(name = IMPORTS)
  private List<String> imports;

  /**
   * If set to true, then the default collection will be added to the imports when a member uses a collection type
   */
  @Parameter(name = IMPORT_DEFAULT_COLLECTIONS, defaultValue = "false")
  private boolean importDefaultCollections;

  /**
   * A map of classes to be used instead of the defaults for concrete collection types.
   */
  @Parameter(name = DEFAULT_COLLECTIONS)
  private Map<String, String> defaultCollections;

  /**
   * If true, scan methods in superclasses & interfaces for annotations
   */
  @Parameter(name = INHERIT_METHOD_ANNOTATIONS, defaultValue = "false")
  private boolean inheritMethodAnnotations;
  /**
   * Optional list of Aggregators to be applied.
   */
  @Parameter(name = AGGREGATORS)
  private List<Aggregator> aggregators;

  /**
   * Classname of the model producer implementation.
   */
  @Parameter(name = MODEL_PRODUCER, required = true)
  private String modelProducer;

  /**
   * Path to the FreeMarker templates, relative to the classpath.
   */
  @Parameter(name = TEMPLATE_PATH, required = true)
  private String templatePath;

  /**
   * Classname of the template loader implementation.
   */
  @Parameter(name = TEMPLATE_LOADER, required = true)
  private String templateLoader;

  /**
   * Filename of the Freemarker template.
   */
  @Parameter(name = TEMPLATE)
  private String template;

  /**
   * Where to write the files, relative to the genDir.  This overrides packageMap if provided.
   */
  @Parameter(name = TARGET_PATH)
  private String targetPath;

  /**
   * Where to write the files, relative to the genDir.  Not used if targetPath is provided
   */
  @Parameter(name = PACKAGE_MAP)
  private String packageMap;

  /**
   * Optional regex replacement map applied to each file.  Can be used to assign a different target path to each file.
   */
  @Parameter(name = TARGET_PATH_MAP)
  private String targetPathMap;

  /**
   * Regex match/replace patterns that will be applied to FQN classes in the imports list
   */
  @Parameter(name = IMPORT_MAPS)
  private List<String> importMaps;

  /**
   * Target file extension, default = ".java"
   */
  @Parameter(name = TARGET_FILE_EXT, required = true, defaultValue = ".java")
  private String targetFileExt;

  @Parameter
  private MojoDescriptor mojoDescriptor;

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    Util.log = getLog();

    if (!enable) {
      getLog().info("Execution disabled by config");
      return;
    }

    getLog().info("PLUGIN CONTEXT:");
    getPluginContext().entrySet().stream().forEach(entry -> getLog().info(String.valueOf(entry) + '\n'));

    List<String> absoluteSrcDirs = new ArrayList<>();

    if (srcDirs != null) {
      srcDirs.stream()
          .map(srcDir -> srcDir.startsWith("/") ? new File(srcDir) : new File(buildDir, srcDir))
          .filter(src -> src.exists() && src.canRead())
          .forEach(src -> absoluteSrcDirs.add(src.getAbsolutePath()));
    }

    if (modelProducer != null) {
      // expand the producer & load classes if necessary
      if (!modelProducer.contains("."))
        modelProducer = JavaDtoProducer.class.getPackage().getName() + "." + modelProducer;
      if (!templateLoader.contains("."))
        templateLoader = ResourceTemplateLoader.class.getPackage().getName() + "." + templateLoader;
      try {
        // load the model producer class
        getLog().info("Loading model producer " + modelProducer);
        Class<?> modelProducerClass = Class.forName(modelProducer);
        // initialize the params for the producer
        Map<String, Object> modelParams = new HashMap<>();
        if (customProps != null)
          modelParams.putAll(customProps);
        modelParams.put(LOG, getLog());
        if (baseDir == null) {
          baseDir = targetDir != null ? new File(targetDir).getParent() : "";
        }
        getLog().info("BASE_DIR: " + baseDir);
        modelParams.put(BASE_DIR, baseDir);
        modelParams.put(COMPILE_CLASSPATH, classpath);
        modelParams.put(BUILD_DIR, buildDir.getAbsolutePath());
        modelParams.put(SRC_DIRS, absoluteSrcDirs);
        modelParams.put(EXCLUDE_CLASSPATH, excludeClasspath);
        modelParams.put(MODEL_BASE_PATH, modelBasePath);
        modelParams.put(MODEL_PATH_INCLUDE_PATTERN, modelPathPattern != null ? modelPathPattern : modelPathIncludePattern);
        modelParams.put(MODEL_PATH_EXCLUDE_PATTERN, modelPathExcludePattern);
        modelParams.put(TARGET_PATH, targetPath);
        modelParams.put(PACKAGE_MAP, packageMap);
        modelParams.put(IMPORT_MAPS, importMaps);
        modelParams.put(TARGET_FILE_EXT, targetFileExt);
        modelParams.put(MODEL_PROPS, modelProps);

        // model patterns
        modelParams.put(INCLUDE_MODEL_PATTERN, Pattern.compile(includeModelPattern));
        modelParams.put(EXCLUDE_MODEL_PATTERN, Pattern.compile(excludeModelPattern));

        // member patterns
        modelParams.put(INCLUDE_MEMBER_PATTERN, Pattern.compile(includeMemberPattern));
        modelParams.put(EXCLUDE_MEMBER_PATTERN, Pattern.compile(excludeMemberPattern));

        // model annotation patterns
        modelParams.put(INCLUDE_MODEL_ANNOTATION_PATTERN, Pattern.compile(includeModelAnnotationPattern));
        modelParams.put(EXCLUDE_MODEL_ANNOTATION_PATTERN, Pattern.compile(excludeModelAnnotationPattern));

        // model annotation patterns
        modelParams.put(INCLUDE_MEMBER_ANNOTATION_PATTERN, Pattern.compile(includeMemberAnnotationPattern));
        modelParams.put(EXCLUDE_MEMBER_ANNOTATION_PATTERN, Pattern.compile(excludeMemberAnnotationPattern));

        // type patterns
        modelParams.put(INCLUDE_TYPE_PATTERN, Pattern.compile(includeTypePattern));
        modelParams.put(EXCLUDE_TYPE_PATTERN, Pattern.compile(excludeTypePattern));

        // arg patterns
        modelParams.put(INCLUDE_ARG_TYPE_PATTERN, Pattern.compile(includeArgTypePattern));
        modelParams.put(EXCLUDE_ARG_TYPE_PATTERN, Pattern.compile(excludeArgTypePattern));

        modelParams.put(INCLUDE_ENUMS, includeEnums);

        modelParams.put(IMPORTS, imports != null ? imports : new ArrayList<String>(0));

        modelParams.put(IMPORT_DEFAULT_COLLECTIONS, importDefaultCollections);
        modelParams.put(INHERIT_METHOD_ANNOTATIONS, inheritMethodAnnotations);
        final Map<String, Class<?>> collectionDefaults = new HashMap<>();
        collectionDefaults.put("list", LinkedList.class);
        collectionDefaults.put("set", LinkedHashSet.class);
        collectionDefaults.put("map", LinkedHashMap.class);
        if (defaultCollections != null) {
          defaultCollections.entrySet().stream()
              .map(entry -> new SimpleEntry<>(entry.getKey(), entry.getValue().contains(".") ? entry.getValue() : "java.util." + entry.getValue()))
              .map(entry -> {
                try {
                  return new SimpleEntry<String, Class<?>>(entry.getKey(), Class.forName(entry.getValue()));
                } catch (ClassNotFoundException x) {
                  getLog().error("Can't load collection class for type " + entry.getKey(), x);
                  return null;
                }
              })
              .filter(entry -> entry != null)
              .forEach(entry -> collectionDefaults.put(entry.getKey(), entry.getValue()));
        }
        modelParams.put(DEFAULT_COLLECTIONS, collectionDefaults);

        getLog().debug("Plugin Model Params:");
        modelParams.entrySet().stream().forEach(param -> getLog().debug(param.getKey() + " -> " + String.valueOf(param.getValue())));


        new RegexReplace(targetPathMap, modelParams);
        // instantiate and run the model producer
        Object modelProducerInstance = modelProducerClass.getConstructor(Map.class).newInstance(modelParams);
        Map<String, ? extends Model> models = new ProducerWrapper(modelProducerInstance).getModels();

        if (models.isEmpty() && (aggregators == null || aggregators.stream().noneMatch(a -> a.isRunWithoutModels()))) {
          getLog().info("No models loaded, no aggregators configured.  Nothing to generate.");
          return;
        }
        // load the template loader class
        getLog().info("Loading template loader " + templateLoader);
        Class<? extends TemplateLoader> templateLoaderClass = (Class<? extends TemplateLoader>) Class.forName(templateLoader);

        // initialize the params for the template loader
        Map<String, Object> templateParams = new HashMap<>();
        templateParams.put(LOG, getLog());
        templateParams.put(COMPILE_CLASSPATH, classpath);
        templateParams.put(BASE_DIR, modelParams.get(BASE_DIR));

        getLog().info(TEMPLATE_PATH + " = " + templatePath);
        templateParams.put(TEMPLATE_PATH, templatePath);
//        getLog().info(TARGET_PATH + " = " + targetPath);
//        templateParams.put(TARGET_PATH, targetPath);

        // instnatiate the template loader and run
        TemplateLoader templateLoaderInstance = templateLoaderClass.getConstructor(Map.class).newInstance(templateParams);
        Configuration cfg = templateLoaderInstance.getConfiguration();

        createGenDir(genDir);

        getLog().info(GEN_DIR + ": " + genDir.getAbsolutePath());

        // run the aggregator templates, if specified
        if (aggregators != null) {
          for (Aggregator aggregator : aggregators) {
//						aggregator.put(AGG__PACKAGE_NAME, targetPath.replace("/", "."));
            getLog().info("Aggregator: " + aggregator.getName());
            aggregator.setParams(modelParams);
            aggregator.setModels((Collection<Model>) models.values());
            File aggGenDir = aggregator.getGenDir() != null ? aggregator.getGenDir() : genDir;
            createGenDir(aggGenDir);
            getLog().info("loading aggregator template /" + aggregator.getTemplate());
            Template aggTemplate = cfg.getTemplate("/" + aggregator.getTemplate() + ".ftl");
            processTemplate(aggTemplate, aggregator, aggGenDir, modelParams, null);
          }
        }
        if (template == null || template.isEmpty()) {
          getLog().info("No template specified:  skipping mode generation");
          return;
        }
        String templateRelPath = template + ".ftl";
        getLog().info("loading template " + templateRelPath);
        Template mainTemplate = cfg.getTemplate(templateRelPath);
        RegexReplace targetPathReplace = targetPathMap == null || targetPathMap.isEmpty() ? null :  new RegexReplace(targetPathMap, modelParams);
        for (Model model : models.values()) {
          processTemplate(mainTemplate, model, genDir, modelParams, targetPathReplace);
        }
      } catch (Exception ex) {
        throw new MojoExecutionException(ex.getMessage(), ex);
      }

    } else {
      getLog().error("No modelProducer specified in the config:  models not produced.");
    }
  }

  private void createGenDir(File genDir) throws MojoExecutionException {
    if (!genDir.exists()) {
      genDir.mkdirs();
    } else if (!genDir.isDirectory() && !genDir.canWrite()) {
      throw new MojoExecutionException(MessageFormat.format("The genDir [{0}] is not a dir that can be written.", genDir.getAbsolutePath()));
    }
  }

  private void processTemplate(Template template, Model model, File parentDir, Map<String, Object> modelParams, RegexReplace targetPathReplace) throws TemplateException, IOException, MojoExecutionException {
    model.setTemplate(template.getName());
    String relativePath = new ModelWrapper(model, modelParams).getTargetFileName();

    relativePath = processTargetPathMap(relativePath, targetPathReplace);

    relativePath = processFileNameOperators(relativePath);

    File outFile = new File(parentDir.getAbsolutePath() + '/' + relativePath);

//    outFile = new File(outFile.getParent(), processFileNameOperators(outFile.getName()));
    getLog().info("Generating file " + outFile);
    if (!outFile.getParentFile().exists())
      outFile.getParentFile().mkdirs();
    Writer w = new OutputStreamWriter(new FileOutputStream(outFile));
    template.process(model, w);
  }

  private String processTargetPathMap(String filePath, RegexReplace targetPathReplace) {
    if (targetPathReplace == null) {
      return filePath;
    }
    return targetPathReplace.replace(filePath);
  }

  private String processFileNameOperators(String relFilePath) {
    int lastDot = targetFileExt.lastIndexOf('.');
    String finalExt = lastDot < 0 ? targetFileExt : targetFileExt.substring(lastDot);
    // if the target is Java, make the file uppercase on the first char
    switch (finalExt) {
      case ".java":
        int lastSlash = relFilePath.lastIndexOf('/');
        return relFilePath.substring(0, lastSlash+1) + Util.toCamel(relFilePath.substring(lastSlash+1));
      case ".ts":
      case ".js":
        return Util.toSlug(relFilePath);
      default:
        return relFilePath;
    }
  }

}
