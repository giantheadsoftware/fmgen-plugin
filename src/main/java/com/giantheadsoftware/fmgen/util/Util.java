package com.giantheadsoftware.fmgen.util;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author clininger $Id: $
 */
public class Util {
  public static Log log;

  private static final Pattern FIRST_LETTER_PATTERN = Pattern.compile("([^\\w][A-Z])");
  private static final Pattern CAMEL_LETTER_PATTERN = Pattern.compile("(\\w[A-Z])");
  private static final Pattern SLUG_LETTER_PATTERN = Pattern.compile("(-[a-z])");

  /**
   * Converts file path to Java package.
   * @param filePath Must be relative to source root (i.e com/, org/, etc); file separator must be forward slash '/'
   * @return
   */
  public static final String filePath2Package(String filePath) {
    if (filePath == null)
      return null;
    String path = (filePath.charAt(0) == '/') ? filePath.substring(1) : filePath;
    return path.replaceAll("/", ".");
  }

  /**
   * Convert the package to a file path.
   * @param packageName the name of a Java package
   * @return the file path, with '.' converted to '/' (always forward slash, even in Windows)
   */
  public static final String package2FilePath(String packageName) {
    if (packageName == null)
      return null;
    return packageName.replaceAll("\\.", "/");
  }

  public static final String indentStruct(String in) {
    return in;
//    Stack<String> indents = new Stack<>();
//    final String indent = "  ";
//    StringBuilder sb = new StringBuilder();
//    in.chars().forEachOrdered((int value)
//        -> {
//      switch (value) {
//        case '[':
//        case '{':
//          indents.push(indent);
//          break;
//        case ']':
//        case '}':
//          if (!indents.isEmpty())
//            indents.pop();
//          break;
//      }
//      if (sb.length() > 0 && sb.charAt(sb.length() - 1) == '\n')
//        indents.stream().forEach(s -> sb.append(s));
//      sb.append((char) value);
//    });
//    return sb.toString();
  }

  /**
   * Formatted string representation of a collection.
   * @param coll The collection to represent as a string
   * @return Formatted string representation of a collection
   */
  public static final String collectionToString(Collection coll) {
    StringBuilder sb = new StringBuilder("[");
    boolean first = true;
    for (Object o : coll) {
      if (!first)
        sb.append(',');
      sb.append('\n').append(String.valueOf(o));
      first = false;
    }
    if (!first)
      sb.append('\n');
    return sb.append(']').toString();
  }

  /**
   * @param value value to test
   * @param includePattern include pattern
   * @param excludePattern exclude pattern
   * @return true if the value matches the includePattern and does not match the excludePattern
   */
  public static boolean checkStringPattern(String value, Pattern includePattern, Pattern excludePattern) {
    return includePattern.matcher(value).matches() && !excludePattern.matcher(value).matches();
  }

  /**
   * @param annos annotations to test
   * @param includeAnnotationPattern include pattern
   * @param excludeAnnotationPattern exclude pattern
   * @return true if any annotation for the class matches the include pattern, but none match the exclude pattern.
   * If there are no annotations, verify that the includePattern allows it.
   */
  public static boolean checkAnnotations(Annotation[] annos, Pattern includeAnnotationPattern, Pattern excludeAnnotationPattern) {
    if (annos.length == 0) {
      return includeAnnotationPattern.matcher("").matches();  // make sure we're allowed to have no annotations
    }
    boolean allowed = false;
    for (Annotation anno : annos) {
      // deny immediately if any annotation is excluded
      if (excludeAnnotationPattern.matcher(anno.annotationType().getName()).matches()) {
        return false;
      }
      // look for at least 1 annotation that matches the include pattern
      if (!allowed && includeAnnotationPattern.matcher(anno.annotationType().getName()).matches()) {
        allowed = true;
      }
    }
    return allowed;
  }

  /**
   * @param args method args
   * @param includeAnnotationPattern include pattern
   * @param excludeAnnotationPattern exclude pattern
   * @return true if any parameter for the method matches the include pattern, but none match the exclude pattern.
   * If there are no annotations, verify that the includePattern allows it.
   */
  public static boolean checkParamTypes(Parameter[] args, Pattern includeAnnotationPattern, Pattern excludeAnnotationPattern) {
    if (args.length == 0) {
      return includeAnnotationPattern.matcher("").matches();  // make sure we're allowed to have no annotations
    }
    boolean allowed = false;
    for (Parameter arg : args) {
      // deny immediately if any annotation is excluded
      if (excludeAnnotationPattern.matcher(arg.getType().getName()).matches()) {
        return false;
      }
      // look for at least 1 annotation that matches the include pattern
      if (!allowed && includeAnnotationPattern.matcher(arg.getType().getName()).matches()) {
        allowed = true;
      }
    }
    return allowed;
  }

  public static Class defaultCollectionClass(Class type, Map<String, Class> defaults) {
    if (defaults == null) {
      log.error(new MojoExecutionException("default collections not provided!"));
      return type;
    }
    if (!Modifier.isAbstract(type.getModifiers())) {
      return type;
    }
    if (Set.class.isAssignableFrom(type)) {
      return defaults.get("set");
    }
    if (List.class.isAssignableFrom(type)) {
      return defaults.get("list");
    }
    if (Map.class.isAssignableFrom(type)) {
      return defaults.get("map");
    }
    return type;
  }

  /**
   * Test the type to see if it's an instance of the class c.  For parameterized types
   * the rawType is checked.  For type vars and wild cards, the bounds are tested.
   * @param type Type to test
   * @param baseClass Base class to test
   * @return true if the actual class of type is an instance of class c.
   */
  public static boolean instanceOf(Type type, Class baseClass) {
    if (type instanceof Class) {
      return baseClass.isAssignableFrom((Class)type);
    }
    if (type instanceof ParameterizedType) {
      return instanceOf(((ParameterizedType)type).getRawType(), baseClass);
    }
    if (type instanceof TypeVariable) {
      for (Type bound : ((TypeVariable)type).getBounds()) {
        if (instanceOf(bound, baseClass)) {
          return true;
        }
      }
      return false;
    }
    if (type instanceof WildcardType) {
      for (Type bound : ((WildcardType)type).getLowerBounds()) {
        if (instanceOf(bound, baseClass)) {
          return true;
        }
      }
      for (Type bound : ((WildcardType)type).getUpperBounds()) {
        if (instanceOf(bound, baseClass)) {
          return true;
        }
      }
      return false;
    }
    return false;
  }

  public static String changePackage(final String currentFqn, final String newPackage) {
    String className = currentFqn.substring(currentFqn.lastIndexOf('.')+1);
    return (newPackage != null ? (newPackage + '.') : "") + className;
  }

  public static String toSlug(String camel) {
    StringBuffer sb1 = new StringBuffer();
    sb1.append(camel.substring(0, 1).toLowerCase());
    Matcher m = FIRST_LETTER_PATTERN.matcher(camel.substring(1));
    while (m.find() && m.groupCount() > 0) {
      m.appendReplacement(sb1, ""+m.group(1).charAt(0) + Character.toLowerCase(m.group(1).charAt(1)));
    }
    m.appendTail(sb1);

    StringBuffer sb2 = new StringBuffer();
    Matcher m2 = CAMEL_LETTER_PATTERN.matcher(sb1.toString());
    while (m2.find() && m2.groupCount() > 0) {
      m2.appendReplacement(sb2, m2.group(1).charAt(0) + "-" + Character.toLowerCase(m2.group(1).charAt(1)));
    }
    m2.appendTail(sb2);
    return sb2.toString();
  }

  public static String fileToCamel(String filePath) {
    int idx = filePath.lastIndexOf("/");
    if (idx < 0) {
      return toCamel(filePath);
    }
    return filePath.substring(0, idx+1)+toCamel(filePath.substring(idx+1));
  }

  public static String fileToSlug(String filePath) {
    int idx = filePath.lastIndexOf("/");
    if (idx < 0) {
      return toSlug(filePath);
    }
    return filePath.substring(0, idx+1)+toSlug(filePath.substring(idx+1));
  }

  public static String toCamel(String slug) {
    StringBuffer sb1 = new StringBuffer();
    sb1.append(slug.substring(0, 1).toUpperCase());
//    Matcher m = FIRST_LETTER_PATTERN.matcher(slug.substring(1));
//    while (m.find() && m.groupCount() > 0) {
//      m.appendReplacement(sb1, ""+m.group(1).charAt(0) + Character.toUpperCase(m.group(1).charAt(1)));
//    }
//    m.appendTail(sb1);

//    StringBuffer sb2 = new StringBuffer();
    Matcher m2 = SLUG_LETTER_PATTERN.matcher(slug.substring(1));
    while (m2.find() && m2.groupCount() > 0) {
      m2.appendReplacement(sb1, String.valueOf(m2.group(1).charAt(1)).toUpperCase());
    }
    m2.appendTail(sb1);
    return sb1.toString();
  }
}
