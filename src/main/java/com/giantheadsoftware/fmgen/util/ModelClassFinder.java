package com.giantheadsoftware.fmgen.util;

import com.giantheadsoftware.fmgen.ParameterNames;
import com.giantheadsoftware.fmgen.util.ModelPackageReader.ClassFileFilter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author clininger $Id: $
 */
public class ModelClassFinder extends ModelSourceFinder {

  private final Pattern includeAnnotationPattern;
  private final Pattern excludeAnnotationPattern;
	private final boolean includeEnums;

  /**
   * Locate and load the classes that make up the source of the model.
   * @param pluginParams Parameters of the Maven plugin
   */
  public ModelClassFinder(Map<String, Object> pluginParams) {
    super(pluginParams);
    includeAnnotationPattern = (Pattern) pluginParams.get(ParameterNames.INCLUDE_MODEL_ANNOTATION_PATTERN);
    excludeAnnotationPattern = (Pattern) pluginParams.get(ParameterNames.EXCLUDE_MODEL_ANNOTATION_PATTERN);
		includeEnums = (boolean) pluginParams.get(ParameterNames.INCLUDE_ENUMS);
  }

  /**
   * Use the class loader to scan the class path elements looking for items that are in the modelPackage.
   *
   * @return A list of classes that are in the data model.
   * @throws ClassNotFoundException on error
   * @throws IOException on io error
   */
  public List<Class<?>> getSourceClasses() throws IOException {
    List<Class<?>> classes = new LinkedList<>();
    Util.log.info("Seeking model sources in path, incl, excl patterns: \""+modelBasePath+"\", \""+ modelPathIncludePatternStr +"\", \""+ modelPathExcludePatternStr +"\"");
    Util.log.info("\tModel include, exclude pattern:  \""+includeModelPattern+"\", \""+excludeModelPattern+"\"");
    Util.log.info("\tAnnotation include, exclude pattern:  \""+includeAnnotationPattern+"\", \""+excludeAnnotationPattern+"\"");
    getModelSources(new ClassFileFilter(modelBasePath, modelPathIncludePatternStr, modelPathExcludePatternStr, includeModelPattern, excludeModelPattern)).stream()
        .map(modelSource -> modelSource.name.substring(0, modelSource.name.lastIndexOf(".class")).replace("/", "."))
        .map(this::loadClass)
        .filter(Objects::nonNull)
        .filter(loadedClass -> includeEnums || !loadedClass.isEnum())
        .filter(loadedClass -> Util.checkAnnotations(loadedClass.getAnnotations(), includeAnnotationPattern, excludeAnnotationPattern))
        .forEach(loadedClass -> {
          classes.add(loadedClass);
          log.debug("Loaded class: " + loadedClass.getName());
        });
    return classes;
  }

  private Class<?> loadClass(String classname) {
    try {
      return classLoader.loadClass(classname);
    } catch (ClassNotFoundException x) {
      log.warn(getClass().getSimpleName()+": "+classname+": class not found");
      return null;
    }
  }
}
