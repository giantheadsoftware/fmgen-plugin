package com.giantheadsoftware.fmgen.util;

import com.giantheadsoftware.fmgen.ParameterNames;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.maven.plugin.logging.Log;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author clininger $Id: $
 */
public class ModelSourceFinder {

  protected final URLClassLoader classLoader;
  protected final List<String> compileClasspath;
  protected final boolean excludeClasspath;
  protected final Pattern excludeModelPattern;
  protected final Pattern includeModelPattern;
  protected final Log log;
  protected final String modelBasePath;
  protected final String modelPathIncludePatternStr;
  protected final String modelPathExcludePatternStr;
  protected final List<String> srcDirs;

  public ModelSourceFinder(Map<String, Object> pluginParams) {
    this.compileClasspath = (List<String>) pluginParams.get(ParameterNames.COMPILE_CLASSPATH);
    this.modelBasePath = (String) pluginParams.get(ParameterNames.MODEL_BASE_PATH);
    this.modelPathIncludePatternStr = (String) pluginParams.get(ParameterNames.MODEL_PATH_INCLUDE_PATTERN);
    this.modelPathExcludePatternStr = (String) pluginParams.get(ParameterNames.MODEL_PATH_EXCLUDE_PATTERN);
    this.srcDirs = (List<String>) pluginParams.get(ParameterNames.SRC_DIRS);
    this.excludeClasspath = Boolean.TRUE.equals(pluginParams.get(ParameterNames.EXCLUDE_CLASSPATH));
    this.log = (Log) pluginParams.get(ParameterNames.LOG);

    includeModelPattern = (Pattern) pluginParams.get(ParameterNames.INCLUDE_MODEL_PATTERN);
    excludeModelPattern = (Pattern) pluginParams.get(ParameterNames.EXCLUDE_MODEL_PATTERN);
    this.classLoader = excludeClasspath ? null : initClassLoader();
  }

  /**
   * A model source wraps a JAR or directory where model source files can be located.
   * @param filter Used to limit the source file search
   * @return List of Model sources where the modelPath can be found
   * @throws IOException on io error
   */
  public List<ModelSource> getModelSources(ModelFileFilter filter) throws IOException {
    // start with a list of containers identified in the srcDirs config, if any
    List<String> modelContainers = new ArrayList<>(getSrcDirContainers());
    // search the classpath for more containers, unless excluded
    if (!excludeClasspath) {
      // check for the existence of the modelBasePath in all the classpath locations.  There can be more than one.
      Collections.list(classLoader.findResources(modelBasePath)).stream()
            .map(URL::toExternalForm)
            .map(extUrl -> extUrl.contains("!") ? extUrl.substring(0, extUrl.indexOf('!')) : extUrl)
            .map(extUrl -> {
              try {
                // keep this old call for JDK 1.8 compatibility :-/
                return URLDecoder.decode(extUrl, StandardCharsets.UTF_8.displayName());
              } catch (UnsupportedEncodingException x) {
                log.error(x);
                return null;
              }
            })
            .filter(Objects::nonNull)
            .forEach(modelContainers::add);
    }
    return new ModelPackageReader(log).getPackageContents(modelContainers, filter);
  }

  /**
   * Look in the srcDirs (set in the config) for potential model source containers.  A srcDir entry can
   * be either a directory name or a JAR file name.
   * @return list of containers 
   */
  protected List<String> getSrcDirContainers() {
    return srcDirs
        .stream()
        .map(File::new)
        .map((srcFile) -> {
          if (srcFile.isFile() && srcFile.getName().toLowerCase().endsWith(".jar")) {
            return ModelPackageReader.JAR_FILE_PREFIX + srcFile.getAbsolutePath();
          }
          if (srcFile.isDirectory() && srcFile.canRead()) {
            return ModelPackageReader.FILE_PREFIX + srcFile.getAbsolutePath();
          }
          return null;
        })
        .filter(container -> container != null)
        .collect(Collectors.toList());
  }

  private URLClassLoader initClassLoader() {
    if (!excludeClasspath) {
      try {
        List<URL> urls = new ArrayList<>(compileClasspath.size());
        for (String compileClasspathElement : compileClasspath) {
          log.debug("CPE: " + compileClasspathElement);
          urls.add(new File(compileClasspathElement).toURI().toURL());
        }
        return URLClassLoader.newInstance(urls.toArray(new URL[0]), Thread.currentThread().getContextClassLoader());
      } catch (Exception ex) {
        log.error(ex);
      }
    } else {
      log.info("Classpath excluded");
    }
    return null;
  }

}
