package com.giantheadsoftware.fmgen.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarFile;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.maven.plugin.logging.Log;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * This class is used by the ModelSourceFinder, you probably don't need to use this class directly for anything.
 * @author clininger $Id: $
 */
public class ModelPackageReader {

  public static final String JAR_FILE_PREFIX = "jar:file:";
  public static final String FILE_PREFIX = "file:";

  private final Log log;

  public ModelPackageReader(Log log) {
    this.log = log;
  }

  /**
   * Locate the file contents of the specified package and return the contents.
   *
   * @param modelContainers List of JAR or directory entries that may contain the model files we're looking for
   * @param filter Predicate used to select files based on the path of the classpath entry
   * @return List of file names in the package
   * @throws IOException on io error
   */
  public List<ModelSource> getPackageContents(final List<String> modelContainers, final ModelFileFilter filter) throws IOException {
    List<ModelSource> contents = new LinkedList<>();
    for (String container : modelContainers) {
      if (container.startsWith(JAR_FILE_PREFIX)) {
        // this container is a JAR file
        contents.addAll(getPackageContentsFromJar(container.substring(JAR_FILE_PREFIX.length()), filter));
      } else if (container.startsWith(FILE_PREFIX)) {
        // this container is a directory
        contents.addAll(getPackageContentsFromDir(container.substring(FILE_PREFIX.length()), filter));
      } else {
        log.warn("unsupported container type: " + container);
      }
    }
    return contents;
  }

  /**
   * Return all of the FQ class names from this JAR.
   *
   * @param jarFilePath The full path to the JAR file
   * @param filter Filter to select which classes are taken
   * @return List of fully qualified class names in the JAR matching the filter
   * @throws IOException
   */
  private List<ModelSource> getPackageContentsFromJar(final String jarFilePath, final ModelFileFilter filter) throws IOException {
    log.debug("scanning JAR file: " + jarFilePath);
    final JarFile jarFile = new JarFile(jarFilePath);
    return jarFile.stream()
        .filter(entry -> filter.test(entry.getName()))
        .map(entry -> new ModelSource(entry.getName(), jarFilePath) {
            @Override
            public InputStream getInputStream() {
              try {
                return jarFile.getInputStream(entry);
              } catch (IOException x) {
                log.error(MessageFormat.format("Could not read JAR entry {0} from {1}", entry.getName(), jarFilePath));
                return null;
              }
            }
        })
        .collect(Collectors.toList());
  }

  /**
   * Return all of the FQ class names from this directory.
   *
   * @param dirPath Path to a directory, has already been determined to match the desired package. All classes in this dir will be returned.
   * @param filter The filter applied to the dir contents
   * @return List of fully qualified classes in dirPath
   */
  private List<ModelSource> getPackageContentsFromDir(final String dirPath, final ModelFileFilter filter) {
    log.debug("scanning directory: " + dirPath);
    List<ModelSource> contents = new LinkedList<>();
    File packageDir = new File(dirPath);
    if (packageDir.isDirectory()) {
      File[] files = packageDir.listFiles((dir, name) -> {
        int modelPathIdx = dir.getAbsolutePath().indexOf(filter.getModelBasePath());
        return modelPathIdx >= 0 && filter.test(dir.getAbsolutePath().substring(modelPathIdx) + "/" + name);
      });
      for (final File file : files) {
        final String filePath = file.getAbsolutePath();
        final String fqn = filePath.substring(filePath.indexOf(filter.getModelBasePath()));
        log.debug("---adding model = " + filePath);
        contents.add(new ModelSource(fqn, dirPath) {
          @Override
          public InputStream getInputStream() {
            try {
              return new FileInputStream(file);
            } catch (FileNotFoundException x) {
              log.error(MessageFormat.format("Could not read directory entry {0}", filePath));
              return null;
            }
          }
        });
      }
    }
    return contents;
  }

  /**
   * Filter for files
   */
  public static class BasicFileFilter implements ModelFileFilter {

    private final String extension;
    private final String modelBasePath;
    private final Pattern packagePathIncludePattern;
    private final Pattern packagePathExcludePattern;
    private final Pattern includePattern;
    private final Pattern excludePattern;

    /**
     * Construct the BasicFileFilter
     * @param modelBasePath Model paths must begin with this string
     * @param modelPathIncludePattern If provided, the file must be in a subdir matching this pattern
     * @param fileExtension The required file extension
     * @param modelPathExcludePattern
     * @param includePattern Pattern applied to the base file name (not the full path)
     * @param excludePattern Pattern applied to the base file name (not the full path)
     */
    public BasicFileFilter(String modelBasePath, String modelPathIncludePattern, String modelPathExcludePattern, String fileExtension, Pattern includePattern, Pattern excludePattern) {
      this.modelBasePath = modelBasePath;
      packagePathIncludePattern = modelPathIncludePattern == null
          || ".*".equals(modelPathIncludePattern)
          || modelPathIncludePattern.isEmpty()
          ? null : Pattern.compile(modelPathIncludePattern);
      this.extension = fileExtension;
      this.packagePathExcludePattern = modelPathExcludePattern == null
            || "^$".equals(modelPathExcludePattern)
            || modelPathExcludePattern.isEmpty()
            ? null : Pattern.compile(modelPathExcludePattern);
      this.includePattern = includePattern;
      this.excludePattern = excludePattern;
    }

    @Override
    public boolean test(String fullPath) {
      if (fullPath.startsWith(modelBasePath) && fullPath.endsWith(extension)) {
        int lastSlash = fullPath.lastIndexOf('/');
        if (lastSlash >= 0) {
          if (packagePathExcludePattern != null) {
            // verify that the file is in the correct package (subfolder path)
            String entryPackage = fullPath.substring(0, lastSlash);
            Util.log.debug("testing path to exclude pattern: "+entryPackage+" : "+ packagePathExcludePattern.pattern());
            if (packagePathExcludePattern.matcher(entryPackage).matches()) {
              return false;
            }
          }
          if (packagePathIncludePattern != null) {
            // verify that the file is in the correct package (subfolder path)
            String entryPackage = fullPath.substring(0, lastSlash);
            Util.log.debug("testing path to include pattern: "+entryPackage+" : "+ packagePathIncludePattern.pattern());
            if (!packagePathIncludePattern.matcher(entryPackage).matches()) {
              return false;
            }
          }
          String fileName = fullPath.substring(lastSlash+1, fullPath.lastIndexOf(extension));
          return Util.checkStringPattern(fileName, includePattern, excludePattern);
        }
      }
      return false;
    }

    @Override
    public String getModelBasePath() {
      return modelBasePath;
    }

    @Override
    public String getExtension() {
      return extension;
    }

    @Override
    public Pattern getModelPathPattern() {
      return packagePathIncludePattern;
    }

  }

  /**
   * Filter special for .class files.  Excludes inner classes that have "$" in the filename.
   */
  public static class ClassFileFilter extends BasicFileFilter {

    /**
     * Creates a BasicFileFilter with a ".class" extension
     * @param modelBasePath passed to BasicFileFilter
     * @param modelPathIncludePattern passed to BasicFileFilter
     * @param modelPathExcludePattern passed to BasicFileFilter
     * @param includePattern passed to BasicFileFilter
     * @param excludePattern passed to BasicFileFilter
     */
    public ClassFileFilter(String modelBasePath, String modelPathIncludePattern, String modelPathExcludePattern, Pattern includePattern, Pattern excludePattern) {
      super(modelBasePath, modelPathIncludePattern, modelPathExcludePattern, ".class", includePattern, excludePattern);
    }

    @Override
    public boolean test(String fullPath) {
      return !fullPath.contains("$") && super.test(fullPath);  // don't accept inner classes
    }

  }

  /**
   * Filter for files ending in .json.
   */
  public static class JsonFileFilter extends BasicFileFilter {

    /**
     * Creates a BasicFileFilter with a ".json" extension
     * @param modelBasePath passed to BasicFileFilter
     * @param modelPathIncludePattern passed to BasicFileFilter
     * @param modelPathExcludePattern passed to BasicFileFilter
     * @param includePattern passed to BasicFileFilter
     * @param excludePattern passed to BasicFileFilter
     */
    public JsonFileFilter(String modelBasePath, String modelPathIncludePattern, String modelPathExcludePattern, Pattern includePattern, Pattern excludePattern) {
      super(modelBasePath, modelPathIncludePattern, modelPathExcludePattern, ".json", includePattern, excludePattern);
    }

  }
}
