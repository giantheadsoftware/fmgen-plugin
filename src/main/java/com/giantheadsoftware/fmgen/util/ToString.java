package com.giantheadsoftware.fmgen.util;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * common operations for rendering the model as strings
 *
 * @author clininger $Id: $
 */
public class ToString {

  private final Map<Object, String> done = new HashMap<>();
  private int depth;
  private String indent = "";

  public String toString(Object thing, Function<ToString, String> content) {
    String string;
    if (thing == null) {
      string = "null";
    } else if (done.containsKey(thing)) {
      string = done.get(thing);
    } else if (thing instanceof String) {
      string = '"'+(String) thing+'"';
    } else {
      done.put(thing, "<working...>");
      if (thing instanceof Collection) {
        string = toString((Collection)thing, (ts, elem) -> String.valueOf(elem));
      } else if (thing instanceof Map) {
        string = "(Map) {";
        deeper();
        for (Map.Entry elem : ((Map<Object, Object>) thing).entrySet()) {
          string += String.valueOf(elem.getKey()) + " = ";
          if (content != null) {
            string += content.apply(this) + newLine();
          } else {
            string += indent() + String.valueOf(elem.getValue()) + newLine();
          }
        }
        shallower();
        string += newLine() + "}";
      } else {
        string = '(' + thing.getClass().getSimpleName() + ") { ";
        deeper();
        string += (content != null ? content.apply(this) : String.valueOf(thing));
        shallower();
        string += (string.contains("\n") ? newLine() : " ") + "}";
      }
      done.put(thing, string);
    }
    return string;
  }

  public <T> String toString(Collection<T> things, BiFunction<ToString, T, String> content) {
    if (things == null) {
      return "null";
    }
    if (things.isEmpty()) {
      return "[ ]";
    }
    deeper();
    String string = "[" + (things.size() > 1 ? '\n' : ' ');
    for (Object elem : (Collection) things) {
      if (content != null) {
        string += indent() + content.apply(this, (T)elem);
      } else {
        string += indent() + String.valueOf(elem);
      }
      if (things.size() > 1) {
        string += '\n';
      }
    }
    shallower();
    string += indent() + ']';
    return string;
  }

  private void deeper() {
    indent += "  ";
  }

  private void shallower() {
    if (indent.length() >= 2) {
      indent = indent.substring(2);
    }
  }

  public String indent() {
    return indent;
  }

  public String newLine() {
    return '\n' + indent;
  }

  public String toString(Object thing) {
    return toString(thing, null);
  }

}
