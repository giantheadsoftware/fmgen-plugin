package com.giantheadsoftware.fmgen.util;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.maven.plugin.MojoExecutionException;

/**
 *
 * @author clininger $Id: $
 */
public class RegexReplace {

  private final Pattern match;
  private final String replace;

  /**
   * Decode a string in the form of {@code <delim>match_pattern<delim>replace_pattern<delim>}
   * <p>
   * The first char in the string is determined to be the delimiter.
   * <p>
   * The match_pattern is a regex expression that can include grouping,
   * <p>
   * The replace_pattern is a string that may reference group variables using parens like (n) there n is the group number from the regex pattern (starting at 1)
   * <p>
   * The map string may contain a single variable in the form of {var}, where var is the name of a property in the provided properties. If such a variable exists, it will be
   * replaced by the property value before the patterns are constructed.
   *
   * @param map Replacement pattern, as described above
   * @param properties properties of the execution, to be used as potential replacement values.
   * @throws org.apache.maven.plugin.MojoExecutionException on error
   */
  public RegexReplace(String map, Properties properties) throws MojoExecutionException {
    Pattern search = null;
    String replacement = null;
    if (map != null && !map.isEmpty()) {
      Util.log.debug("replace-pattern: " + map);
      // search for a single variable in the pattern in the form {var}
      // the pattern can include a property from the config in the form of a variable.
      // This var is replaced with the value of the property.
      Matcher m = Pattern.compile("\\{([^}]+)}").matcher(map);
      while (m.find()) {
        String propertyValue = (String) properties.get(m.group(1));
        if (propertyValue == null)
          throw new MojoExecutionException("Class name path replacement: no such property: " + m.group(1));
        map = map.replaceAll("\\{(" + m.group(1) + ")\\}", propertyValue);
        Util.log.debug("Classname map: " + map);
      }
      // the replace delimiter is whatever the 1st char is in the string.
      String delim = String.valueOf(map.charAt(0));
      // split the string on that delimiter char
      String[] args = map.substring(1).split(delim);
      Util.log.debug("Replace ARGS:  " + Arrays.toString(args));
      // the first segment of the pattern is the match pattern
      search = Pattern.compile(args[0]);
      // the 2nd segment of the pattern is the replacement
      replacement = args[1];
    }
    this.match = search;
    this.replace = replacement;
  }

  public RegexReplace(String map, Map<String,Object> params) throws MojoExecutionException {
    this(map, convertToProperties(params));
  }

  /**
   * Replace the source string according to the search/replace patterns.
   * First, the string is tested against the search pattern.  It it matches,
   * the it is replaced by the replacement string.
   * <p>Finally, if the replacement string contains group references, replace them.
   * @param source The string to replace
   * @return the new string if it matches the search pattern.  Otherwise, returns the source string.
   */
  public String replace(String source) {
    if (match == null || source == null) {
      return source;
    }
    // map strings according to the pattern
    Matcher matcher = match.matcher(source);
    if (matcher.matches()) {
      String mapped = replace;
      // if the replacement string calls out groups, replace them.
      for (int i = 0; i <= matcher.groupCount(); i++) {
        mapped = mapped.replaceAll("\\(" + i + "\\)", Matcher.quoteReplacement(matcher.group(i)));
      }
      Util.log.debug("Replaced '"+source+"' with '"+mapped+"'");
      return mapped;
    }
    return source;
  }

  /**
   * Convert a Map&lt;String, Object> to a Properties instance.  What gets added to the properties depends on what type the value is:
   * <ul>
   *   <li>String:  the value is added directly</li>
   *   <li>List:  items in the list which are type String are added individually with the index (n) of the item: param[n]=value</li>
   *   <li>Map:  entries with key and value both String are added as param.key=value</li>
   * </ul>
   *
   * @param params A Map of parameters
   * @return Properties where the only String parameters are included
   */
  private static Properties convertToProperties(Map<String, Object> params) {
    Properties p = new Properties();
    for (Map.Entry<String, Object> param : params.entrySet()) {
      Object value = param.getValue();
      if (value instanceof String) {
        p.setProperty(param.getKey(), (String)param.getValue());
      } else if (value instanceof List) {
        int i = 0;
        for (Object listItem : (List<?>)value) {
          if (listItem instanceof String) {
            p.setProperty(param.getKey()+'['+listItem+']', (String)listItem);
          }
        }
      } else if (value instanceof Map) {
        for (Map.Entry<?, ?> entry : ((Map<?,?>)value).entrySet()) {
          if (entry.getKey() instanceof String && entry.getValue() instanceof String) {
            p.setProperty(param.getKey()+'.'+(String)entry.getKey(), (String)entry.getValue());
          }
        }
      }
    }
    return p;
  }
}
