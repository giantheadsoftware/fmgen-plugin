package com.giantheadsoftware.fmgen.template;

import com.giantheadsoftware.fmgen.ParameterNames;
import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author clininger $Id: $
 */
public class ResourceTemplateLoader implements ParameterNames, TemplateLoader {

  private final Log log;
  private final String templatePath;
  private final List<String> compileClasspath;
  private ClassLoader classLoader;
  private Configuration configuration;

  public ResourceTemplateLoader(Map<String, Object> params) {
    log = (Log) params.get(LOG);
    templatePath = (String) params.get(TEMPLATE_PATH);
    compileClasspath = (List<String>) params.get(COMPILE_CLASSPATH);
    initClassLoader();
  }

  private void initClassLoader() {
    try {
      List<URL> urls = new ArrayList<>(compileClasspath.size());
      for (String compileClasspathElement : compileClasspath) {
        log.debug("Template CPE: " + compileClasspathElement);
        urls.add(new File(compileClasspathElement).toURI().toURL());
      }
      classLoader = URLClassLoader.newInstance(
          urls.toArray(new URL[0]),
          Thread.currentThread().getContextClassLoader());
    } catch (Exception ex) {
      log.error(ex);
    }
  }

  @Override
  public Configuration getConfiguration() {
    if (configuration == null) {
      configuration = new Configuration(Configuration.VERSION_2_3_29);
      configuration.setClassLoaderForTemplateLoading(classLoader, templatePath);
    }
    return configuration;
  }

  @Override
  public Template getTemplate(String templateName) throws MojoExecutionException{
    try {
      return getConfiguration().getTemplate('/'+templateName);
    } catch (IOException x) {
      throw new MojoExecutionException("Error getting template "+templateName, x);
    }
  }
}
