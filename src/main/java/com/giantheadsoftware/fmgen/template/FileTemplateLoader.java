package com.giantheadsoftware.fmgen.template;

import static com.giantheadsoftware.fmgen.ParameterNames.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author clininger $Id: $
 */
public class FileTemplateLoader implements TemplateLoader {

  private final Log log;
  private final String templatePath;
  private Configuration configuration;

  public FileTemplateLoader(Map<String, Object> params) {
    log = (Log) params.get(LOG);
    String path = (String) params.get(TEMPLATE_PATH);
    if ("/".equals(File.separator)) {
      templatePath = path.startsWith("/") || params.get(BASE_DIR) == null ? path : (String) params.get(BASE_DIR) + "/" + path;
    } else {
      // assume Windows and look for a drive letter
      templatePath = path.matches("[a-zA-Z]:.*") ? path : (String) params.get(BASE_DIR) + File.separator + path;
    }
  }

  @Override
  public Configuration getConfiguration() throws MojoExecutionException {
    if (configuration == null) {
      configuration = new Configuration(Configuration.VERSION_2_3_29);
      try {
        configuration.setDirectoryForTemplateLoading(new File(templatePath));
      } catch (IOException ex) {
        throw new MojoExecutionException("Failure setting FTL template dir", ex);
      }
    }
    return configuration;
  }

  @Override
  public Template getTemplate(String templateName) throws MojoExecutionException {
    try {
      return getConfiguration().getTemplate(templateName);
    } catch (IOException x) {
      throw new MojoExecutionException("Error getting template " + templateName, x);
    }
  }
}
