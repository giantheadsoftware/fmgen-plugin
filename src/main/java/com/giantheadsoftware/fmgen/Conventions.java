package com.giantheadsoftware.fmgen;

/*
 * Copyright 2016-2021 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author clininger $Id: $
 */
public interface Conventions {

  /**
   * Reflection constants, expected from every model producer implementation
   */
  interface ProducerMethods {
    static final String GET_MODELS_METHOD = "getModels";
  }

  interface ModelMethods {
    static final String GET_TARGET_FILE_NAME_METHOD = "getTargetFileName";
    static final String SET_TARGET_FILE_NAME_METHOD = "setTargetFileName";
    static final String GET_PLUGIN_PARAMS_METHOD = "getPluginParams";
    static final String GET_PROPERTIES_METHOD = "getProperties";
    static final String GET_TEMPLATE_METHOD = "getTemplate";
    static final String SET_TEMPLATE_METHOD = "setTemplate";
  }
}
