/*
 */
package com.giantheadsoftware.fmgen.model.java;

import com.giantheadsoftware.fmgen.ParameterNames;
import static com.giantheadsoftware.fmgen.ParameterNames.*;
import com.giantheadsoftware.fmgen.test.model1.ListByteArray;
import com.giantheadsoftware.fmgen.util.Util;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author clininger
 */
public class JavaFilteredMethodProducerTest {

  public JavaFilteredMethodProducerTest() {
  }

  @BeforeClass
  public static void setUpClass() {
  }

  @AfterClass
  public static void tearDownClass() {
  }

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  /**
   * Test of scanModel method, of class JavaFilteredMethodProducer.
   *
   * @throws org.apache.maven.plugin.MojoExecutionException
   */
  @Test
  public void testScanModel() throws MojoExecutionException {
    System.out.println("scanModel");
    Class sourceClass = List.class;

    Map<String, Object> params = new HashMap<>();
    params.put(ParameterNames.LOG, new SystemStreamLog());
    Util.log = (Log) params.get(LOG);
    params.put(ParameterNames.COMPILE_CLASSPATH, new ArrayList<String>(0));
    params.put(ParameterNames.MODEL_BASE_PATH, "java/lang");
    params.put(ParameterNames.TARGET_PATH, "fmgen/test/java");

    params.put(INCLUDE_MODEL_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_MODEL_PATTERN, Pattern.compile("^$"));

    // member patterns
    params.put(INCLUDE_MEMBER_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_MEMBER_PATTERN, Pattern.compile("^$"));
    
    // type patterns
    params.put(INCLUDE_TYPE_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_TYPE_PATTERN, Pattern.compile("^$"));

    // annotation patterns
    params.put(INCLUDE_MODEL_ANNOTATION_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_MODEL_ANNOTATION_PATTERN, Pattern.compile("^$"));
    
    // arg type patterns
    params.put(INCLUDE_ARG_TYPE_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_ARG_TYPE_PATTERN, Pattern.compile(".*"));

    // annotation patterns
    params.put(INCLUDE_MEMBER_ANNOTATION_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_MEMBER_ANNOTATION_PATTERN, Pattern.compile("^$"));
    params.put(IMPORTS, new ArrayList<String>(0));

    final Map<String, Class<?>> collectionDefaults = new HashMap<>();
    collectionDefaults.put("list", LinkedList.class);
    collectionDefaults.put("set", LinkedHashSet.class);
    collectionDefaults.put("map", LinkedHashMap.class);
    params.put(DEFAULT_COLLECTIONS, collectionDefaults);
    params.put(IMPORT_DEFAULT_COLLECTIONS, false);
    params.put(INHERIT_METHOD_ANNOTATIONS, false);

    JavaFilteredMethodProducer instance = new JavaFilteredMethodProducer(params);
    JavaClassModel result = instance.scanModel(sourceClass);
    ((Log) params.get(ParameterNames.LOG)).info("TEST RESULT:\n" + Util.indentStruct(result.toString()));
    assertNotNull(result);
  }
	
	@Test
	public void testScanModel1() throws MojoExecutionException {
    System.out.println("scanModel");
    Class sourceClass = ListByteArray.class;

    Map<String, Object> params = new HashMap<>();
		setDefaults(params);
    params.put(ParameterNames.MODEL_BASE_PATH, "com/giantheadsoftware/fmgen/test/model1");
//    params.put(ParameterNames.TARGET_PATH, "fmgen/test/java");
		params.put(ParameterNames.PACKAGE_MAP, "/^com\\.giantheadsoftware\\.fmgen\\.test\\.(\\w+)/model.cdm.(1)");

    JavaFilteredMethodProducer instance = new JavaFilteredMethodProducer(params);
    JavaClassModel result = instance.scanModel(sourceClass);
    ((Log) params.get(ParameterNames.LOG)).info("TEST RESULT:\n" + Util.indentStruct(result.toString()));
    assertNotNull(result);
	}

	private void setDefaults(Map<String, Object> params) {
    params.put(ParameterNames.LOG, new SystemStreamLog());
    Util.log = (Log) params.get(LOG);
    params.put(ParameterNames.COMPILE_CLASSPATH, new ArrayList<String>(0));
    params.put(INCLUDE_MODEL_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_MODEL_PATTERN, Pattern.compile("^$"));

    // member patterns
    params.put(INCLUDE_MEMBER_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_MEMBER_PATTERN, Pattern.compile("^$"));
    
    // type patterns
    params.put(INCLUDE_TYPE_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_TYPE_PATTERN, Pattern.compile("^$"));

    // annotation patterns
    params.put(INCLUDE_MODEL_ANNOTATION_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_MODEL_ANNOTATION_PATTERN, Pattern.compile("^$"));
    
    // arg type patterns
    params.put(INCLUDE_ARG_TYPE_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_ARG_TYPE_PATTERN, Pattern.compile(".*"));

    // annotation patterns
    params.put(INCLUDE_MEMBER_ANNOTATION_PATTERN, Pattern.compile(".*"));
    params.put(EXCLUDE_MEMBER_ANNOTATION_PATTERN, Pattern.compile("^$"));
    params.put(IMPORTS, new ArrayList<String>(0));
    params.put(INHERIT_METHOD_ANNOTATIONS, false);

    final Map<String, Class<?>> collectionDefaults = new HashMap<>();
    collectionDefaults.put("list", LinkedList.class);
    collectionDefaults.put("set", LinkedHashSet.class);
    collectionDefaults.put("map", LinkedHashMap.class);
    params.put(DEFAULT_COLLECTIONS, collectionDefaults);
    params.put(IMPORT_DEFAULT_COLLECTIONS, false);
	}
}
