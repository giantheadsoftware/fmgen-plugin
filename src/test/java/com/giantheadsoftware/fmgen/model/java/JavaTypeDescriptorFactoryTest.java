/*
 */
package com.giantheadsoftware.fmgen.model.java;

import com.giantheadsoftware.fmgen.util.Util;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author clininger
 */
public class JavaTypeDescriptorFactoryTest {

  public JavaTypeDescriptorFactoryTest() {
  }

  @BeforeClass
  public static void setUpClass() {
  }

  @AfterClass
  public static void tearDownClass() {
  }

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testCreateType() throws NoSuchMethodException, MojoExecutionException {
    System.out.println("createType - parameterized");
    Type t = TestApi.class.getMethod("findThings", Set.class).getGenericReturnType();

    Map<String, Object> params = new HashMap<>();
    Properties p = new Properties();
    Log log = new SystemStreamLog();
    Util.log = log;
    JavaTypeRefFactory factory = new JavaTypeRefFactory(params, p, log);
    JavaTypeRef modelRef = factory.createModelRef(TestApi.class);
    JavaClassModel model = new JavaClassModel(modelRef);

    JavaTypeRef memberRef = factory.createTypeRef(t, model);
    JavaMethodModel member = new JavaMethodModel(memberRef);

    assertEquals(List.class, member.getSelfRef().getRawClass());
    assertEquals("java.util.List<java.lang.String>", member.getSelfRef().getType().getTypeName());
  }

//  /**
//   * Test of setSelfVar method, of class JavaTypeDescriptorFactory.
//   */
//  @Test
//  public void testSetSelfVar() {
//    System.out.println("setSelfVar");
//    JavaTypeRef typeRef = null;
//    JavaTypeDescriptorFactory instance = null;
//    instance.setSelfVar(typeRef);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of isModelClass method, of class JavaTypeDescriptorFactory.
//   */
//  @Test
//  public void testIsModelClass() {
//    System.out.println("isModelClass");
//    String fullName = "";
//    JavaTypeDescriptorFactory instance = null;
//    boolean expResult = false;
//    boolean result = instance.isModelClass(fullName);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }
//
//  /**
//   * Test of computeModelInterface method, of class JavaTypeDescriptorFactory.
//   */
//  @Test
//  public void testComputeModelInterface() {
//    System.out.println("computeModelInterface");
//    JavaTypeRef typeRef = null;
//    JavaTypeDescriptorFactory instance = null;
//    JavaTypeRef expResult = null;
//    JavaTypeRef result = instance.computeModelInterface(typeRef);
//    assertEquals(expResult, result);
//    // TODO review the generated test code and remove the default call to fail.
//    fail("The test case is a prototype.");
//  }

  private interface TestApi {
    List<String> findThings(Set<String> set);
  }
}
