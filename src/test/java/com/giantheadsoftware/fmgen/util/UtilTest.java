package com.giantheadsoftware.fmgen.util;

import junit.framework.TestCase;

public class UtilTest extends TestCase {

  public void testToSlug() {
    assertEquals("camel-case",Util.toSlug("CamelCase"));
    assertEquals("java.util.string",Util.toSlug("java.util.String"));
    assertEquals("com.discussoftware.camel-case",Util.toSlug("com.discussoftware.CamelCase"));
    assertEquals("com.discussoftware.camel-case.ts",Util.toSlug("com.discussoftware.CamelCase.ts"));
  }

  public void testToCamel() {
    assertEquals("CamelCase", Util.toCamel("camel-case"));
//    assertEquals("java.CamelCase", Util.toCamel("java.camel-case"));
  }

  public void testFileToCamel() {
    assertEquals("CamelCase", Util.fileToCamel("camel-case"));
    assertEquals("/c/com/CamelCase", Util.fileToCamel("/c/com/camel-case"));
  }

  public void testFileToSlug() {
    assertEquals("camel-case", Util.fileToSlug("CamelCase"));
    assertEquals("/c/com/camel-case", Util.fileToSlug("/c/com/CamelCase"));
  }
}